import numpy as np
from StringIO import StringIO
try:
	import pydicom as dicom
except:
	import dicom

class mlcBeam(object):
	'''holds data for each beam in plan'''
	
	def __init__(self):
		# first dimension of arrays represent individual beams
		self.ControlPointCount = 0
		self.Name = None
		self.Number = None #beam number
		self.BeamEnergy = -1.0

		self.MU = [] #this is typically cumulative MU
		self.BankA = []
		self.BankB = []
		self.X1 = []
		self.X2 = []
		self.Y1 = []
		self.Y2 = []
		#isocenters (assumed static so only [0] get ref'ed)
		self.isoX = []
		self.isoY = []
		self.isoZ = []
		#rotations
		self.GantryAng = []
		self.CollimatorAng = []
		self.CouchAng = []

		#not used here
		self.TotMU = None #actually meterset
		self.map = None
		self.CP_maps = None #{'cp':cp,'mu':fmapData.cpMU[cp],'map':FluMapLibObj.get_map()}
		self.map = None
		self.cpMU = [] #this is a re-calculated per-control point MU

def load_plan(dicom_file):
	#TODO: check sop class
	plan = dicom.read_file(dicom_file)

	#we always want to rewind in case of StringIO
	if isinstance(dicom_file,StringIO):
		dicom_file.seek(0)

	return plan

def read_number_of_fractions(dicom_file):
	plan = load_plan(dicom_file)

	if len(plan.FractionGroupSequence) == 1:
		return plan.FractionGroupSequence[0].NumberOfFractionsPlanned
	else:
		raise Exception('More than one fraction group found!')

def read_mlc_boundaries(dicom_file):
	''' return the array for leaf LeafPositionBoundaries for first TREATMENT beam having an MLCX BLD ''' 
	plan = load_plan(dicom_file)

	for aBeam in plan.Beams:
		if aBeam.TreatmentDeliveryType == 'TREATMENT':
			for bld in aBeam.BLDs:
				if bld.RTBeamLimitingDeviceType == 'MLCX':
					return bld.LeafPositionBoundaries

def read_dicom_beams(dicom_file):
	''' all native dicom units (mm)'''
	dp = load_plan(dicom_file)

	######################################BEGIN DICOM READ ASSUMING IMRT################################
	dicomBeams = {}

	for b in dp.Beams:
		if b.TreatmentDeliveryType == "TREATMENT":
			print "Adding Beam (Number:{}, Energy:{}x)".format(b.BeamNumber,b.ControlPoints[0].NominalBeamEnergy)

			dicomBeams[b.BeamNumber] = mlcBeam()
			dicomBeams[b.BeamNumber].Number = b.BeamNumber
			try:
				dicomBeams[b.BeamNumber].Name = b.BeamName + "," + b.BeamDescription
			except:
				dicomBeams[b.BeamNumber].Name = b.BeamName

			# CONTROL POINTS
			dicomBeams[b.BeamNumber].ControlPointCount = b.NumberofControlPoints
			dicomBeams[b.BeamNumber].BeamEnergy = b.ControlPoints[0].NominalBeamEnergy
			dicomBeams[b.BeamNumber].Name += " ("+str(dicomBeams[b.BeamNumber].BeamEnergy)+"x)"

			temp_isoX = None 
			temp_isoY = None 
			temp_isoZ = None 

			#control point index
			#cpIdx = 1
			for cp in b.ControlPoints:
				# print "dicom_data.ControlPointIndex("+str(beamIdx)+","+str(cpIdx)+")=" + str(cp.ControlPointIndex)
			 	dicomBeams[b.BeamNumber].MU.append(cp.CumulativeMetersetWeight)

				try:
					#try to get data for VMAT
					dicomBeams[b.BeamNumber].GantryAng.append(cp.GantryAngle)
				except:
					pass

				try:
					dicomBeams[b.BeamNumber].CollimatorAng.append(cp.BeamLimitingDeviceAngle)
				except:
					pass

				try:
					dicomBeams[b.BeamNumber].CouchAng.append(cp.PatientSupportAngle)
				except:
					pass

				try:
					dicomBeams[b.BeamNumber].isoX.append(cp.IsocenterPosition[0])
					dicomBeams[b.BeamNumber].isoY.append(cp.IsocenterPosition[1])
					dicomBeams[b.BeamNumber].isoZ.append(cp.IsocenterPosition[2])
					temp_isoX = cp.IsocenterPosition[0] 
					temp_isoY = cp.IsocenterPosition[1] 
					temp_isoZ = cp.IsocenterPosition[2] 
				except:
					# print "no isocenter at cp, using previous recording ({},{},{})".format(temp_isoX,temp_isoY,temp_isoZ)
					dicomBeams[b.BeamNumber].isoX.append(temp_isoX)
					dicomBeams[b.BeamNumber].isoY.append(temp_isoY)
					dicomBeams[b.BeamNumber].isoZ.append(temp_isoZ)
					pass

				#try:
				#READ JAW AND MLC

				#first figure out what is in the control point and what the indexes are
				JawXidx = None
				JawYidx = None
				MLCidx = None
				
				idx = 0

				try:
					for bld in cp.BeamLimitingDevicePositions:
						if bld.RTBeamLimitingDeviceType == 'ASYMX':
							JawXidx = idx
						if bld.RTBeamLimitingDeviceType == 'ASYMY':
							JawYidx = idx
						if bld.RTBeamLimitingDeviceType == 'MLCX':
							MLCidx = idx
						idx +=1

				#for some conformal beams the last CP just says meterset is one (and BeamLimitingDevicePositions d.n.e.:
				except AttributeError:
					if cp.CumulativeMetersetWeight == 1.0:
						#then we are done with the beam anyway
						pass
					else:
						raise Exception('Control point empty, but CumulativeMetersetWeight != 1.0')

				if JawXidx is not None:
					jaw = cp.BeamLimitingDevicePositions[JawXidx]
					dicomBeams[b.BeamNumber].X1.append(jaw.LeafJawPositions[0])
					dicomBeams[b.BeamNumber].X2.append(jaw.LeafJawPositions[1])

				if JawYidx is not None:
					jaw = cp.BeamLimitingDevicePositions[JawYidx]
					dicomBeams[b.BeamNumber].Y1.append(jaw.LeafJawPositions[0])
					dicomBeams[b.BeamNumber].Y2.append(jaw.LeafJawPositions[1])
				
				if MLCidx is not None:							
					#mlc leaves
					leafs = cp.BeamLimitingDevicePositions[MLCidx].LeafJawPositions
					dicomBeams[b.BeamNumber].BankA.append( np.array(leafs[:len(leafs)/2])) #first half
					dicomBeams[b.BeamNumber].BankB.append( np.array(leafs[len(leafs)/2:])) #second half
					
	######################################END DICOM READ###################################


	# match beam numbers to MU (aka meterset)
	# find total mu for each beam and multiply meterset weigts (fractions to MU)
	for refBeam in dp.FractionGroups[0].ReferencedBeams:
		#self.beamDoses[refBeam.ReferencedBeamNumber] = refBeam.BeamDose # this is the dose at some ref point (in Gy)
		try:
			dicomBeams[refBeam.ReferencedBeamNumber].TotMU = refBeam.BeamMeterset # i think this is the mu
			dicomBeams[refBeam.ReferencedBeamNumber].MU = np.array(dicomBeams[refBeam.ReferencedBeamNumber].MU) * refBeam.BeamMeterset # i think this is the mu
			# print dicomBeams[refBeam.ReferencedBeamNumber]
		except:
			#some dose references are for setup
			pass
	return dicomBeams

