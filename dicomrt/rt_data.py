"""
This module contains functions and classes used to import and synchronize data contained
in DICOM RT dose, image, and structure files.
"""

__author__ = 'dstaub'

import os

import numpy as np

from scipy.interpolate import interpn
from scipy.ndimage.morphology import distance_transform_edt
from scipy.io import savemat, netcdf, loadmat
import scipy.io.matlab
scipy.io.matlab.mio5_params.NP_TO_MTYPES['b1'] = scipy.io.matlab.mio5_params.miUINT8
scipy.io.matlab.mio5_params.NP_TO_MXTYPES['b1'] = scipy.io.matlab.mio5_params.mxDOUBLE_CLASS

from matplotlib.path import Path
import matplotlib.pyplot as plt
import matplotlib.patches as patches

from StringIO import StringIO

#pydicom support
try:
    import pydicom as dicom
except ImportError:
    import dicom

# import threading

class RTData:
    """ Holds, imports, and calculates various information related to DICOM RT images.

    Contains methods that import and process image data and associated information from DICOM files.
    All data attributes can be set using the import and processing methods of the class, or assigned during
    object instantiation.

        Attributes:
            - slices: A 3D array of stacked image slices. Should be the same as `slices_raw` except wih redundant
                slices removed.
            - dv: A list of 3 values indicating x, y, and z voxel size in mm.
            - m_trans: A 3D array of stacked 3x3 transformation matrices. Each matrix converts the voxel subset of the
                identically indexed image slice in `slices` to patient coordinates.
            - slice_locations: A list of values indicating the location of each slice in `slices`.
            - contour_arrays: A dictionary where keys are ROI names and values are lists of arrays containing
                contour data for that ROI.
            - contour_image_inds: A dictionary whose keys are ROI names and values are lists of integers. Each integer
                list is the same length as the corresponding contour array list in `contour_arrays`, and provide the
                index of the slice in `slices` to which each contour array corresponds.
            - roi_names_native: A list of names associated with each imported ROI.
            - roi_names: A user specified list of names for each imported ROI, defaults to `roi_names_native` if
                none is supplied. These names are used as the keys for all dictionaries containing ROI related
                information.
            - volume_mask: A dictionary where keys are ROI names and values are 3D boolean arrays with dimensions identical
                to `slices`. Each array is True within the bounds of its corresponding ROI contours contained in
                `contour_arrays`, False else.
            - surface_mask: A dictionary where keys are ROI names and values are 3D boolean arrays with dimensions identical
                to `slices`. Values in each array are True for all True values in the corresponding `volume_mask` that are
                neighbored by 5 or less additional True values, False else.
            - voxel_coords: A 4D array of floats. The first 3 dimensions are identical in length to the
                `slices` matrix and the last dimension has length 3. The triplet voxel_coords[i,j,k,:] is the
                location of voxel slices[i,j,k] in the imaging coordinate system.
            - dose_grid: A 3D array of dose values with identical dimensions to `slices`.
            - alternate_image_file_groups: A list of lists of image file names that can be used to overwrite the original
                set of images with a new set. This can be useful, e.g., when there is an image time-series for which only
                the first image-set is contoured.
    """
    def __init__(self, slices=None, dv=None, m_trans=None, slice_locations=None, contour_arrays=None,
                 contour_image_inds=None, roi_names_native=None, roi_names=None, volume_mask=None, surface_mask=None,
                 voxel_coords=None, dose_grid=None, alternate_image_file_groups=None):
        self.slices = slices
        self.dv = dv
        self.m_trans = m_trans
        self.slice_locations = slice_locations
        self.contour_arrays = contour_arrays
        self.contour_image_inds = contour_image_inds
        self.roi_names_native = roi_names_native
        self.roi_names = roi_names
        self.volume_mask = volume_mask
        self.surface_mask = surface_mask
        self.voxel_coords = voxel_coords
        self.dose_grid = dose_grid
        self.alternate_image_file_groups = alternate_image_file_groups

    def import_data(self, image_files, structure_file=None, roi_ref_nums=None, roi_key_names=None, keep_only_roi_images=True, dose_file=None):
        """ Imports raw contour and image data from DICOM files.

        Imports image and contour data from DICOM files. Image data and contour objects are stored along with a stack of
        transformation matrices that convert 2D image pixel coordinates into 3D positions in the patient coordinate
        system. All data is sorted in descending order of image slice location.

            Parameters:
                - image_files: List of strings each containing a file path pointing to a DICOM image associated
                    with a contour object in the DICOM structure file.
                - structure_file: String containing the file path where a DICOM structure file is located. If not supplied
                    no contour information is imported and all image slices pointed to in `image_files` will be imported
                    and saved. Else contour information for desired ROIs, as specified by `roi_ref_nums`, will be imported
                    along with only associated image slices from `image_files`.
                - roi_ref_nums: List of reference numbers of ROIs for which contours should be imported. If not supplied the
                    default is the first ROI in the sequence within the structure object.
                - roi_key_names: List of strings, each containing the name of an ROI whose data should be imported. Each
                    name passed in this list will be paired with the identically indexed ROI specified in `roi_ref_nums`.
                - keep_only_roi_images: If True only image slices and data associated with ROI contours will be kept. If
                    False all image slices and data pointed to in the `image_files` file list will be imported and kept.
                    If no `structure_file` is supplied all images will be imported in either case.

            Returns:
                - self: The ContourImage object.

            Raises:
                - An error if any of the supplied file paths are not valid.
                - An error if the specified ROI does not exist.

            References:
                - http://nipy.org/nibabel/dicom/dicom_orientation.html
        """



        #############################################################################
        #- Import contour objects, contour image slice uids, and contour roi names -#
        #############################################################################
        contour_arrays = None
        contour_image_uids = None
        contour_image_inds = None
        roi_names_native = None
        roi_names = None

        if structure_file is not None:
            # Check that file paths point to valid files.
            # if os.path.isfile(structure_file) is False:
            #     raise Exception('File \'{0}\' does not exist.'.format(structure_file))

            # Check input key names
            if roi_key_names is not None:
                try:
                    len(np.array(roi_key_names))
                except TypeError:
                    roi_key_names = [roi_key_names]

            # Check input reference numbers
            if roi_ref_nums is not None:
                try:
                    len(roi_ref_nums)
                except TypeError:
                    roi_ref_nums = [roi_ref_nums]

            # Check that length of key name and reference number lists are identical
            if roi_key_names is not None and roi_ref_nums is not None:
                if len(roi_key_names) != len(roi_ref_nums):
                    raise ValueError('Number of ROI key names does not match number of ROI reference numbers.')

            # Import structure data
            s = dicom.read_file(structure_file)

            #rewind if string IO buffer
            if isinstance(structure_file,StringIO):
                structure_file.seek(0)

            # Get ROI reference numbers for which ROIs should be imported if none supplied
            if roi_ref_nums is None:
                roi_ref_nums = [seq.ReferencedROINumber for seq in s.ROIContourSequence]
                if roi_key_names is not None:
                    roi_ref_nums = roi_ref_nums[:len(roi_key_names)]

            # Find indices and names of ROIs for which contours should be imported
            roi_inds = []
            roi_names_native = []
            for roi_ref_num in roi_ref_nums:
                for i in range(len(s.ROIContourSequence)):
                    if s.ROIContourSequence[i].ReferencedROINumber == roi_ref_num:
                        roi_inds.append(i)
                    if s.StructureSetROISequence[i].ROINumber == roi_ref_num:
                        roi_names_native.append(s.StructureSetROISequence[i].ROIName)
            if len(roi_inds) != len(roi_ref_nums):
                raise NameError('One or more ROI reference numbers were not valid.')

            # Set roi names to be used as dictionary keys
            if roi_key_names is not None:
                roi_names = roi_key_names
            else:
                roi_names = roi_names_native

            # Import arrays of contour data and arrays of uids specifying the image to which each contour belongs
            contour_arrays = {}
            contour_image_uids = {}
            for roi, ind in zip(roi_names, roi_inds):
                contours = np.array(s.ROIContourSequence[ind].ContourSequence)
                mask = np.array([len(contour.ContourData) / 3 > 2 for contour in contours])  # identify nonsense contours with 2 or fewer vertices
                contour_arrays[roi] = contours[mask]
                contour_image_uids[roi] = np.array([c_obj.ContourImageSequence[0].ReferencedSOPInstanceUID for c_obj in contour_arrays[roi]])
                for i, contour_array in enumerate(contour_arrays[roi]):
                    contour_points = np.array(contour_array.ContourData, dtype='float64')
                    contour_arrays[roi][i] = np.array([contour_points[0::3], contour_points[1::3], contour_points[2::3]]).transpose()

        #######################
        #- Import image data -#
        #######################
        # Import image image slice uids and locations, sort image data according to location
        image_files = np.array(image_files)
        dicom_images = []
        image_locations = np.zeros(len(image_files), dtype='float')
        image_uids = np.zeros(len(image_files), dtype='object')
        for i in range(len(image_files)):
            image = dicom.read_file(image_files[i])
            #now replace file pointer (or path) with pydicom object
            dicom_images.append(image)
            image_locations[i] = image.SliceLocation
            image_uids[i] = image.SOPInstanceUID
        inds = np.argsort(+image_locations) #michael switched from - to + sorting
        image_locations = image_locations[inds]
        image_uids = image_uids[inds]
        # image_files = image_files[inds]

        #rewind file pointers if they are StringIO buffers
        for imgf in image_files:
            if isinstance(imgf,StringIO):
                imgf.seek(0)

        #replace file pointers with actual pydicom objects
        image_files = np.array(dicom_images)[inds]

        # Crop image data arrays if necessary, get slice indices for each contour
        if contour_arrays is not None:
            # Get all contour image uids in one big list
            contour_image_uids_all = []
            for contour_image_uid in contour_image_uids.values():
                contour_image_uids_all.extend(contour_image_uid)

            # Find intersection of image uid and contour image uid list
            inds = np.where(np.in1d(image_uids, contour_image_uids_all))
            min_ind = np.min(inds)
            max_ind = np.max(inds) + 1

            if keep_only_roi_images is True:
                # Crop arrays
                image_locations = image_locations[min_ind:max_ind]
                image_uids = image_uids[min_ind:max_ind]
                image_files = image_files[min_ind:max_ind]

            # Get slice index for each contour array
            contour_image_inds = {}
            for roi in roi_names:
                contour_image_inds[roi] = np.array([np.where(c_im_uids == image_uids)[0][0] for c_im_uids in contour_image_uids[roi]])

        # Import initial image slice data
        # image = dicom.read_file(image_files[0]) #old
        image = image_files[0]
        num_pix = np.array([image.Rows, image.Columns])
        dv = np.array([image.PixelSpacing[0], image.PixelSpacing[1], image.SliceThickness])
        x_r = np.array(image.ImageOrientationPatient[0:3]) * dv[1]
        y_r = np.array(image.ImageOrientationPatient[3:]) * dv[0]
        f_r = np.array([y_r, x_r])
        slices = np.zeros(np.append(num_pix, len(image_files)), dtype='uint16')
        m_trans = np.zeros([3, 3, len(image_files)])

        # Read in image slice data and pixel-to-imaging-coordinate-system transformation matrix for all image slices
        for i,image in enumerate(image_files):#range(len(image_files)):
            # image = dicom.read_file(image_files[i])
            slices[:, :, i] = image.pixel_array
            s_r = np.array(image.ImagePositionPatient)
            m_trans[:, :, i] = np.append(f_r, [s_r], axis=0).transpose()

        # Get grid of image voxel coordinates in imaging coordinate system
        grid_i = voxel_coords_grid(num_pix, m_trans)

        #######################################
        #- Import and save dose related data -#
        #######################################
        dose_grid = None
        if dose_file is not None:
            # Check that file path points to valid file.
            # if os.path.isfile(dose_file) is False:
            #     raise Exception('File \'{0}\' does not exist.'.format(dose_file))

            # Import dose data
            d = dicom.read_file(dose_file)

            #rewind if string IO buffer
            if isinstance(dose_file,StringIO):
                dose_file.seek(0)

            dose_grid_raw = np.transpose(d.pixel_array, [1, 2, 0])
            dp_d = [d.PixelSpacing[0], d.PixelSpacing[1]]
            x_r = np.array(d.ImageOrientationPatient[0:3]) * dp_d[1]
            y_r = np.array(d.ImageOrientationPatient[3:]) * dp_d[0]
            f_r = np.array([y_r, x_r])
            m_trans_d = np.zeros([3, 3, dose_grid_raw.shape[2]])
            for i in range(dose_grid_raw.shape[2]):
                s_r = np.array(d.ImagePositionPatient) + np.array([0, 0, d.GridFrameOffsetVector[i]])
                m_trans_d[:, :, i] = np.append(f_r, [s_r], axis=0).transpose()

            # Resample dose grid at voxel locations, convert to units of Gy.
            grid_d = voxel_coords_grid(dose_grid_raw.shape[0:2], m_trans_d)
            xi = grid_i.reshape([np.prod(grid_i.shape[0:3]), 3])
            xi = xi[:, [1, 0, 2]]
            points = (grid_d[:, 0, 0, 1], grid_d[0, :, 0, 0], grid_d[0, 0, :, 2])
            dose_grid = interpn(points, dose_grid_raw, xi, bounds_error=False, fill_value=0).reshape(grid_i.shape[0:3]).astype('float32')
            dose_grid *= d.DoseGridScaling

        #############################
        #- Assign attribute values -#
        #############################
        self.contour_arrays = contour_arrays
        self.contour_image_inds = contour_image_inds
        self.slices = slices
        self.m_trans = m_trans
        self.dv = dv
        self.slice_locations = image_locations
        self.roi_names = roi_names
        self.roi_names_native = roi_names_native
        self.voxel_coords = grid_i
        self.dose_grid = dose_grid

        return self

    def create_roi_masks(self, rois_to_mask=None):
        """ Creates 3D binary volume and surface masks from DICOM contour and image data.

        Produces 3D binary masks denoting all voxels within contour volume, and all voxels on surface of volume mask.
        It also removes redundant image data and saves cropped arrays. In addition arrays containing the positions of
        all mask points and contour points are created and stored.

            Parameters:
                - rois_to_mask: List of strings denoting names of ROIs that should be masked. Each
                    string is assumed to be a valid key into the `contour_arrays` and
                    `contour_image_uids` dictionaries. If none is supplied masks for all ROIs
                    are created by default.

            Returns:
                - self: The DicomRTImage object.

            Raises:
        """
        # Initialize values
        if rois_to_mask is None:
            roi_names = np.array(self.roi_names)
        else:
            try:
                len(np.array(rois_to_mask))
            except TypeError:
                roi_names = np.array([rois_to_mask])
            else:
                roi_names = np.array(rois_to_mask)
            if len(set(roi_names).intersection(self.roi_names)) != len(roi_names):
                raise KeyError('Invalid ROI. Valid options are {0}.'.format(self.roi_names))

        # Create volume and surface masks
        volume_mask = {}
        surface_mask = {}
        for roi in roi_names:
            volume_mask[roi] = self.make_volume_mask(roi)
            surface_mask[roi] = self.make_surface_mask(volume_mask[roi])

        # Assign attribute values
        self.volume_mask = volume_mask
        self.surface_mask = surface_mask

        return self

    def make_volume_mask(self, roi):
        """ Create a 3D volume mask from a series of 2D contours.

        Produces a 3D binary mask based on a stack of 2D contours and a 4D grid of voxel coordinates.

            Parameters:
                - roi: strings denoting the name of an ROI that should be masked. The
                    string is assumed to be a valid key into the `contour_arrays` and
                    `contour_image_uids` dictionaries. If none is supplied masks for all ROIs
                    are created by default.

            Returns:
                - volume_mask: The 3D binary mask.

            Raises:
        """
        # Import and initialize data
        contour_image_inds = self.contour_image_inds[roi]
        contour_arrays = self.contour_arrays[roi]
        slice_shape = self.slices.shape
        volume_mask = np.zeros(slice_shape, dtype='bool')
        iv, jv = np.meshgrid(np.arange(slice_shape[0]), np.arange(slice_shape[1]), sparse=False, indexing='ij')
        grid_points = np.vstack([iv.flatten(), jv.flatten()]).T

        # Create mask
        for i, contour_array in enumerate(contour_arrays):
            # Get slice index for current contour
            slice_num = contour_image_inds[i]

            # Convert contour points to index units
            m_trans_xy = self.m_trans[0:2, 0:2, slice_num]
            m_trans_z = self.m_trans[0:2, 2, slice_num]
            m_trans_xy_inv = np.linalg.inv(m_trans_xy)
            contour_array = np.dot(m_trans_xy_inv, (contour_array[:, 0:2] - m_trans_z).T).T

            # Create polygon from contour points, see which grid points fall inside it
            codes = [Path.MOVETO] + [Path.LINETO] * (len(contour_array) - 2) + [Path.CLOSEPOLY]
            contour_poly = Path(contour_array, codes=codes)
            in_points = contour_poly.contains_points(grid_points).reshape(slice_shape[0:2])

            # Add new contour mask to mask slice, check for ring structure
            in_points = in_points.reshape(slice_shape[0:2])
            overlap_mask = volume_mask[:, :, slice_num] & in_points
            overlap_count = np.sum(overlap_mask)
            min_count = np.min([np.sum(volume_mask[:, :, slice_num]), np.sum(in_points)])
            volume_mask[:, :, slice_num] = volume_mask[:, :, slice_num] | in_points
            if overlap_count == min_count:
                volume_mask[:, :, slice_num] *= ~overlap_mask  # Ring structure present, remove inner pixels from mask slice

        return volume_mask

    @staticmethod
    def make_surface_mask(volume_mask):
        """ Create a 3D surface mask from a 3D volume mask.

        Produces a 3D binary mask consisting of all points within a previously calculated volume mask not
        bordered on all 6 sides by another positive mask point.

            Parameters:
                - volume_mask: A 3D boolean mask array.

            Returns:
                - surface_mask: A 3D boolean array equal to True for all True points within the volume mask bordered
                    by less than 6 additional True values, else False.

            Raises:
        """
        nv = volume_mask.shape
        zeros_i = np.zeros([1, nv[1], nv[2]])
        zeros_j = np.zeros([nv[0], 1, nv[2]])
        zeros_k = np.zeros([nv[0], nv[1], 1])
        surround_points_num = (np.concatenate([zeros_i, volume_mask[0:nv[0] - 1, :, :]], axis=0) +
                               np.concatenate([volume_mask[1:nv[0], :, :], zeros_i], axis=0) +
                               np.concatenate([zeros_j, volume_mask[:, 0:nv[1] - 1, :]], axis=1) +
                               np.concatenate([volume_mask[:, 1:nv[1], :], zeros_j], axis=1) +
                               np.concatenate([zeros_k, volume_mask[:, :, 0:nv[2] - 1]], axis=2) +
                               np.concatenate([volume_mask[:, :, 1:nv[2]], zeros_k], axis=2))
        surface_mask = (surround_points_num < 6) * volume_mask
        return surface_mask

    def visualize(self, attribute, ind, roi=None, show=True, color='red', alpha=1.0, threshold=0):
        """ Visualise an image or contour slice.

        Produces a 2D plot of the specified slice of desired ContourImage attribute. Attribute data should have already
        been loaded.

            Parameters:
                - attribute: String indicating which data attribute of the DicomRTImage object
                    should be visualized.
                - ind: Index of the image slice or contour object to be visualized.
                - roi: A string indicating for which ROI the visualization should be performed.
                - show: If True an image will be plotted, if False the image will be created but
                    not plotted. This can be useful for overlaying multiple images.
                - color: Sets the color of certain attributes (e.g. a contour).
                - alpha: Sets the transparency of certain attributes (e.g. the volume mask).
                - threshold: Sets the intensity threshold below which an image is not displayed
                    for certain attributes (e.g. the dose grid).

            Returns:

            Raises:
                - Error if valid ContourImage attribute is not supplied.
                - Error if an out of bounds index is given.
        """
        # Create list of possible attributes
        general_attributes = np.array(['slices', 'dose_grid'])
        roi_specific_attributes = np.array(['volume_mask', 'surface_mask', 'contour_arrays'])
        valid_attributes = np.concatenate([general_attributes, roi_specific_attributes])
        initialized_attributes = np.array([attr for attr in valid_attributes if getattr(self, attr) is not None])

        # Check and process input attributes to be exported
        if attribute not in initialized_attributes:
            raise Exception('Invalid attribute supplied for visualization. Initialized attributes are {0}'.format(initialized_attributes))

        # Check for valid ROI
        if attribute in roi_specific_attributes and roi not in self.roi_names:
                raise Exception('Invalid ROI supplied for exporting. Valid ROIs are {0}'.format(self.roi_names))

        # Get attribute to plot
        attr = getattr(self, attribute)
        if attribute in roi_specific_attributes:
            attr = attr[roi]

        # Make plot of contour
        if attribute == 'contour_arrays':
            fig = plt.figure()
            ax = fig.add_subplot(111)
            cont_inds = np.where(self.contour_image_inds[roi] == ind)[0]
            for cont_ind in cont_inds:
                # Import contour points
                contour_points = attr[cont_ind]

                # Convert contour points to index units
                m_trans_xy = self.m_trans[0:2, 0:2, ind]
                m_trans_z = self.m_trans[0:2, 2, ind]
                m_trans_xy_inv = np.linalg.inv(m_trans_xy)
                contour_points = np.dot(m_trans_xy_inv, (contour_points[:, 0:2] - m_trans_z).T).T
                contour_points = contour_points[:, [1, 0]]

                # Create polygon from contour points
                codes = [Path.MOVETO] + [Path.LINETO] * (len(contour_points) - 2) + [Path.CLOSEPOLY]
                contour_poly = Path(contour_points, codes=codes)

                # Plot contour
                patch = patches.PathPatch(contour_poly, facecolor='none', lw=2, ec=color)
                ax.add_patch(patch)

                # Set plot boundaries
                if self.slices is not None:
                    plt.xlim([0, self.slices.shape[1] - 1])
                    plt.ylim([self.slices.shape[0] - 1, 0])
                else:
                    plt.xlim([np.min(contour_points[:, 0]) - 1, np.max(contour_points[:, 0]) + 1])
                    plt.ylim([np.min(contour_points[:, 1]) - 1, np.max(contour_points[:, 1]) + 1])

        # Make plot of mask slice
        elif attribute in ('volume_mask', 'surface_mask'):
            masked_data = np.ma.masked_array(~attr[:, :, ind], ~attr[:, :, ind])
            plt.imshow(masked_data, alpha=alpha)

        # Make plot of image slice
        elif attribute == 'slices':
            plt.imshow(attr[:, :, ind], cmap='gray')

        # Make plot of dose slice
        elif attribute == 'dose_grid':
            if np.max(attr[:, :, ind]) > threshold:
                masked_dose = np.ma.masked_where(attr[:, :, ind] < threshold, attr[:, :, ind])
                plt.imshow(masked_dose, alpha=alpha)

        # Make generic image plot
        else:
            plt.imshow(attr[:, :, ind])
            plt.show()

        # Display plot if desired
        if show:
            plt.show()

    def get_time_groups(self, image_files):
        """ Group together image files based on acquisition time.

        Groups together image files based on the TemporalPositionIdentifier DICOM attribute.

            Parameters:
                - image_files: List of file paths pointing to DICOM image files.

            Returns:
                - An array of lists of strings pointing to DICOM image files. All files within a list belong to the
                    same time group.

            Raises:
        """
        # Get temporal positions for all images
        time_pos = np.array([dicom.read_file(image_file).TemporalPositionIdentifier for image_file in image_files])
        time_pos_unique = np.unique(time_pos)

        # Group together image files belonging to the same time group.
        image_files = np.array(image_files)
        time_groups = np.array([image_files[time_pos == time] for time in time_pos_unique])

        # Set attribute values
        self.alternate_image_file_groups = time_groups

        return self

    def import_alternate_images(self, ind):
        """ Import an alternate set of image slices.

        Imports a new set of image slices corresponding to original slice locations and overwrites old image slice
        data. This is useful, for instance, when there is a time-series of images that contour features should be
        extracted from, but only one of the images has an associated contour.

            Parameters:
                - ind: Index of the alternate image group to be imported.

            Returns:
                - self: The ContourImage object.

            Raises:
                An error if the alternate image group index is out of bounds.
        """
        # Check for out of bounds index
        if ind >= len(self.alternate_image_file_groups):
            raise IndexError('Maximum alternate file group index exceeded, limit is {0}.'
                             .format(len(self.alternate_image_file_groups) - 1))

        # Import slices for new image files
        slices = np.zeros(self.slices.shape)
        for image_file in self.alternate_image_file_groups[ind]:
            image = dicom.read_file(image_file)
            if image.SliceLocation in self.slice_locations:
                inds = self.slice_locations == image.SliceLocation
                slices[:, :, inds] = np.dstack([image.pixel_array for i in range(sum(inds))])

        # Set attribute values
        self.slices = slices

        return self

    def calc_roi_norm_grid(self, norm_roi, attribute, roi=None, weights=None, calc_avg=True):
        """ Resample dose grid relative to ROI surface.

        Creates a resampled data grid that looks like the result of collapsing the
        original data grid around the surface of the specified ROI. First the minimum
        displacement (in voxel units) of each voxel to the input ROI is found. Then each
        voxel's displacement is used as the new location for that voxel's data value in a
        resampled data grid. Data values mapped to the same location in the resampled grid
        are averaged by default, or they can be summed. The center of the resampled data
        grid is treated as the origin.

            Parameters:
                - norm_roi: Name of the ROI to which the dose grid is normalized.
                - attribute: The attribute of the DicomRTImage object upon which the normalization
                    transform should be performed.
                - roi: A string used as a key for retrieving an ROI specific data array. If the
                    specified attribute is not ROI specific then this value is ignored.
                - weights: Can be used to specify how voxels are weighted. This can be important
                    since the during the normalization transform multiple dose values from the
                    original dose grid may be mapped to identical locations in the transformed
                    dose grid. In such situations the final transformed dose grid value will be
                    calculated as a weighted average of contributing values from the original
                    dose grid. Valid options are: None - all voxels will be weighted equally,
                    "ROI" - voxels falling within an ROI mask will be weighted such that
                    contributions from non-ROI voxels are negligible, 3D numpy array - a custom
                    weight matrix with the same shape as the dose grid.
                - calc_avg: Flag used for specifying whether or not values mapped to the same
                    location are averaged or summed.

            Returns:
                - self: The DicomRTObject object.

            Raises:
                An error if the `norm_roi` does not match the name of an imported ROI.
        """
        # Check input norm ROI value
        if norm_roi not in self.roi_names:
            raise KeyError('"{0}" does not match the name of any imported ROI. Valid options are {1}.'.format(norm_roi, self.roi_names))

        # Check input attribute value
        general_attributes = np.array(['slices', 'dose_grid'])
        roi_specific_attributes = np.array(['volume_mask', 'surface_mask'])
        valid_attributes = np.concatenate([general_attributes, roi_specific_attributes])
        initialized_attributes = np.array([attr for attr in valid_attributes if getattr(self, attr) is not None])
        if attribute not in initialized_attributes:
            raise AttributeError('"{0}" does not match the name of any initialized attribute. Valid options are {1}.'
                                 .format(attribute, initialized_attributes))

        # Check input ROI value
        if attribute in roi_specific_attributes:
            if roi not in self.roi_names:
                raise KeyError('"{0}" does not match the name of any imported ROI. Valid options are {1}.'.format(roi, self.roi_names))

        # Check and process weight matrix
        try:
            weights.shape
        except AttributeError:
            if weights is None:
                weights = np.ones(self.dose_grid.shape)
            elif weights == 'ROI':
                weights = np.ones(self.dose_grid.shape)
                for roi in self.roi_names:
                    weights[self.volume_mask[roi]] = 1e7
            else:
                raise ValueError('Invalid weight argument, valid options are None, "ROI", or a custom weight matrix.')
        else:
            if weights.shape != self.dose_grid.shape:
                raise ValueError('Weight matrix shape does not match dose grid shape.')

        # Get grid of 3D data
        if attribute in general_attributes:
            data_grid = getattr(self, attribute)
        elif attribute in roi_specific_attributes:
            data_dict = getattr(self, attribute)
            data_grid = data_dict[roi]

        # Initialize new data grid
        grid_size_new = (np.array(data_grid.shape) / 2) * 2 + 1  # If dimension length is even, increase by 1, else leave the same
        data_grid_new = np.zeros(grid_size_new)

        # Compute minimum displacement vectors in voxel units
        disp = calc_min_displacement(self, None, norm_roi, target_type='volume', disp_type='ijk')

        # Convert displacements into subscripts for new dose grid
        subs_new = (disp + grid_size_new / 2).T

        # Get weighted data grid
        weights = weights.flatten()
        data_grid = data_grid.flatten()
        data_grid = data_grid * weights

        # Get flattened subscript and data arrays, sort so that identical locations are adjacent
        sort_inds = np.lexsort(subs_new)
        subs_new_sort = subs_new[:, sort_inds]
        data_grid_sort = data_grid[sort_inds]
        weights_sort = weights[sort_inds]

        # Calculate places where sorted subscript sets change, get indices of unique subscript sets
        change_inds = np.where(np.linalg.norm(np.diff(subs_new_sort), axis=0) != 0)[0]
        unique_inds = np.concatenate([[0], change_inds + 1])

        # Get data and weight sums for each unique subscript set
        cum_data_grid_sort = np.cumsum(data_grid_sort)
        cum_weights_sort = np.cumsum(weights_sort)
        sum_data = np.concatenate([[cum_data_grid_sort[change_inds[0]]],
                                   cum_data_grid_sort[change_inds[1:]] - cum_data_grid_sort[change_inds[:-1]],
                                   [cum_data_grid_sort[-1] - cum_data_grid_sort[change_inds[-1]]]])
        sum_weight = np.concatenate([[cum_weights_sort[change_inds[0]]],
                                     cum_weights_sort[change_inds[1:]] - cum_weights_sort[change_inds[:-1]],
                                     [cum_weights_sort[-1] - cum_weights_sort[change_inds[-1]]]])

        # Get average data value for each unique subscript set, avoid dividing by zero; or just stick with the summed data
        if calc_avg is True:
            zero_mask = sum_weight == 0
            norm_data = np.zeros(len(sum_data))
            norm_data[~zero_mask] = sum_data[~zero_mask].astype('float') / sum_weight[~zero_mask]
        else:
            norm_data = sum_data

        # Remove out of bounds data, assign data to new data grid
        subs_new_unique = subs_new_sort[:, unique_inds]
        mask = ((subs_new_unique[0, :] >= 0) * (subs_new_unique[0, :] < grid_size_new[0]) *
                (subs_new_unique[1, :] >= 0) * (subs_new_unique[1, :] < grid_size_new[1]) *
                (subs_new_unique[2, :] >= 0) * (subs_new_unique[2, :] < grid_size_new[2]))
        subs_new_unique = subs_new_unique[:, mask]
        norm_data = norm_data[mask]
        data_grid_new[subs_new_unique[0, :], subs_new_unique[1, :], subs_new_unique[2, :]] = norm_data

        return data_grid_new

    def reverse_roi_norm_grid(self, norm_roi, data_grid_norm, grid_size_rev):
        """ Reverse norm ROI dose transformation.

        Takes an ROI normalized dose grid and reverses the transformation procedure to recover the
        original dose grid.

            Parameters:
                - norm_roi: Name of the ROI to which the dose grid is normalized.

            Returns:
                - self: A 3D dose grid array with the same dimensions as the original dose grid array.

            Raises:
                An error if no ROI normalized dose distribution has been previously computed.
        """
        # Check input norm ROI value
        if norm_roi not in self.roi_names:
            raise KeyError('"{0}" does not match the name of any imported ROI. Valid options are {1}.'.format(norm_roi, self.roi_names))

        # Initialize new data grid
        grid_size_norm = np.array(data_grid_norm.shape)
        data_grid_rev = np.zeros(grid_size_rev)

        # Compute euclidean distance transform to selected ROI, get results in terms of voxel index
        subs_edt = distance_transform_edt(~self.volume_mask[norm_roi], sampling=self.dv, return_distances=False, return_indices=True)

        # Get array of voxel subscripts
        i_sub, j_sub, k_sub = np.meshgrid(range(grid_size_rev[0]), range(grid_size_rev[1]), range(grid_size_rev[2]), indexing='ij')
        ijk_sub = np.concatenate([i_sub.reshape((1,) + i_sub.shape), j_sub.reshape((1,) + j_sub.shape), k_sub.reshape((1,) + k_sub.shape)], axis=0)

        # Compute minimum displacement vectors in voxel units
        disp = ijk_sub - subs_edt

        # Convert displacements into subscripts for new dose grid
        subs_new = disp + grid_size_norm.reshape([3, 1, 1, 1]) / 2

        # Remove out of bounds data
        mask = ((subs_new[0, :] >= 0) * (subs_new[0, :] < grid_size_norm[0]) *
                (subs_new[1, :] >= 0) * (subs_new[1, :] < grid_size_norm[1]) *
                (subs_new[2, :] >= 0) * (subs_new[2, :] < grid_size_norm[2]))
        subs_new = subs_new[:, mask]
        ijk_sub = ijk_sub[:, mask]

        # Get flattened subscript and arrays, get reverse transformed dose distribution
        subs_new = subs_new.reshape([3, np.prod(subs_new.shape[1:])])
        ijk_sub = ijk_sub.reshape([3, np.prod(ijk_sub.shape[1:])])
        data_grid_rev[ijk_sub[0, :], ijk_sub[1, :], ijk_sub[2, :]] = data_grid_norm[subs_new[0, :], subs_new[1, :], subs_new[2, :]]

        return data_grid_rev

    def resample(self, resamp_factor, attributes=None, rois=None, method='linear', bthresh=0.0):
        """ Resample data arrays that are attributes of a DicomRTImage object.

        Resamples desired data arrays that are attributes of a DicomRTImage object
        according to user supplied resampling factors.

            Parameters:
                - resamp_factor: A number or list of 3 numbers. If a single number then data arrays
                    will be resampled to have new voxel size equal to the old voxel size uniformly scaled
                    by the resampling factor along each dimension. If a list of 3 numbers the new voxel
                    dimensions will be scaled individually according to the corresponding resampling
                    factor in the list.
                - attributes: A string or a list of strings indicating DicomRTObject attributes that should be resampled.
                - rois: A string or a list of strings indicating ROIs for which resampling should be performed. If no
                    ROI specific attributes are specified for resampling then this value is ignored.
                - method: Interpolation method used for resampling. Valid options are 'linear' or 'nearest'.
                - bthresh: A number indicating the threshold to use for converting a resampled data
                    array to boolean type. This value is ignored for non-boolean data arrays.

            Returns:
                - image_resamp: A new DicomRTImage object containing resampled data and relevant attributes.

            Raises:
                - An error if an invalid attribute is supplied.
                - An error if an invalid ROI is supplied.
        """
        # Create list of possible attributes
        general_attributes = np.array(['slices', 'dose_grid'])
        roi_specific_attributes = np.array(['volume_mask', 'surface_mask'])
        valid_attributes = np.concatenate([general_attributes, roi_specific_attributes])
        initialized_attributes = np.array([attr for attr in valid_attributes if getattr(self, attr) is not None])

        # Check and process input attributes to be resampled
        if attributes is None:
            attributes = initialized_attributes
        else:
            try:
                len(np.array(attributes))
            except TypeError:
                attributes = np.array([attributes])
            else:
                attributes = np.array(attributes)
            if len(set(attributes).intersection(initialized_attributes)) != len(attributes):
                raise Exception('Invalid attribute supplied for resampling. Initialized attributes are {0}'.format(initialized_attributes))

        # Check and process input ROIs to be resampled
        if rois is None:
            rois = np.array(self.roi_names)
        else:
            try:
                len(np.array(rois))
            except TypeError:
                rois = np.array([rois])
            else:
                rois = np.array(rois)
            if len(set(rois).intersection(self.roi_names)) != len(rois):
                raise Exception('Invalid ROI supplied for resampling. Valid ROIs are {0}'.format(self.roi_names))

        # Initialize values
        image_resamp = DicomRTImage()

        # Perform resampling
        for attribute in attributes:
            if attribute in general_attributes:
                # Retrieve attribute data
                attr = getattr(self, attribute)

                # Resample attribute
                attr_resamp = resample_array(resamp_factor, attr, return_type='float', method=method)

                # Set attribute in new object
                setattr(image_resamp, attribute, attr_resamp)

            elif attribute in roi_specific_attributes:
                # Retrieve attribute dictionary, initialize new dictionary
                attr_dict = getattr(self, attribute)
                attr_dict_resamp = {}

                # Do resampling for each ROI
                for roi in rois:
                    # Retrieve attribute data
                    attr = attr_dict[roi]

                    # Resample attribute
                    attr_resamp = resample_array(resamp_factor, attr, return_type='bool', method=method, bthresh=bthresh)

                    # Save data in dictionary
                    attr_dict_resamp[roi] = attr_resamp

                # Set attributes in new object
                setattr(image_resamp, attribute, attr_dict_resamp)
                image_resamp.roi_names = rois

            # Set additional object attributes
            image_resamp.nv = attr_resamp.shape
            if self.dv is not None:
                image_resamp.dv = self.dv * resamp_factor

        return image_resamp

    def export(self, file_dir, export_names=None, attributes=None, rois=None, file_format='npy'):
        """ Export data arrays stored as DicomRTImage attributes.

        Export desired data array attributes of a DicomRTImage object to disk as DICOM files.

            Parameters:
                - file_dir: Directory to which files should be written.
                - export_names: A string or list of strings with the same length as `attributes` used to
                    create the file names to which data arrays are written. File names for the
                    attribute specified in `attributes[i]` are composed of the string `export_names[i]`
                    followed by an underscore followed by the ROI name (if applicable).
                    If `export_names` is None the  file name for the attribute specified in
                    `attributes[i]` is composed of the string `attributes[i]` followed by an
                    underscore, followed by the ROI name (if applicable). All file names are
                    appended with the a file format specific extension if the extension is not already
                    present.
                - attributes: A list of strings indicating DicomRTObject attributes that should be
                    exported. Each string must be the exact name of the desired attribute. If None then
                    all initialized attributes are saved.
                - rois: A list of ROIs for which data should be exported. If no ROI specific attributes
                    are specified for exporting then this value is ignored. If None then all initialized
                    ROIs are saved.
                - file_format: Output format for data files. Options are "npy" for NumPy format, "mat" for
                    Matlab format, "netcdf" for NetCDF format, "bin" for binary format, and "txt" for
                    text format.

            Raises:
                - An error if an invalid attribute is supplied.
                - An error if an invalid ROI is supplied.
        """
        # Create list of possible attributes
        general_attributes = np.array(['slices', 'dose_grid', 'dv', 'roi_names', 'roi_names_native', 'm_trans', 'voxel_coords', 'slice_locations'])
        roi_specific_attributes = np.array(['volume_mask', 'surface_mask', 'contour_arrays', 'contour_image_inds'])
        valid_attributes = np.concatenate([general_attributes, roi_specific_attributes])
        initialized_attributes = np.array([attr for attr in valid_attributes if getattr(self, attr) is not None])

        # Check and process input attributes to be exported
        if attributes is None:
            attributes = initialized_attributes
        else:
            try:
                len(np.array(attributes))
            except TypeError:
                attributes = np.array([attributes])
            else:
                attributes = np.array(attributes)
            if len(set(attributes).intersection(initialized_attributes)) != len(attributes):
                raise Exception('Invalid attribute supplied for exporting. Initialized attributes are {0}'.format(initialized_attributes))

        # Check and process input ROIs to be exported
        if rois is None:
            rois = np.array(self.roi_names)
        else:
            try:
                len(np.array(rois))
            except TypeError:
                rois = np.array([rois])
            else:
                rois = np.array(rois)
            if len(set(rois).intersection(self.roi_names)) != len(rois):
                raise Exception('Invalid ROI supplied for exporting. Valid ROIs are {0}'.format(self.roi_names))

        # Check and process export names
        if export_names is None:
            export_names = attributes
        else:
            try:
                len(np.array(export_names))
            except TypeError:
                export_names = np.array([export_names])
            else:
                export_names = np.array(export_names)
            if len(export_names) != len(attributes):
                raise Exception('The number of export names must be the same as the number of attributes to be exported.')

        # Export data
        for attribute, export_name in zip(attributes, export_names):
            if attribute in general_attributes:
                # Get data array
                attr = getattr(self, attribute)

                # Get export file path
                file_path = os.path.join(file_dir, export_name)

                # Export
                export_array(attr, file_path, file_format=file_format)

            elif attribute in roi_specific_attributes:
                # Retrieve attribute dictionary
                attr_dict = getattr(self, attribute)

                # Do export for each ROI
                for roi in rois:
                    # Get data array
                    attr = attr_dict[roi]

                    # Get export file path
                    file_path = os.path.join(file_dir, export_name + '_' + roi)

                    # Export
                    export_array(attr, file_path, file_format=file_format)

    def load(self, file_path, attribute, roi=None, file_format=None, shape=None, dtype=None):
        """ Load data array from disk into a DicomRTImage attribute.

        Load the data array stored at the specified file path into an attribute of the DicomRTImage object.

            Parameters:
                - file_path: File path of data array to be loaded.
                - attribute: A string indicating DicomRTObject attribute to which the loaded data should
                    be assigned.
                - roi: A string indicating the ROI which data being loaded belongs to. If the attribute
                    for which data is being loaded is not ROI specific then this value is ignored.
                - file_format: Format of file being loaded. Options are "npy" for NumPy format, "mat" for
                    Matlab format, "nc" for NetCDF format, "bin" for binary format, and "txt" for
                    text format. If None, an attempt will be made to detect a recognized file extension.
                - shape: The shape of the data array being loaded. Only necessary for raw data types,
                    i.e. "txt" and "bin".
                - dtype: The data type of the data array being loaded. Only necessary for raw data types,
                    i.e. "txt" and "bin".

            Raises:
                - An error if an invalid attribute is supplied.
                - An error if an ROI is not supplied for an ROI specific attribute.
        """
        # Create list of possible attributes
        general_attributes = np.array(['slices', 'dose_grid', 'dv', 'roi_names', 'roi_names_native', 'm_trans', 'voxel_coords', 'slice_locations'])
        roi_specific_attributes = np.array(['volume_mask', 'surface_mask', 'contour_arrays', 'contour_image_inds'])
        valid_attributes = np.concatenate([general_attributes, roi_specific_attributes])

        # Check and process input attributes to be exported
        if attribute not in valid_attributes:
            raise Exception('Invalid attribute "{0}" supplied for loading. Valid attributes are {1}'.format(attribute, valid_attributes))

        # Check and process input ROIs to be exported
        if roi is None and attribute in roi_specific_attributes:
            raise Exception('ROI must be supplied for attribute "{0}"'.format(attribute))

        # Load attribute data
        data_array = load_array(file_path, file_format=file_format, shape=shape, dtype=dtype)

        # Set data in object attribute
        if attribute in general_attributes:
            setattr(self, attribute, data_array)
        elif attribute in roi_specific_attributes:
            # Get dictionary of attribute values, or create a new dictionary if none exists
            attr_dict = getattr(self, attribute)
            if attr_dict is None:
                attr_dict = {}

            # Get current list of ROI names, or create a new list if none exists. Add new ROI to list if not already present.
            roi_names = self.roi_names
            if roi_names is None:
                roi_names = []
            if roi not in roi_names:
                roi_names.append(roi)

            # Save data
            attr_dict[roi] = data_array
            setattr(self, attribute, attr_dict)
            self.roi_names = roi_names


def load_array(file_path, file_format=None, shape=None, dtype=None):
    """ Load data array from disk.

    Load the data array stored at the specified file path into a numpy array.

        Parameters:
            - file_path: File path of data array to be loaded.
            - file_format: Format of file being loaded. Options are "npy" for NumPy format, "mat" for
                Matlab format, "nc" for NetCDF format, "bin" for binary format, and "txt" for
                text format. If None, an attempt will be made to detect a recognized file extension.
            - shape: The shape of the data array being loaded. Only necessary for raw data types,
                i.e. "txt" and "bin".
            - dtype: The data type of the data array being loaded. Only necessary for raw data types,
                i.e. "txt" and "bin".

        Returns:
            - data_array: A numpy array of loaded data, or -1 if load was unsuccessful.

        Raises:
            - An error if an invalid file format is supplied or no valid file format is detected.
    """
    # List valid file formats
    valid_formats = ['npy', 'mat', 'nc', 'txt', 'bin']

    # Get extension, check results
    if file_format is None:
        for valid_format in valid_formats:
            if file_path[-(len(valid_format) + 1):] == '.' + valid_format:
                file_format = valid_format
    if file_format not in valid_formats:
        raise NameError('Invalid file format detected, valid options are "{0}"'.format(valid_formats))

    # Load data
    data_array = -1
    if file_format is 'npy':
        data_array = np.load(file_path)
    elif file_format is 'mat':
        data_array = loadmat(file_path)['data_array']
    elif file_format is 'nc':
        f = netcdf.netcdf_file(file_path, 'r')
        data_array = np.copy(f.variables['data_array'].data)
        f.close()
    elif file_format is 'bin':
        data_array = np.fromfile(file_path, dtype=dtype).reshape(shape)
    elif file_format is 'txt':
        data_array = np.genfromtxt(file_path, dtype=dtype).reshape([shape[2], shape[0], shape[1]]).T.swapaxes(0, 1)

    return data_array


def export_array(array, file_path, file_format=None):
    """ Export numpy data array.

    Export desired numpy data array to file.

        Parameters:
            - array: Array to be exported.
            - file: File path to which data array should be written.
            - format: Output format for data files. Options are "npy" for NumPy format, "mat" for
                Matlab format, "nc" for NetCDF format, "bin" for binary format, and "txt" for
                comma separated text format (CSV).

        Raises:
            - An error if an invalid format is supplied.
    """
    # List valid file formats
    valid_formats = ['npy', 'mat', 'nc', 'txt', 'bin']

    # Get extension, check results
    if file_format is None:
        for valid_format in valid_formats:
            if file_path[-(len(valid_format) + 1):] == '.' + valid_format:
                file_format = valid_format
    if file_format not in valid_formats:
        raise NameError('Invalid file format detected, valid options are "{0}"'.format(valid_formats))

    # Make sure array is numpy array
    array = np.array(array)

    # Add file extension if necessary
    if file_path[-(1 + len(file_format)):] != '.' + file_format:
        file_path += '.' + file_format

    # Export data
    if file_format is 'npy':
        np.save(file_path, array)
    elif file_format is 'mat':
        savemat(file_path, {'data_array': array})
    elif file_format is 'nc':
        f = netcdf.netcdf_file(file_path, 'w')
        f.createDimension('i', array.shape[0])
        f.createDimension('j', array.shape[1])
        f.createDimension('k', array.shape[2])
        if array.dtype.type is np.bool_:
            array = array.astype('int8')  # scipy.io.netcdf doesn't support boolean type
        arr = f.createVariable('data_array', array.dtype, ('i', 'j', 'k'))
        arr[:] = array
        f.close()
    elif file_format is 'bin':
        array.tofile(file_path)
    elif file_format is 'txt':
        arr_res = array.T.swapaxes(1, 2).reshape([array.shape[0] * array.shape[2], array.shape[1]])
        np.savetxt(file_path, arr_res, fmt='%s')


def resample_array(resamp_factor, array, return_type='float', bthresh=0.0, method='linear'):
    """ Resample data array.

    Resamples a 3D data array according to user supplied resampling factors.

        Parameters:
            - resamp_factor: A number or list of 3 numbers. If a single number then data arrays
                will be resampled to have new voxel size equal to the old voxel size uniformly scaled
                by the resampling factor along each dimension. If a list of 3 numbers the new voxel
                dimensions will be scaled individually according to the corresponding resampling
                factor in the list.
            - array: A 3D data array.
            - return_type: The type of the resampled data array to be returned.
            - bthresh: A number indicating the threshold to use for converting a resampled data
                array to boolean type.
            - method: Interpolation method used for resampling. Valid options are 'linear' or 'nearest'.

        Returns:
            - array_resamp: The resampled data array.

        Raises:
            - An error if invalid resampling factor is supplied.
    """
    # Get shape of original array
    array = np.array(array)
    nv = np.array(array.shape)

    # Check number of resampling factors
    try:
        num_factors = len(resamp_factor)
    except TypeError:
        resamp_factor = [resamp_factor] * 3
    else:
        if num_factors == 1:
            resamp_factor = [resamp_factor for i in range(3)]
        elif num_factors == 3:
            resamp_factor = [resamp_factor[i] for i in range(3)]
        else:
            raise Exception('Invalid number of resampling factors supplied. Either 1 or a list of 3 factors are valid.')

    # Check values of resampling factors
    resamp_factor = np.array(resamp_factor)
    if np.sum(np.floor(nv / resamp_factor) == 0) > 0:
        raise Exception('Resampling factor exceeds array size. Maximum values are {0}.'.format(nv))

    # Get current voxel locations along each axis
    ijk_subs = (np.arange(nv[0]) + 0.5, np.arange(nv[1]) + 0.5, np.arange(nv[2]) + 0.5)

    # Get Nx3 matrix with rows specifying new voxel locations
    i, j, k = np.meshgrid(np.arange(0, nv[0], resamp_factor[0]), np.arange(0, nv[1], resamp_factor[1]),
                          np.arange(0, nv[2], resamp_factor[2]), indexing='ij')
    ijk_coords_resamp = np.vstack([i.ravel(), j.ravel(), k.ravel()]).T + 0.5 * resamp_factor

    # Resample
    array_resamp = interpn(ijk_subs, array, ijk_coords_resamp, method=method, bounds_error=False, fill_value=None).reshape(i.shape)

    # Process the results
    if return_type == 'float':
        pass
    elif return_type == 'int':
        array_resamp = array_resamp.astype('int32')
    elif return_type == 'bool':
        array_resamp = array_resamp > bthresh
    else:
        raise ValueError('Invalid return_type supplied, valid options are "float", "int", or "bool".')

    return array_resamp


def calc_min_displacement(image_data, source_roi, target_roi, target_type='volume', disp_type='xyz'):
    """ Calculates the minimum displacement from points in one ROI to a second ROI.

    Calculate the minimum displacement from points in the ROI contained in `image_data` to a second ROI contained
    in `target_image_data`. Overlapping points have a displacement of 0. Minimum displacements are returned as an
    Nx3 flattened array where N is the number of voxels contained within the source ROI.

        Parameters:
            - image_data: A DicomRTImage object containing image slices, volume and surface contour masks,
                and volume and surface mask point arrays for the source and target ROIs.
            - source_roi: A string used as a key into the contour related attributes of the `image_data` object
                for data related to the source ROI. If None then the entire image volume is treated as the
                source ROI.
            - target_roi: A string used as a key into the contour related attributes of the `image_data` object
                for data related to the target ROI.
            - target_type: Type of mask to use to identify target voxels. Options are "volume" or "surface".
            - disp_type: The type of units in which the minimum displacement vectors should be returned.
                If "ijk" then displacement will be returned in voxel units with each vector ordered
                as {i (column direction) displacement, j (row direction) displacement, k (slice direction)
                displacement}. If "xyz" then displacement will be returned in distance (mm) units with each
                vector ordered as {x (row direction), y (column direction), z (slice direction)}.

        Returns:
            - self: Nx3 array of displacement vectors.

        Raises:
            - An error if an incorrect target_type is supplied.
            - An error if an incorrect disp_type is supplied.
    """
    # Make inverse target mask of correct type
    if target_type == 'volume':
        target_mask_inv = ~image_data.volume_mask[target_roi]
    elif target_type == 'surface':
        target_mask_inv = ~image_data.surface_mask[target_roi]
    else:
        raise Exception('Incorrect target mask type supplied, options are "surface" or "volume".')

    # Use Euclidean distance transform to find closest target voxel to other voxels
    subs_edt = distance_transform_edt(target_mask_inv, sampling=image_data.dv, return_distances=False, return_indices=True)

    # Get array of voxel subscripts
    grid_size_orig = target_mask_inv.shape
    subs_i, subs_j, subs_k = np.meshgrid(range(grid_size_orig[0]), range(grid_size_orig[1]), range(grid_size_orig[2]), indexing='ij')
    subs_ijk = np.concatenate([subs_i.reshape((1,) + subs_i.shape), subs_j.reshape((1,) + subs_j.shape), subs_k.reshape((1,) + subs_k.shape)], axis=0)

    # Get only target subscripts corresponding to ROI voxels
    if source_roi is None:
        source_mask = np.ones(grid_size_orig, dtype='bool')
    else:
        source_mask = image_data.volume_mask[source_roi]
    target_coords = subs_edt[:, source_mask].T
    source_coords = subs_ijk[:, source_mask].T

    # Convert voxel subscript coordinates to imaging coordinate system locations if desired
    if disp_type == 'xyz':
        target_coords = image_data.voxel_coords[target_coords[:, 0], target_coords[:, 1], target_coords[:, 2], :]
        source_coords = image_data.voxel_coords[source_coords[:, 0], source_coords[:, 1], source_coords[:, 2], :]
    elif disp_type != 'ijk':
        raise Exception('Incorrect displacement type supplied, valid options are "xyz" or "ijk".')

    # Get final displacements
    return source_coords - target_coords


def voxel_coords_grid(shape, m_trans):
    """ Creates 4D grid of voxel locations.

    Produces a 4D grid of voxel locations. The first 3 dimensions correspond to the i, j, and k indices of voxels
    within a stack of image slices. The last dimension holds the x, y, and z coordinate values for the voxel in
    the patient coordinate system. x is the direction of changing column index, y is the direction of changing
    row index, and z is the direction of changing slice index.

        Parameters:
            - shape: 2 element list. The first value is the number of slice rows, and the second value is the
                number of slice columns.
            - m_trans: 3D array of stacked 3x3 transformation matrices.

        Returns:
            - grid: A 4D array of voxel coordinates.

        Raises:
    """
    iv, jv = np.meshgrid(np.arange(shape[0]), np.arange(shape[1]), sparse=False, indexing='ij')
    grid = np.zeros(np.append(shape, [m_trans.shape[2], 3]), dtype='float32')
    for slice_num in range(m_trans.shape[2]):
        for comp in range(3):
            grid[:, :, slice_num, comp] = (m_trans[comp, 0, slice_num] * iv + m_trans[comp, 1, slice_num] * jv +
                                           m_trans[comp, 2, slice_num])
    return grid
