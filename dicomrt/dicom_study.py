from collections import namedtuple
from StringIO import StringIO

#pydicom support
try:
	import pydicom as dicom
except ImportError:
	import dicom

DicomStudyFiles = namedtuple('DICOM_STUDY', ['plan', 'ct', 'structures', 'dose' ])

SOP_CT		= "1.2.840.10008.5.1.4.1.1.2";
SOP_RTPLAN	= "1.2.840.10008.5.1.4.1.1.481.5";
SOP_RTSTRUCT	= "1.2.840.10008.5.1.4.1.1.481.3";
SOP_RTDOSE	= "1.2.840.10008.5.1.4.1.1.481.2";

class DicomStudy:
	'''container which sorts dicom files by type, and creates an ROI list with edit functions''' 
	def __init__(self,file_list):
		'''
		Input:
			file_list - a list of file paths or a list of file buffer points (esp. StringIO)

		Assumptions:
			There should be only one plan, structures, and dose file

		Members:
			files: a DicomStudyFiles object containing a list of ct files, as well as individual plan, structures, and dose files
			structure_list: a list of dicom labels associated with ROIs
		'''
		self.files = DicomStudyFiles([],[],[],[])

		for f in file_list:
			dcm = dicom.read_file(f)

			#rewind if buffer
			if isinstance(f,StringIO):
				f.seek(0)

			#find plan
			if dcm.SOPClassUID == SOP_RTPLAN:
				self.files.plan.append(f)

			#find dose
			if dcm.SOPClassUID == SOP_RTDOSE:
				self.files.dose.append(f)

			#find contours
			if dcm.SOPClassUID == SOP_RTSTRUCT:
				self.files.structures.append(f)

			#find and count CT files
			if dcm.SOPClassUID == SOP_CT:
				self.files.ct.append(f)

		temp_files = DicomStudyFiles(
				enforce_single_file(self.files.plan,'plan'),
				self.files.ct,
				enforce_single_file(self.files.structures,'structures'),
				enforce_single_file(self.files.dose,'dose')
			)

		self.files = temp_files

		#initialize structures list
		self.structure_list = get_structure_labels_and_idx(self.files.structures)

	def get_structure_index_list(self):
		return [x[1] for x in self.structure_list]

	def get_structure_label_list(self):
		return [x[0] for x in self.structure_list]

	def find_structure(self, idx_or_label):
		found = filter(lambda structure: idx_or_label in structure, self.structure_list)
		if len(found) == 1:
			return found[0]
		elif len(found) >1:
			raise Exception("more than one match was found for {}".format(idx_or_label))
		else:
			return None
	def rename_structure(self,idx_or_label, new_name):
		roi = self.find_structure(idx_or_label)
		roi[0] = str(new_name)

	def remove_structure(self,label_or_idx_list):
		''' removes items listed in label_or_idx_list from the structure_list '''
		if not isinstance(label_or_idx_list,list):
			label_or_idx_list = [label_or_idx_list] 

		for label_or_idx in label_or_idx_list:
			found = self.find_structure(label_or_idx)
			if found:
				self.structure_list.remove(found)
		for label, index in self.structure_list:
			print "{}, {}".format(label, index)

def enforce_single_file(file_list,label):
	''' checks that one and only one file exists in file_list and replaces the list with the instance, else error '''
	if len(file_list) == 1:
		#flatten list
		return file_list[0]
	elif len(file_list) > 1:
	 	print '[Note] More than one {} file was found in dicom file list.'.format(label)
	else:
	 	print 'Missing {} file'.format(label)

def get_structure_labels_and_idx(sructure_file):
	''' returns list of (dicom_ROI_name,dicom_ROI_number) tuples'''

	dcm = dicom.read_file(sructure_file)
	#rewind if StringIO buffer
	if isinstance(sructure_file,StringIO):
		sructure_file.seek(0)

	if dcm.SOPClassUID == SOP_RTSTRUCT:
		structure_list = []
		for idx in range(len(dcm.StructureSetROISequence)):
			structure_list.append([dcm.StructureSetROISequence[idx].ROIName,dcm.StructureSetROISequence[idx].ROINumber])
			print "{}, {}".format(structure_list[-1][0],structure_list[-1][1])
		return structure_list

	else:
		raise Exception("could not identify file as a dicom structure file.")