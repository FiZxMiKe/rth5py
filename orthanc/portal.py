
import RestToolbox
from time import time
from StringIO import StringIO


class Struct:
  '''The recursive class for building and representing objects with.'''
  def __init__(self, obj):
    for k, v in obj.iteritems():
      if isinstance(v, dict):
        setattr(self, k, Struct(v))
      else:
        setattr(self, k, v)
  def __getitem__(self, val):
    return self.__dict__[val]
  def __repr__(self):
    return '{%s}' % str(', '.join('%s : %s' % (k, repr(v)) for
      (k, v) in self.__dict__.iteritems()))

class Portal:

	def __init__(self,URL,username=None,password=None):
		
		self.URL = URL
		self.Patients = None

		if username and password:
			RestToolbox.SetCredentials(username,password)

	def loadPatientList(self):
		self.Patients = []

		for patient in RestToolbox.DoGet('{}/patients'.format(self.URL)):
			#get basic data
			self.Patients.append(Struct(RestToolbox.DoGet('%s/patients/%s' % (self.URL, patient))))
			studies = RestToolbox.DoGet('{}/patients/{}/studies'.format(self.URL,self.Patients[-1].ID))

			#populate study information
			for sidx, study_id in enumerate(self.Patients[-1].Studies):
				self.Patients[-1].Studies[sidx] = Struct(RestToolbox.DoGet('{}/studies/{}'.format(self.URL,study_id)))

	def getDicomStudy(self,study_id):
		dicom_buffers = []

		self.loadPatientList()

		series_list = RestToolbox.DoGet('{}/studies/{}/series'.format(self.URL,study_id))
		
		#benchmarking
		tic = time()
		total_bytes = 0

		for series in series_list:
			#get the instances
			s = Struct(series)
			for inst_id in s.Instances:
				dicom_buffers.append(StringIO(RestToolbox.DoGet('{}/instances/{}/file'.format(self.URL,inst_id))))
    			total_bytes += dicom_buffers[-1].len

		sec = time() - tic

		print "{} bytes in {} seconds ({} MB/s)".format(total_bytes,sec,float(total_bytes/2**20)/sec)

		return dicom_buffers