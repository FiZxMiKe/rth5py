#fluenceMap.py
import numpy as np
import glob, struct, os
import matplotlib.pyplot as plt


#custom python libs (SKIPPED FOR NOW)
#from rtHelper import FluenceCalc
class FluenceCalc(object):
	pass

from dicomrt.rt_plan import mlcBeam

# ASSUMPTIONS:
# colimator at fixed angle per beam
# couch motion ignored
# first control point of beam contains jaw positions in BLD[0] and BLD[1], and MLCX in BLD[3]
# all other control points have MLCX data in BLD[0]


# class mlcBeam(object):
# 	'''holds data for each beam in plan'''
	
# 	def __init__(self):
# 		# first dimension of arrays represent individual beams
# 		self.ControlPointCount = 0
# 		self.Name = None
# 		self.cp_idx = None #used for vmat data to record control point at the start of a new subbeam
# 		self.Number = None #beam number
# 		self.BeamEnergy = -1.0
# 		self.MU = [] #this is typically cumulative MU
# 		self.cpMU = [] #this is a re-calculated per-control point MU
# 		self.TotMU = None #actually meterset for the beam
# 		self.BankA = []
# 		self.BankB = []
# 		self.X1 = []
# 		self.X2 = []
# 		self.Y1 = []
# 		self.Y2 = []
# 		#isocenters (assumed static so only [0] get ref'ed)
# 		self.isoX = []
# 		self.isoY = []
# 		self.isoZ = []
# 		#rotations
# 		self.GantryAng = []
# 		self.CollimatorAng = []
# 		self.CouchAng = []
# 		self.map = None
# 		self.CP_maps = None #{'cp':cp,'mu':fmapData.cpMU[cp],'map':FluMapLibObj.get_map()}

# 	def __str__(self):
# 		return "Beam %s, %i control pts, %f MU" % (self.Name,self.ControlPointCount,self.MU[-1])

# 	def __unicode__(self):
# 		return "Beam %s, %i control pts, %f MU" % (self.Name,self.ControlPointCount,self.MU[-1])



# 	def mapAsText(self,fileObj):
# 		#buff = cStringIO.cStringIO()
# 		mapString = ''
# 		if self.map is not None:
# 			#leaf data is stored "upside down"
# 			dimX = self.map.shape[1]
# 			leafRange = range(dimX)
			
# 			fmapT = self.map.transpose()
# 			np.savetxt(fileObj,fmapT,'%.4f')

# 			# for lp in leafRange:
# 			# 	strArray = [("%.4f " % n) if n > 0. else "0.0 " for n in fmapT[lp]]
# 			# 	for num in strArray:
# 			# 		mapString += num
# 			# 	mapString += '\n'

# 			# return mapString

# 		else:
# 			raise Exception('map data has not been generated yet')


class Fluence(object):
	'''the main fluence map class, loads all beams, holds plan data and map specs, exports beams'''
	#some constants:
	GANTRY_STATIC_THRESHOLD = 0.001 #degrees #the
	GANTRY_STATIC_BIN_WIDTH = 0.5 #degrees

	modality = None #set after convertIfIMRT()
	#default values
	resolution = None       # pixels parallel to MLC motion
	minLeafWidth_in_px = None #5 # number of pixels per smallest leaf
	transmission = 0.019   # MLC leaf percentage as decimal (NOTE: not passed to c code fluence generator)
	fieldSize = 40.        # cm (square)

	minLeafWidth = None  #fixed: one pixel per smallest leaf (found in dicom plan)
	leafBoundaries = None
	planFilePath = None
	planFileFolder = None
	fmaps = None # array of fluence maps
	mlcData = None

	leafWidths = None # list of leaf widths in units of ?cm?
	leafPixelWidths = None # a list of the width of each leaf in units of pixels (along y dimension)

	fractionMU = None #MU for the entire fraction
	#beamDoses = None # currently not used
	#beamMUs = None #eliminated

	dicomBeams = None #holds beam data from plan as dictionary indexed by beam number!

	def __init__(self, leafBoundaries_mm, resolution = 200, minLeafPxWidth = 4):
		#resolution is number of pixels along the x axis

		self.resolution = resolution
		self.minLeafWidth_in_px = minLeafPxWidth

		#find the RT plan in the dicom_folder
		# self.planFolder = dicom_folder
		# self.fmaps = []
		self.leafPixelWidths = []
		self.beamDoses = {}
		self.beamMUs = {}

		self.leafBoundaries = self.fieldSize/2. + np.array(leafBoundaries_mm) / 10. #convert to cm in corner coords
				
		#assure leafs are in field
		self.leafBoundaries = map(lambda x: x if x > 0. else 0.0, self.leafBoundaries)
		self.leafBoundaries = map(lambda x: x if x <= self.fieldSize else self.fieldSize, self.leafBoundaries)

		#convert boundaries and widths to pixels (perpendicular to leaf motion direction)
		self.minLeafWidth = self.fieldSize #surely larger than any leaf
		self.leafWidths = []
		#first find minimum width
		for l in range(len(self.leafBoundaries)-1):
			testWidth = self.leafBoundaries[l+1] - self.leafBoundaries[l]
			self.leafWidths.append(testWidth)
			if testWidth < self.minLeafWidth:
				self.minLeafWidth = testWidth

		#now build pixel width array
		for lw in self.leafWidths:
			if lw % self.minLeafWidth == 0.:
				self.leafPixelWidths.append(self.minLeafWidth_in_px*int(lw/self.minLeafWidth))
			else:
				raise Exception("Odd leaf width encountered. Leaf size is not multiple of smallest leaf (%f)" % self.minLeafWidth)

		#calculate number of rows in resultant maps
		self.rowRes = np.sum(self.leafPixelWidths)
		print "Number of Rows in Map: %i" % self.rowRes

	def get_log_type(self,logFolder):
		# here = os.getcwd()
		# os.chdir(logFolder)
		
		if len(glob.glob(logFolder+"/A*.dlg")) > 0:
			#dynalog
			# os.chdir(here) #back to root folder
			return 'dynalog'

		elif len(glob.glob(logFolder+"/*.bin")) > 0:
			#trajectory log is default
			# os.chdir(here) #back to root folder
			return 'trajectory'
		else:
			# os.chdir(here) #back to root folder
			raise Exception("No .dlg or .bin files found in " + logFolder)

	def jaws_phsp_format(self,X2,X1,Y2,Y1,SAD=100.,print_header=False):
		'''Generates jaw file in format for gDPM_phsp'''
		#Note 1 and 2 jaws are flipped (gDPM coordinates)!?

		#varian pdf
		yzf = 27.89 #28.16
		yzb = yzf + 7.77
		xzf = yzb + 0.95
		xzb = xzf + 7.80

		#zhen's file
		#yzf = 27.90404
		#yzb = yzf + 7.86394
		#xzf = yzb + 0.28849
		#xzb = xzf + 7.86394

		xbp = X1/SAD*xzb
		xfp = X1/SAD*xzf
		xbn = X2/SAD*xzb #this was flipped when loading from file
		xfn = X2/SAD*xzf #this was flipped when loading from file

		ybp = Y1/SAD*yzb #at read in... this was flipped
		yfp = Y1/SAD*yzf #at read in... this was flipped
		ybn = Y2/SAD*yzb
		yfn = Y2/SAD*yzf

		return_str = ""
		if print_header:
			return_str += "{} {}\n".format(yzf,yzb) #yzb
			return_str += "{} {}\n".format(xzf,xzb) #xzb
		else:
			return_str += "{} {} {} {}\n".format(yfp,ybp,yfn,ybn) #ybp #ybn
			return_str += "{} {} {} {}\n".format(xfp,xbp,xfn,xbn) #xbp #xbn

		return return_str

	def export_maps(self,format,prefix='fluence'):
		'''exports maps either in .png format (plot), or in text format for gDPM'''

		print "Exporting maps to: %s" % prefix

		if format == 'gDPM_PHSP':
			#prepare output for GDPM in PHSP mode
			#for gDPM we give the mean values for "fixed" items like jaw and angles
			

			#beamIdx = 1
			for fmapData in self.mlcData:

				#check for dynamic jaws within segment
				if np.std(fmapData.X1) > 0.001 or np.std(fmapData.X2) > 0.001 or np.std(fmapData.Y1) > 0.001 or np.std(fmapData.Y2) > 0.001 :
					raise Exception("[Export Fluence Maps (gDPM PHSP)] Dynamic Jaws Detected! Dynamic Jaws are not supported at this time.")

				#fluence_map.txt file (row by row) raw data in text format
				mapFile = open(prefix+"_map{}.txt".format(fmapData.Number),'w')

				#fluence_jaw.txt file
				jawFile = open(prefix+"_jaw{}.txt".format(fmapData.Number),'w')

				#fluence.ang file
				angFile = open(prefix+"{}.ang".format(fmapData.Number),'w')
				#numBeams:352 fieldSize:40.0,40.0 beamletRes(y,x):0.5,0.2
				angFile.write("numBeams:%i fieldSize:%2.3f,%2.3f beamletRes(y,x):%.3f,%.3f\n" % (1,self.fieldSize,self.fieldSize,self.minLeafWidth/float(self.minLeafWidth_in_px),self.fieldSize/self.resolution))
				#beam_index gantry_angel collimater_angel Couch_angel
				angFile.write("beam_index gantry_angel collimater_angel Couch_angel\n")

				fmapData.mapAsText(mapFile)
				#converting from varian scale to ICRU?
				angFile.write("%i %.6f %.6f %.6f\n" % (1,np.mean(fmapData.GantryAng), np.mean(fmapData.CollimatorAng), np.mean(fmapData.CouchAng) ))
				
				# a = np.mean(fmapData.X2) - np.mean(fmapData.X1)
				# b = np.mean(fmapData.Y2) - np.mean(fmapData.Y1)
				# effective_field = 2.*a*b/(a+b)
				jawFile.write(self.jaws_phsp_format(np.mean(fmapData.X1), np.mean(fmapData.X2), np.mean(fmapData.Y1), np.mean(fmapData.Y2),print_header=True)) #prints only header
				jawFile.write(self.jaws_phsp_format(np.mean(fmapData.X1), np.mean(fmapData.X2), np.mean(fmapData.Y1), np.mean(fmapData.Y2))) #SAD can be passed also
				#jawFile.write(self.jaws_phsp_format(-5.,5.,-5.,5.)) #SAD can be passed also
				#beamIdx += 1

				# f = open(prefix+"_fluence_logfile_map.txt",'w')
				# f.write(mapFileIO.getvalue())
				mapFile.close()
				# f.close()

				# f = open(prefix+"_fluence_logfile_jaw.txt",'w')
				# f.write(jawFileIO.getvalue())
				jawFile.close()
				# f.close()

				# f = open(prefix+"_fluence_logfile.ang",'w')
				# f.write(angFileIO.getvalue())
				angFile.close()
				# f.close()

		elif format == 'gDPM_PHSP_for_arcs': #group like energies (or maybe by arc id)
			#prepare output for GDPM
			#for gDPM we give the mean values for "fixed" items like jaw and angles

			#self.beam_energies_list = []
			self.beam_energies_dict = {} # will have dict[energy] = [list of beams]
			#group beams by energy
			for fmapData in self.mlcData:
				if fmapData.BeamEnergy not in self.beam_energies_dict.keys() :
					self.beam_energies_dict[fmapData.BeamEnergy]=[fmapData,]
				else:
					self.beam_energies_dict[fmapData.BeamEnergy].append(fmapData)

			print self.beam_energies_dict

			for beam_energy in self.beam_energies_dict.keys():

				#fluence_map.txt file (row by row) raw data in text format
				mapFile = open(prefix+"_map{}.txt".format(beam_energy),'w')

				#fluence_jaw.txt file
				jawFile = open(prefix+"_jaw{}.txt".format(beam_energy),'w')
				jawFile.write(self.jaws_phsp_format(0.0,0.0,0.0,0.0,print_header=True)) #prints only header
				#beamIdx += 1

				#fluence.ang file
				angFile = open(prefix+"{}.ang".format(beam_energy),'w')
				#numBeams:352 fieldSize:40.0,40.0 beamletRes(y,x):0.5,0.2
				angFile.write("numBeams:%i fieldSize:%2.3f,%2.3f beamletRes(y,x):%.3f,%.3f\n" % (len(self.beam_energies_dict[beam_energy]),self.fieldSize,self.fieldSize,self.minLeafWidth/float(self.minLeafWidth_in_px),self.fieldSize/self.resolution))
				angFile.write("beam_index gantry_angel collimater_angel Couch_angel\n")

				beamIdx = 1
				for beam in self.beam_energies_dict[beam_energy]:
					#beam_index gantry_angel collimater_angel Couch_angel

					beam.mapAsText(mapFile)

					#converting from varian scale to ICRU?
					angFile.write("%i %.6f %.6f %.6f\n" % (beamIdx, np.mean(beam.GantryAng), np.mean(beam.CollimatorAng), np.mean(beam.CouchAng) ))
					jawFile.write(self.jaws_phsp_format(np.mean(beam.X1), np.mean(beam.X2), np.mean(beam.Y1), np.mean(beam.Y2))) #SAD can be passed also
					beamIdx += 1

				mapFile.close()
				jawFile.close()
				angFile.close()


		elif format == 'gDPM':
			#prepare output for GDPM
			#for gDPM we give the mean values for "fixed" items like jaw and angles
			
			#fluence_map.txt file (row by row) raw data in text format
			mapFile = open(prefix+"_map.txt",'w')

			#fluence_jaw.txt file
			jawFile = open(prefix+"_jaw.txt",'w')
			#beam_index X1 X2 Y1 Y2 Effective (2ab/(a+b))
			jawFile.write("beam_index X1 X2 Y1 Y2 Effective\n") #ICRU

			#fluence.ang file
			angFile = open(prefix+".ang",'w')
			#numBeams:352 fieldSize:40.0,40.0 beamletRes(y,x):0.5,0.2
			angFile.write("numBeams:%i fieldSize:%2.3f,%2.3f beamletRes(y,x):%.3f,%.3f\n" % (len(self.mlcData),self.fieldSize,self.fieldSize,self.minLeafWidth/float(self.minLeafWidth_in_px),self.fieldSize/self.resolution))
			#beam_index gantry_angel collimater_angel Couch_angel
			angFile.write("beam_index gantry_angel collimater_angel Couch_angel\n")

			beamIdx = 1
			for fmapData in self.mlcData:
				fmapData.mapAsText(mapFile)
				#converting from varian scale to ICRU?
				angFile.write("%i %.6f %.6f %.6f\n" % (beamIdx, np.mean(fmapData.GantryAng), np.mean(fmapData.CollimatorAng), np.mean(fmapData.CouchAng) ))
				
				a = np.mean(fmapData.X2) - np.mean(fmapData.X1)
				b = np.mean(fmapData.Y2) - np.mean(fmapData.Y1)
				effective_field = 2.*a*b/(a+b)
				jawFile.write("%i %.3f  %.3f  %.3f  %.3f  %.3f\n" % ( beamIdx, np.mean(fmapData.X1), np.mean(fmapData.X2), np.mean(fmapData.Y1), np.mean(fmapData.Y2), effective_field ))
				beamIdx += 1

			# f = open(prefix+"_fluence_logfile_map.txt",'w')
			# f.write(mapFileIO.getvalue())
			mapFile.close()
			# f.close()

			# f = open(prefix+"_fluence_logfile_jaw.txt",'w')
			# f.write(jawFileIO.getvalue())
			jawFile.close()
			# f.close()

			# f = open(prefix+"_fluence_logfile.ang",'w')
			# f.write(angFileIO.getvalue())
			angFile.close()
			# f.close()

		elif format == 'gDPM_per_arc': # or format == 'gDPM_per_beam':
			#older beam model, but newer handling of isocenter

			#self.beam_energies_list = []
			self.beam_dict = {} # will have dict[energy] = [list of beams]
			#group beams by energy
			for fmapData in self.mlcData:
				if fmapData.Number not in self.beam_dict.keys() :
					self.beam_dict[fmapData.Number]=[fmapData,]
				else:
					self.beam_dict[fmapData.Number].append(fmapData)

			# print self.beam_dict


			for beam_num in self.beam_dict.keys():

				#save isocenter
				bm = self.beam_dict[beam_num][0] #assuming same beam iso for entire beam or arc!!!
				f = open(prefix+"_isocenter{}.xargs".format(beam_num),'w')
				f.write("--Isocenter=%f\n" % bm.isoX[0])
				f.write("--Isocenter=%f\n" % bm.isoY[0])
				f.write("--Isocenter=%f\n" % bm.isoZ[0])
				f.close()
				print "[export_maps] Isocenter: {}, {}, {}".format(bm.isoX[0],bm.isoY[0],bm.isoZ[0])

				#fluence_map.txt file (row by row) raw data in text format
				mapFile = open(prefix+"_map{}.txt".format(beam_num),'w')

				#fluence_jaw.txt file
				jawFile = open(prefix+"_jaw{}.txt".format(beam_num),'w')
				jawFile.write("beam_index X1 X2 Y1 Y2 Effective\n") #ICRU
				#beamIdx += 1

				#fluence.ang file
				angFile = open(prefix+"{}.ang".format(beam_num),'w')
				#numBeams:352 fieldSize:40.0,40.0 beamletRes(y,x):0.5,0.2
				angFile.write("numBeams:%i fieldSize:%2.3f,%2.3f beamletRes(y,x):%.3f,%.3f\n" % (len(self.beam_dict[beam_num]),self.fieldSize,self.fieldSize,self.minLeafWidth/float(self.minLeafWidth_in_px),self.fieldSize/self.resolution))
				angFile.write("beam_index gantry_angel collimater_angel Couch_angel\n")

				for sub_beam_num, sub_beam in enumerate(self.beam_dict[beam_num]):
					sub_beam.mapAsText(mapFile)
					#converting from varian scale to ICRU?
					angFile.write("%i %.6f %.6f %.6f\n" % (sub_beam_num, np.mean(sub_beam.GantryAng), np.mean(sub_beam.CollimatorAng), np.mean(sub_beam.CouchAng) ))
					a = np.mean(sub_beam.X2) - np.mean(sub_beam.X1)
					b = np.mean(sub_beam.Y2) - np.mean(sub_beam.Y1)
					effective_field = 2.*a*b/(a+b)
					jawFile.write("%i %.3f  %.3f  %.3f  %.3f  %.3f\n" % ( sub_beam_num, np.mean(sub_beam.X1), np.mean(sub_beam.X2), np.mean(sub_beam.Y1), np.mean(sub_beam.Y2), effective_field ))

				mapFile.close()
				jawFile.close()
				angFile.close()




		elif format == 'gDPM_by_CP':
			#prepare output for GDPM (older version 6x only)
			#for gDPM we give the mean values for "fixed" items like jaw and angles


			#beamIdx = 1
			for fmapData in self.mlcData:
				for cp in fmapData.CP_maps: #{'cp':cp,'mu':fmapData.cpMU[cp],'map':FluMapLibObj.get_map()}

					#write beam specific isocenter
					f = open(prefix+"_isocenter{}_cp{}.xargs".format(fmapData.Number,cp['cp']),'w')

					f.write("--Isocenter=%f\n" % fmapData.isoX[0])
					f.write("--Isocenter=%f\n" % fmapData.isoY[0])
					f.write("--Isocenter=%f\n" % fmapData.isoZ[0])
					f.close()

					#fluence_map.txt file (row by row) raw data in text format
					mapFile = open(prefix+"_map{}_cp{}.txt".format(fmapData.Number,cp['cp']),'w')

					#fluence_jaw.txt file
					jawFile = open(prefix+"_jaw{}_cp{}.txt".format(fmapData.Number,cp['cp']),'w')
					#beam_index X1 X2 Y1 Y2 Effective (2ab/(a+b))
					jawFile.write("beam_index X1 X2 Y1 Y2 Effective\n") #ICRU

					#fluence.ang file
					angFile = open(prefix+"{}_cp{}.ang".format(fmapData.Number,cp['cp']),'w')
					#numBeams:352 fieldSize:40.0,40.0 beamletRes(y,x):0.5,0.2
					angFile.write("numBeams:%i fieldSize:%2.3f,%2.3f beamletRes(y,x):%.3f,%.3f\n" % (1,self.fieldSize,self.fieldSize,self.minLeafWidth/float(self.minLeafWidth_in_px),self.fieldSize/self.resolution))
					#beam_index gantry_angel collimater_angel Couch_angel
					angFile.write("beam_index gantry_angel collimater_angel Couch_angel\n")

					#save map in text format
					dimX = cp['map'].shape[1]
					leafRange = range(dimX)
			
					fmapT = cp['map'].transpose()
					np.savetxt(mapFile,fmapT,'%.4f')

					#converting from varian scale to ICRU?
					angFile.write("%i %.6f %.6f %.6f\n" % (1, np.mean(fmapData.GantryAng), np.mean(fmapData.CollimatorAng), np.mean(fmapData.CouchAng) ))

					a = np.mean(fmapData.X2) - np.mean(fmapData.X1)
					b = np.mean(fmapData.Y2) - np.mean(fmapData.Y1)
					effective_field = 2.*a*b/(a+b)
					jawFile.write("%i %.3f  %.3f  %.3f  %.3f  %.3f\n" % ( 1, np.mean(fmapData.X1), np.mean(fmapData.X2), np.mean(fmapData.Y1), np.mean(fmapData.Y2), effective_field ))
					#beamIdx += 1

					# f = open(prefix+"_fluence_logfile_map.txt",'w')
					# f.write(mapFileIO.getvalue())
					mapFile.close()
					# f.close()

					# f = open(prefix+"_fluence_logfile_jaw.txt",'w')
					# f.write(jawFileIO.getvalue())
					jawFile.close()
					# f.close()

					# f = open(prefix+"_fluence_logfile.ang",'w')
					# f.write(angFileIO.getvalue())
					angFile.close()
					# f.close()


		elif format == 'png':
			i = 0
			for fmapData in self.mlcData:
				plt.figure()
				plt.pcolor(fmapData.map)
				# plt.xlabel('cm')
				# plt.ylabel('leaf #')
				plt.title(fmapData.Name)
				plt.set_cmap('gray')
				cb = plt.colorbar()
				cb.set_label('MU')
				plt.savefig(prefix+"map_"+ str(i) + ".png")
				#plt.show()
				print "saving: "+prefix+"map_" + str(i) + ".png"
				i+=1


		else:
			raise Exception('Export format "' + format + '" not supported')

	def get_map(self,idx):
		'''returns a single map refereced by idx'''
		if self.mlcData[idx].map is not None:
			return self.mlcData[idx].map
		else:
			raise Exception('Map requested does not exist (maybe index outside of range)')

	def load_maps(self, logFolder = None, logtype = 'trajectory', use_leaf_rounding = False, block_jaw = True, by_CP = False):
		'''loads data from dicom or log file and converts leaf/jaw positions
		   into 2d pixel fluence arrays, stores data into self.mlcData[*].map
		   use_leaf_rounding calls external C funtion to compute fluence map'''
		print "Generating maps from MLC data..."

		# default mode is to read from DOCOM data
		if logFolder is not None:
			if logtype == 'trajectory':
				#get data from trajectory log file
				self.mlcData = self.load_traj_log(logFolder)
			elif logtype == 'dynalog':
				self.mlcData = self.load_dyna_log(logFolder)
			else:
				raise Exception('unrecognized "logtype"')

		#DICOM has been decoupled from this object
		# else:
		# 	if self.dicomBeams:
		# 		#now we convert from dictionary to array
		# 		#print self.dicomBeams
		# 		self.mlcData = []
		# 		for key, value in self.dicomBeams.iteritems():
		# 			self.mlcData.append(value)
		# 	else:
		# 		raise Exception('dicomBeams not set. Pass in logFolder or set self.dicomBeams')
		#first we want to do any interpolation required
		self.computeCPMU(interpolate=True) #this will update the cpMU values

		#we do this here since DICOM beams may not be loaded 'till now
		self.convertIfVMAT() #overwrites mlcData

		if use_leaf_rounding:
			FluMapLibObj = FluenceCalc(self.resolution,self.rowRes,self.fieldSize/self.resolution,self.minLeafWidth/self.minLeafWidth_in_px,self.leafPixelWidths)
			# FluMapLibObj = FluenceCalc(200,80,.2,.5, self.leafPixelWidths)
			# 	self.resolution, # num. of pixels in x dimension
			# 	self.rowRes, # num. of pixels in y dimension
			# 	self.fieldSize/self.resolution, #pixel size in x dimension
			# 	self.minLeafWidth # pixel size in y dimension
			# ) #(200,80,.2,.5)

		for fmapData in self.mlcData:
			#for each "BEAM"

			fmapData.map = np.zeros((self.rowRes,self.resolution),dtype=np.float32)

			if by_CP:
				fmapData.CP_maps = []

			# print "Processing fluence for beam {}".format(fmapData.Number)
			# print fmapData.ControlPointCount

			#prepare lengths for indexing condition
			BankA_length = len(fmapData.BankA)
			BankB_length = len(fmapData.BankB)
			X1_length = len(fmapData.X1)
			X2_length = len(fmapData.X2)
			Y1_length = len(fmapData.Y1)
			Y2_length = len(fmapData.Y2)

			#sanity check (the only acceptable lenghts are total CP count or One)
			if BankA_length != 1 and BankA_length < fmapData.ControlPointCount:
				raise Exception("Unrecognized Control Point Configuration")
			if BankB_length != 1 and BankB_length < fmapData.ControlPointCount:
				raise Exception("Unrecognized Control Point Configuration")
			if X1_length != 1 and X1_length < fmapData.ControlPointCount:
				raise Exception("Unrecognized Control Point Configuration")
			if X2_length != 1 and X2_length < fmapData.ControlPointCount:
				raise Exception("Unrecognized Control Point Configuration")
			if Y1_length != 1 and Y1_length < fmapData.ControlPointCount:
				raise Exception("Unrecognized Control Point Configuration")
			if Y2_length != 1 and Y2_length < fmapData.ControlPointCount:
				raise Exception("Unrecognized Control Point Configuration")


			for cp in range(0,fmapData.ControlPointCount):
				if fmapData.cpMU[cp] > 0.:

					if by_CP:
						#generates a map for each control point
						if use_leaf_rounding:
							FluMapLibObj.add_to_map( #self.makeMap( #
								1.0, #fmapData.cpMU[cp],
								fmapData.BankA[cp if BankA_length > 1 else 0],
								fmapData.BankB[cp if BankB_length > 1 else 0],
								fmapData.X1[cp if X1_length > 1 else 0],
								fmapData.X2[cp if X2_length > 1 else 0],
								fmapData.Y1[cp if Y1_length > 1 else 0],
								fmapData.Y2[cp if Y2_length > 1 else 0],
							    block_jaw
							)
							cp_map = FluMapLibObj.get_map()
							FluMapLibObj.reset_map() #set to zero again

						else:
							cp_map = self.makeMap( #
								1.0, # fmapData.cpMU[cp],
								fmapData.BankA[cp if BankA_length > 1 else 0],
								fmapData.BankB[cp if BankB_length > 1 else 0],
								fmapData.X1[cp if X1_length > 1 else 0],
								fmapData.X2[cp if X2_length > 1 else 0],
								fmapData.Y1[cp if Y1_length > 1 else 0],
								fmapData.Y2[cp if Y2_length > 1 else 0],
							    block_jaw
							)

						if "VMAT" in fmapData.Name:
							# print "cp%i, mu:%f" % (fmapData.cp_idx,fmapData.cpMU[cp])
							#we need to use the "subbeam number"
							fmapData.CP_maps.append({'cp':fmapData.cp_idx,'mu':fmapData.cpMU[cp],'map':cp_map})
						else:
							# print "cp%i, mu:%f" % (cp,fmapData.cpMU[cp])
							fmapData.CP_maps.append({'cp':cp,'mu':fmapData.cpMU[cp],'map':cp_map})

					else:
						# print "cp%i, mu:%f" % (cp,fmapData.cpMU[cp])
						#for all cases
						# we are carefull to only index where data exists
						# it could be the case that 
						if use_leaf_rounding:
							FluMapLibObj.add_to_map( #self.makeMap( #
								fmapData.cpMU[cp],
								fmapData.BankA[cp if BankA_length > 1 else 0],
								fmapData.BankB[cp if BankB_length > 1 else 0],
								fmapData.X1[cp if X1_length > 1 else 0],
								fmapData.X2[cp if X2_length > 1 else 0],
								fmapData.Y1[cp if Y1_length > 1 else 0],
								fmapData.Y2[cp if Y2_length > 1 else 0],
							    block_jaw
							)
						else:
							fmapData.map += self.makeMap( #
								fmapData.cpMU[cp],
								fmapData.BankA[cp if BankA_length > 1 else 0],
								fmapData.BankB[cp if BankB_length > 1 else 0],
								fmapData.X1[cp if X1_length > 1 else 0],
								fmapData.X2[cp if X2_length > 1 else 0],
								fmapData.Y1[cp if Y1_length > 1 else 0],
								fmapData.Y2[cp if Y2_length > 1 else 0],
							    block_jaw
							)

			if use_leaf_rounding and not by_CP:
				fmapData.map = FluMapLibObj.get_map()
				FluMapLibObj.reset_map() #set to zero again


		# import pdb; pdb.set_trace()
		# del FluMapLibObj


	def computeCPMU(self, interpolate):
		# a nieve MU interpolation to distribute MU throughout control points
		# should only be used for sliding window or VMAT
		print "[computeCPMU] Re-ballancing control point MU to simulate VMAT or Sliding Window IMRT"
		for beam in self.mlcData:
			#make sure cpMU is empty
			beam.cpMU = []
			diff_previous = 0.
			diff_next = 0.

			last_idx = beam.ControlPointCount-1
			for cp_idx in range(0,beam.ControlPointCount):
				#the MU at a given control point is given by rectangle method
				#each cp recieves half of the difference from both the previous and next mu
				if cp_idx == 0:
					#for first control point we only look ahead
					diff_previous = 0.0
					diff_next = beam.MU[cp_idx+1] - beam.MU[cp_idx] #next minus this
				elif cp_idx == last_idx:
					diff_previous = beam.MU[cp_idx] - beam.MU[cp_idx-1] #this minus previous
					diff_next = 0.0
				else:
					diff_previous = beam.MU[cp_idx] - beam.MU[cp_idx-1] #this minus previous
					diff_next = beam.MU[cp_idx+1] - beam.MU[cp_idx] #next minus this

				if interpolate:
					# grab half the mu from each end
					beam.cpMU.append((diff_next+diff_previous)/2.)
				else:
					#do previous diff
					beam.cpMU.append(diff_previous)

	def convertIfVMAT(self):
		#creates a "beam" for every control point in an arc (if an arc is detected)
		#should only need to be called once

		#start out assuming IMRT until we are convinced otherwise
		self.modality = 'IMRT'
		vmatSubArcData = []

		#for each beam (or arc) in the current dataset 
		for beam in self.mlcData:

			if len(beam.GantryAng) <= 1:
				if len(beam.GantryAng) == 0:
					raise Exception("Gantry angle list empty for %s" % str(beam))
			# elif len(beam.GantryAng) > 1:
				#works well for DICOM data
				# but just to make sure we check that deviations exist
			elif abs(np.diff(beam.GantryAng)).max() > self.GANTRY_STATIC_THRESHOLD:
					#should work for LOG FILE data
					self.modality = 'VMAT'
					print "beam {} identified as arc".format(beam.Number)

	
			# for vmat plans, convert each control point (within angle tolerance) into it's own beam
			if self.modality == 'VMAT':

				#initialize start angle and new conrol point count
				temp_start_ang = beam.GantryAng[0]
				temp_newBeams_count = 0

				BankA_length = len(beam.BankA)
				BankB_length = len(beam.BankB)
				X1_length = len(beam.X1)
				X2_length = len(beam.X2)
				Y1_length = len(beam.Y1)
				Y2_length = len(beam.Y2)
				ColAng_length = len(beam.CollimatorAng)
				CouchAng_length = len(beam.CouchAng)

				for control_point in range(0,beam.ControlPointCount):

				#for pinnicle plans, MU is off by one??? Need to investigate interpolation here... interpolate mu??

					#starting out or when we reach the angle threshold
					if control_point == 0 or abs(temp_start_ang - beam.GantryAng[control_point]) > self.GANTRY_STATIC_BIN_WIDTH:
					
						#start a new "subArc beam"
						vmatSubArcData.append(mlcBeam())
						temp_start_ang = beam.GantryAng[control_point]
						if control_point == 0:
							temp_start_MU = beam.MU[0]
						else:
							temp_start_MU = beam.MU[control_point-1] #get difference from previous "beam"

						#set these when we start new beam
						vmatSubArcData[-1].TotMU = beam.TotMU #this is wrong if we expect this to represent the tot. MU for sub beam.
						vmatSubArcData[-1].Name = str(beam.Name) + " VMATsubBeam " + str(temp_newBeams_count) + ", angle:{}".format(temp_start_ang)
						vmatSubArcData[-1].cp_idx = temp_newBeams_count
						vmatSubArcData[-1].Number = beam.Number
						vmatSubArcData[-1].BeamEnergy = beam.BeamEnergy
						temp_newBeams_count += 1

					#copy control point
					vmatSubArcData[-1].MU.append(beam.MU[control_point]-temp_start_MU) #this keeps the MU in check when splittin up the VMAT control points
					vmatSubArcData[-1].cpMU.append(beam.cpMU[control_point])					
					vmatSubArcData[-1].GantryAng.append(beam.GantryAng[control_point])

					#these are handled special in case data is missing at a control point
					#typically all values are defined at leas at the first control point				

					vmatSubArcData[-1].CollimatorAng.append(beam.CollimatorAng[control_point if ColAng_length > 1 else 0])
					vmatSubArcData[-1].CouchAng.append(beam.CouchAng[control_point if CouchAng_length > 1 else 0])
					vmatSubArcData[-1].X1.append(beam.X1[control_point if X1_length > 1 else 0])
					vmatSubArcData[-1].X2.append(beam.X2[control_point if X2_length > 1 else 0])
					vmatSubArcData[-1].Y1.append(beam.Y1[control_point if Y1_length > 1 else 0])
					vmatSubArcData[-1].Y2.append(beam.Y2[control_point if Y2_length > 1 else 0])
					vmatSubArcData[-1].BankA.append(beam.BankA[control_point if BankA_length > 1 else 0])
					vmatSubArcData[-1].BankB.append(beam.BankB[control_point if BankB_length > 1 else 0])
					vmatSubArcData[-1].isoX.append(beam.isoX[0])
					vmatSubArcData[-1].isoY.append(beam.isoY[0])
					vmatSubArcData[-1].isoZ.append(beam.isoZ[0])
					vmatSubArcData[-1].ControlPointCount += 1


				self.mlcData = vmatSubArcData #replace old beam data if vmat




	def makeMap(self,mu, bankA, bankB, X1, X2, Y1, Y2, block_jaws=False):
		'''an internal function used to generate a single map from single beam information'''
		# all parameters are expeted to be defined on isocenter plane with respect to isocenter BEV origin
		# BankB = Right Leaves
		# BankA = Left Leaves
		# JawX = (X1_left, X2_right)
		# JawY = (Y1_top, Y2_bottom)

		#check leaf sizes
		if len(bankA) != len(bankB):
			raise Exception("Bank arrays not of equal length")

		#for each pair
		origin = self.fieldSize/2. #left boundary
		pixel_size = self.fieldSize/float(self.resolution) #in cm

		#this gives pixel index coordinates
		bankA_ = (np.array(bankA)+origin)/pixel_size
		bankB_ = (np.array(bankB)+origin)/pixel_size


		Atrunc = np.trunc(bankA_) #integer portion
		Afrac = bankA_ - Atrunc   #fractional portion
		Btrunc = np.trunc(bankB_)
		Bfrac = bankB_ - Btrunc

		#for each leaf pair
		fmap = self.transmission + np.zeros((self.rowRes,self.resolution))

		#enforce limits for out of field leafs?
		Btrunc[Btrunc >= self.resolution] = self.resolution - 1
		Bfrac[Btrunc >= self.resolution] = 1.
		Atrunc[Atrunc < 0] = 0
		Afrac[Afrac < 0] = 0.


		row = 0
		#fill in rows:
		for i in range(len(bankA)):

			# if both leaf ends are in same pixel
			if Atrunc[i] == Btrunc[i]:
				# we are sub pixel and should fill in proportional to the separation between the leaves
				# assuming the B bank is always to the left (smaller value in BEV)
				if Afrac[i] > Bfrac[i]:
					raise Exception('Leaf overlap encountered')
				else:
					fmap[row:row+self.leafPixelWidths[i],Atrunc[i]] = Bfrac[i]-Afrac[i]

			else:
				# here we fill in between the leaf edges
				# fmap[row:row+self.leafPixelWidths[i],Atrunc[i]+1:Btrunc[i]] = 1.0
				###### TESTING BELOW ######
				fmap[row:row+self.leafPixelWidths[i],Atrunc[i]+1:Btrunc[i]] = 1.0


				# then add the fractional portion to the pixel at the leaf edge
				###### TESTING OFF ######
				fmap[row:row+self.leafPixelWidths[i],Atrunc[i] ]  = 1. - Afrac[i]
				try:
					fmap[row:row+self.leafPixelWidths[i],Btrunc[i] ]  = Bfrac[i]
				except:
					raise Exception("index is %i, and Btrunc is %i, shape of fmap is: %i,%i"%(i,Btrunc[i],fmap.shape[0],fmap.shape[1]))
			row += self.leafPixelWidths[i]



		#block out jaws (assumes at least one pixel blocked!!):

		if block_jaws:
			#find index of leafs
			#index coords
			X1_ = (X1 + origin)/pixel_size
			X2_ = (X2 + origin)/pixel_size
			Y1_ = (Y1 + origin)/self.minLeafWidth * self.minLeafWidth_in_px
			Y2_ = (Y2 + origin)/self.minLeafWidth * self.minLeafWidth_in_px

			#handle out of field jaws:
			X1_ = X1_ if X1_ > 0. else 0.
			Y1_ = Y1_ if Y1_ > 0. else 0.

			X2_ = X2_ if X2_ < float(self.resolution - 1) else float(self.resolution - 1)
			Y2_ = Y2_ if Y2_ < float(self.rowRes - 1) else float(self.rowRes - 1)

			#whole numbers
			X1_idx = np.trunc(X1_)
			X2_idx = np.trunc(X2_)
			Y1_idx = np.trunc(Y1_)
			Y2_idx = np.trunc(Y2_)

			#zero out x jaw overlap:
			fmap[:,0:X1_idx] = 0.0
			fmap[:,X2_idx+1:] = 0.0

			###### TESTING OFF ######
			#compute sub-pixel overlap:
			fmap[:,X1_idx] = (1. - (X1_-X1_idx))*fmap[:,X1_idx]
			fmap[:,X2_idx] = (X2_-X2_idx)*fmap[:,X2_idx]

			#zero out y jaw overlap:
			fmap[0:Y1_idx,:] = 0.0
			fmap[Y2_idx+1:,:] = 0.0

			###### TESTING OFF ######
			#compute sub-pixel overlap:
			fmap[Y1_idx,:] = (1. - (Y1_-Y1_idx))*fmap[Y1_idx,:]
			fmap[Y2_idx,:] = (Y2_-Y2_idx)*fmap[Y2_idx,:] #this one has been tested to be okay?


		return mu*fmap



	def load_dyna_log(self,logFolder):
		#we require the DICOM data here so:
		if self.dicomBeams is None:
			raise Exception("dicom beams are desired to fill in some missing information in dyna log file ()")

		# FOLDER = 'DYN_LOG_FOLDER/'
		# here = os.getcwd()
		# os.chdir(logFolder)
		Afiles = []
		Bfiles = []

		for fname in glob.glob(logFolder+"/A*.dlg"):
			Afiles.append(fname)

		for fname in Afiles:
			print fname

		#verify that the B files exist
		files_missing = False

		for fname in Afiles:
			bfile = "B%s" % fname[1:]
			if not os.path.isfile(bfile):
				raise Exception("Missing B bank file: " % bfile)
			else:
				Bfiles.append(bfile)
			
		#process

		beamsArray = []

		for fidc in range(len(Afiles)):
			beamsArray.append(mlcBeam())

			aData = open(Afiles[fidc],'r')
			bData = open(Bfiles[fidc],'r')



			if aData.readline().rstrip() == 'B' and bData.readline().rstrip() == 'B':
				#files appear to be okay

				#skip further varifications, just read the info from
				patientLastName, patientFirstName, patientID = aData.readline().rstrip().split(',')
				BpatientLastName, BpatientFirstName, BpatientID = bData.readline().rstrip().split(',')



				if not patientLastName == BpatientLastName \
					or not patientFirstName == BpatientFirstName \
					or not patientID == BpatientID: #name and ID
					Exception("Inconsistant Patient Info")


				# this next line can also be "plan file name" for standalone implementation of dynalog file
				planUID, beamNumber   = aData.readline().rstrip().split(',')
				BplanUID, BbeamNumber = bData.readline().rstrip().split(',')

				if not planUID == BplanUID or not beamNumber == BbeamNumber:
					Exception("Inconsistant Plan and Beam Info")

				beamNumber = int(beamNumber)

				print "Plan UID: " + planUID
				print "Beam Number: %i" % beamNumber

				#skipping tolerance
				aData.readline().strip()
				bData.readline().strip()

				#mlc count
				mlcCount  = int(aData.readline())
				BmlcCount = int(bData.readline())

				if not mlcCount == BmlcCount: #not likely by this point
					Exception("Inconsistant MLC leaf count")

				print "MLC Leaf Count: %i" % mlcCount

				#MACHINE SCALE
				scale = int(aData.readline().strip())
				Bscale = int(bData.readline().strip())

				if not scale == Bscale:
					raise Exception("Inconsistant Scale between A and B file")
				
				if scale == 0:
					raise Exception("Varian Scale '0' currently not supported")

				if scale != 1:
					raise Exception("Scale '%i' not supported" % scale)


				#start reading in bulk data
				aArray = np.loadtxt(aData,delimiter=',')
				bArray = np.loadtxt(bData,delimiter=',')

				#these should be the same
				#
				meterset = aArray[:,0]
				metersetB = bArray[:,0]

				if sum(meterset != metersetB) > 0:
					raise Exception("Inconsistant Meterset Column (file A vs. B)")

				#normalized meterset (0 to 1)
				meterNorm = meterset/25000.

				#prevSegmentNumber = aArray[:,1]
				
				#beam hold assertions (0 = no hold, 1 = hold, 2 = carrage transition)
				# beamHoldState = aArray[:,2]
				# BbeamHoldState = bArray[:,2]
				# if sum(beamHoldState != BbeamHoldState) > 0:
				# 	raise Exception("Inconsistant Beam Hold States")
				
				#beam on (0 = off, 1 = on)
				beamOnState = aArray[:,3]
				BbeamOnState = bArray[:,3]
				if sum(beamOnState != BbeamOnState) > 0:
					raise Exception("Inconsistant Beam On States")
				
				#can be thought of as previous meterset goal (or gantry angle goal)
				prevSegmentDoseMeter = aArray[:,4]
				BprevSegmentDoseMeter = bArray[:,4]
				if sum(prevSegmentDoseMeter != BprevSegmentDoseMeter) > 0:
					raise Exception("Inconsistant prev. Segment Dose Meter column")

				#can be thought of as previous meterset goal (or gantry angle goal)
				nextSegmentDoseMeter = aArray[:,5]
				BnextSegmentDoseMeter = bArray[:,5]
				if sum(nextSegmentDoseMeter != BnextSegmentDoseMeter) > 0:
					raise Exception("Inconsistant next Segment Dose Meter column")

				#varian scale
				gantryAng = aArray[:,6] / 10.
				BgantryAng = bArray[:,6] / 10.
				if sum(gantryAng != BgantryAng) > 0:
					raise Exception("Inconsistant gantry angle column")

				#varian scale
				collimatorAng = aArray[:,7] / 10.
				BcollimatorAng = bArray[:,7] / 10.
				if sum(collimatorAng != BcollimatorAng) > 0:
					raise Exception("Inconsistant collimator angle column")

				#converted to CM
				Y1 = aArray[:,8] / 10.
				BY1 = bArray[:,8] / 10.
				if sum(Y1 != BY1) > 0:
					raise Exception("Inconsistant Y1 column")

				#converted to CM
				Y2 = aArray[:,9] / 10.
				BY2 = bArray[:,9] / 10.
				if sum(Y2 != BY2) > 0:
					raise Exception("Inconsistant Y2 column")

				#converted to CM
				X1 = aArray[:,10] / 10.
				BX1 = bArray[:,10] / 10.
				if sum(X1 != BX1) > 0:
					raise Exception("Inconsistant X1 column")

				#converted to CM
				X2 = aArray[:,11] / 10.
				BX2 = bArray[:,11] / 10.
				if sum(X2 != BX2) > 0:
					raise Exception("Inconsistant X2 column")

				#converted to CM
				carrageA_expected = aArray[:,12] / 1000.
				carrageB_expected = bArray[:,12] / 1000.
				#converted to CM
				carrageA_actual = aArray[:,13] / 1000.
				carrageB_actual = bArray[:,13] / 1000.
				
				# for each leaf: expected, actual, previous, next
				# converted to CM
				bankA_expected = aArray[:,14::4]/1000.
				bankB_expected = bArray[:,14::4]/1000.
				bankA_actual = aArray[:,15::4]/1000.
				bankB_actual = bArray[:,15::4]/1000.

				# plt.plot(bankA_expected[0])
				# plt.plot(bankA_actual[0])
				# plt.show()

				beamsArray[-1].ControlPointCount = aArray.shape[0]
				beamsArray[-1].MU = meterNorm*self.dicomBeams[beamNumber].TotMU # TAKEN FROM DICOM PLAN
				beamsArray[-1].Name = str(beamNumber)
				beamsArray[-1].Number = beamNumber
				beamsArray[-1].GantryAng = 180. -gantryAng
				beamsArray[-1].CollimatorAng = 180. - collimatorAng
				beamsArray[-1].CouchAng = self.dicomBeams[beamNumber].CouchAng # TAKEN FROM DICOM PLAN
				beamsArray[-1].isoX = self.dicomBeams[beamNumber].isoX[0] # TAKEN FROM DICOM PLAN (assumed static)
				beamsArray[-1].isoY = self.dicomBeams[beamNumber].isoY[0] # TAKEN FROM DICOM PLAN (assumed static)
				beamsArray[-1].isoZ = self.dicomBeams[beamNumber].isoZ[0] # TAKEN FROM DICOM PLAN (assumed static)
				beamsArray[-1].X1 = -X1
				beamsArray[-1].X2 = X2
				beamsArray[-1].Y1 = -Y1
				beamsArray[-1].Y2 = Y2
				beamsArray[-1].BankA = -bankB_actual*2. #NOT SURE WHY I AM MULTIPLYING BY 2 !!
				beamsArray[-1].BankB = bankA_actual*2.  #NOT SURE WHY I AM MULTIPLYING BY 2 !!
				beamsArray[-1].TotMU = self.dicomBeams[beamNumber].TotMU # TAKEN FROM DICOM PLAN

				pass
			else:
				raise Exception("Only version 'B' of dynalog file is supported")
			
		# back to origional path
		os.chdir(here)
		return beamsArray

	def load_traj_log(self,logFolder):
		#####
		#todo: get proper beam number (like the one that would match the DICOM plan) AND energy
		#####
		'''an internal function used to load leaf/jaw/mu information from a varian trajectory log file'''
		ls = glob.os.listdir(logFolder)

		binFiles = []

		beamDataArray = []

		for fname in ls:
			if fname.rfind('.bin') > 0:
				binFiles.append(logFolder+fname)

		for fname in binFiles:
			print fname
			
		#process
		for logFilePath in binFiles:

			logHeader = open(logFilePath.strip('.bin')+'.txt','r')
			beam_energy = None
			beam_number = None
			#get some info:
			for l in logHeader.readlines():
				try:
					key,val = map(lambda x: x.strip(), l.split(':'))
				except ValueError:
					key = ''
					val = ''
				key = key.strip()
				val = val.strip()
				if 'Energy' in key:
					beam_energy = float(val.strip('xX'))
					print "Found Beam Energy: {}".format(beam_energy)
				if 'Beam Number' in key:
					# print "Found Beam Number: {}".format(key)
					beam_number = int(val)
					print "Found Beam Number: {}".format(beam_number)

			logHeader.close()

			logData = open(logFilePath,'rb')

			#first we check first 5 bytes for valid log file
			if struct.unpack('5s11x',logData.read(16))[0] == 'VOSTL':
				print "Valid Trajectory File Found: '" + logFilePath + "'"
				#then we proceed
				version, headSize, sampInterval, nAxes = struct.unpack('<3s13xiii',logData.read(28))
				#version 'x.y' as string
				print "Version: " + version

				if float(version) != 2.1:
					raise Exception("trajectory log file version " + version + " not supported")
				#print "trajectory log file version " + version + " not supported"

				#header size '<i' little endian int - no mention of signed or unsigned
				# print "Header Size: %i" % headSize
				# print "Sampling Interval (ms): %i" % sampInterval
				# print "Number of Axes Sampled: %i" % nAxes

				axesIdxs = struct.unpack('<'+ str(nAxes) + 'i',logData.read(nAxes*4))
				axesLabels = []
				#build axes labels
				for idx in axesIdxs:
					if idx == 0:
						axesLabels.append("Collimator Rot.")
					if idx == 1:
						axesLabels.append("Gantry Rot.")

					if idx == 2:
						axesLabels.append("Jaw Y1")
					if idx == 3:
						axesLabels.append("Jaw Y2")
					if idx == 4:
						axesLabels.append("Jaw X1")
					if idx == 5:
						axesLabels.append("Jaw X2")

					if idx == 6:
						axesLabels.append("Couch Virt.")
					if idx == 7:
						axesLabels.append("Couch Long.")
					if idx == 8:
						axesLabels.append("Couch Lat.")
					if idx == 9:
						axesLabels.append("Couch Rot.")

					if idx == 40:
						axesLabels.append("MU")
					if idx == 41:
						axesLabels.append("Beam Hold")
					if idx == 42:
						axesLabels.append("Control Point")

					if idx == 50:
						axesLabels.append("MLC") #cm
					pass

				#number samples recorded per axis at each snapshot
				axesSampCounts = struct.unpack('<'+ str(nAxes) + 'i',logData.read(nAxes*4))

				#defines units
				axesScale = ''
				tempScale = struct.unpack('<i',logData.read(4))[0]
				if tempScale == 1:
					axesScale = 'VARIAN'
				elif tempScale == 2:
					axesScale = 'IEC_61217'
				else:
					axesScale = 'NOT_RECOGNIZED'

				#number of subbeams in this file
				subBeamCount = struct.unpack('<i',logData.read(4))[0]

				#is this file truncated (stopped recording snapshots after 60000)
				truncatedFile = bool(struct.unpack('<i',logData.read(4))[0])

				#snapshot count for this file
				snapshotCount = struct.unpack('<i',logData.read(4))[0]

				#mlc model string
				mlcModel = ''
				tempModel = struct.unpack('<i',logData.read(4))[0]
				if tempModel == 2:
					mlcModel = 'NDS_120'
				elif tempModel == 3:
					mlcModel = 'NDS_120_HD'
				else:
					mlcModel = str(tempModel)+':NOT_RECOGNIZED'

				#skip reserved spcae
				logData.read(1024-(64+nAxes*8))

				subBeams = {'ctrlPt':[],'mu':[],'radTime':[],'seqNum':[],'name':[]}
				for s in range(subBeamCount):
					# read the subbeam structure
					cp, mu, radTime, seq, sbName = struct.unpack('<iffi32s32x',logData.read(80))
					subBeams['ctrlPt'].append(cp)
					subBeams['mu'].append(mu)
					subBeams['radTime'].append(radTime)
					subBeams['seqNum'].append(seq)
					subBeams['name'].append(sbName.rstrip('\x00'))

				snapshots = {}
				for a in range(nAxes):
					snapshots[axesLabels[a]+" Expected"] = []
					snapshots[axesLabels[a]+" Actual"] = []



				# add snapshots to beamDataArray
				beamDataArray.append(mlcBeam())

				for s in range(snapshotCount):
					for a in range(nAxes):
						if axesSampCounts[a] == 1:
							snapshots[axesLabels[a]+" Expected"].append(struct.unpack('<f',logData.read(4))[0])
							snapshots[axesLabels[a]+" Actual"].append(struct.unpack('<f',logData.read(4))[0])
						else:
							N=axesSampCounts[a]
							temp = np.array(struct.unpack('<'+str(2*N)+'f',logData.read(2*N*4)))
							snapshots[axesLabels[a]+" Expected"].append(np.array(temp[0:2*N:2]))
							snapshots[axesLabels[a]+" Actual"].append(np.array(temp[1:2*N+1:2]))

					#fill in data
					MODE = ' Actual'
					beamDataArray[-1].ControlPointCount += 1

					beamDataArray[-1].MU.append(snapshots['MU' + MODE][s])

					beamDataArray[-1].X1.append(-snapshots['Jaw X1' + MODE][s]) #convert to isocenter coords from varian scale
					beamDataArray[-1].X2.append( snapshots['Jaw X2' + MODE][s])
					beamDataArray[-1].Y1.append(-snapshots['Jaw Y1' + MODE][s]) #convert to isocenter coords from varian scale
					beamDataArray[-1].Y2.append( snapshots['Jaw Y2' + MODE][s])

					beamDataArray[-1].BankB.append(snapshots['MLC' + MODE][s][2:62])
					beamDataArray[-1].BankA.append(-snapshots['MLC' + MODE][s][62:122])

					beamDataArray[-1].GantryAng.append(180.-snapshots['Gantry Rot.' + MODE][s])
					#print("Snapshot Angle:{}".format(snapshots['Gantry Rot.' + MODE][s]))
					beamDataArray[-1].CollimatorAng.append(180.-snapshots['Collimator Rot.' + MODE][s])
					beamDataArray[-1].CouchAng.append(180.-snapshots['Couch Rot.' + MODE][s])


				# beamDataArray[-1].BeamName
				beamDataArray[-1].BeamEnergy = beam_energy
				beamDataArray[-1].Name = subBeams['name'][0] #not sure how robust this is
				beamDataArray[-1].TotMU = subBeams['mu'][0] #not sure how robust this is
				
				print "Assuming Beam Number from Name {}".format(subBeams['name'][0])
				beam_number_test = int(subBeams['name'][0].split(":")[0])
				print "Beam Number Test: " + str(beam_number_test)
				beamDataArray[-1].Number = beam_number_test

				beamDataArray[-1].isoX = [self.dicomBeams[beam_number_test].isoX[0]] # TAKEN FROM DICOM PLAN (assumed static)
				beamDataArray[-1].isoY = [self.dicomBeams[beam_number_test].isoY[0]] # TAKEN FROM DICOM PLAN (assumed static)
				beamDataArray[-1].isoZ = [self.dicomBeams[beam_number_test].isoZ[0]] # TAKEN FROM DICOM PLAN (assumed static)


				print "Sequence Number: {}, Beam Number: {}".format(subBeams['seqNum'],beam_number)
				# print "Beam Name Data: {}, Beam Name Header: {}".format(subBeams['name'],beam_name)

				#print axesLabels
				# print "Axis Labels: " + str(axesLabels)
				# print "Sample Counts: " + str(axesSampCounts)	
				# print "Axes Scale: " + str(axesScale)
				# print "Subbeam Count: " + str(subBeamCount)
				# print "Truncated: " + str(bool(truncatedFile))
				# print "MLC Model: " + mlcModel
				print "Snapshot Count: " + str(snapshotCount)
				print "Subbeam Info: " + str(subBeams)
				print snapshotCount

		return beamDataArray

	# def savePlanXargs(self,prefix):
	# 	dp = dicom.read_file(self.planFilePath,force=True)
	# 	#assuming one fraction group
	# 	if len(dp.FractionGroups) == 1:
	# 		nFractions = dp.FractionGroups[0].NumberofFractionsPlanned
	# 		#PrescriptionDoseGy
	# 	else:
	# 		raise Exception('More than one fraction group found in DICOM plan')
