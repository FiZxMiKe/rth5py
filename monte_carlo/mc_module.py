import tempfile
import shutil
import numpy as np
import os
from dicomrt.rt_plan import mlcBeam
from fluence import Fluence
import subprocess
import time

GDPM_CONFIG_PATH='/home/michael/target/data/gDPM_input/'
BIN_PATH = '/home/michael/target/bin'

def run_mc(rt5, bounding_dataset, ct_group='ct/', mode='per_cp', mc_dir=None):
    """
    modes:
        "per_cp" mode is a fluence map and dose matrix created per control point

    """
    temp_dir = False  # just a switch for deleteing temp dir

    # create temporary folder
    if mc_dir is None:
        mc_dir = tempfile.mkdtemp()
        temp_dir = True
        print "creating temporary mc folder: {}".format(mc_dir)

    # save needed files:
    np.rollaxis(rt5[ct_group + '/density'], 2).astype('float32').tofile(mc_dir + '/density.fxyz')
    np.rollaxis(rt5[ct_group + '/materials'], 2).astype('int32').tofile(mc_dir + '/materials.ixyz')
    np.rollaxis(rt5[bounding_dataset], 2).astype('uint32').tofile(mc_dir + '/structure.ixyz')

    # generate fluence maps #

    # create fluence map object
    fluence = Fluence(leafBoundaries_mm=rt5['plan/leaf_locations'])  # defaults: resolution = 200, minLeafPxWidth = 4):
    fluence.mlcData = []

    # load in beam data
    # include conversion to CM
    for beam_num, rt5_beam in rt5['plan/beams'].iteritems():
        fluence.mlcData.append(mlcBeam())
        fluence.mlcData[-1].Number = beam_num

        fluence.mlcData[-1].Name = rt5_beam['description'].value
        fluence.mlcData[-1].BeamEnergy = rt5_beam['energy'].value
        fluence.mlcData[-1].MU = rt5_beam['mu'].value
        fluence.mlcData[-1].BankA = rt5_beam['mlcx_a'].value / 10.
        fluence.mlcData[-1].BankB = rt5_beam['mlcx_b'].value / 10.
        fluence.mlcData[-1].X1 = rt5_beam['jaw_x1'].value / 10.
        fluence.mlcData[-1].X2 = rt5_beam['jaw_x2'].value / 10.
        fluence.mlcData[-1].Y1 = rt5_beam['jaw_y1'].value / 10.
        fluence.mlcData[-1].Y2 = rt5_beam['jaw_y2'].value / 10.
        fluence.mlcData[-1].GantryAng = rt5_beam['gantry'].value
        fluence.mlcData[-1].CollimatorAng = rt5_beam['collimator'].value
        fluence.mlcData[-1].CouchAng = rt5_beam['couch'].value
        fluence.mlcData[-1].ControlPointCount = rt5_beam.attrs['control_point_count']

        # isocenter needs to be split up:
        rt5_isos = rt5_beam['isocenter'].value
        fluence.mlcData[-1].isoX = rt5_isos[:, 0]
        fluence.mlcData[-1].isoY = rt5_isos[:, 1]
        fluence.mlcData[-1].isoZ = rt5_isos[:, 2]

    # generate maps
    fluence.load_maps(by_CP=True)  # False is default

    # use this loop when generating fluence per cp:
    for f_beam in fluence.mlcData:
        for cp_data in f_beam.CP_maps:
            rt5.create_dataset('plan/fluence/{}/{}'.format(f_beam.Number, cp_data['cp']), cp_data['map'])

    fluence.export_maps('gDPM_by_CP', prefix=mc_dir + '/fluence')
    # fluence.export_maps('png',prefix=mc_dir+'/fluence') #broken


    #can break here for new function if needed


    return_array = []


    #run one monte_carlo:
    mc_start = time.time()
    # for beam in fm.mlcData:
    #     for cp in beam.CP_maps:
    #         #MC PASS

    if not os.path.isfile(mc_dir + "mc{}_cp{}Ave.dat".format(1,1)):#beam.Number,cp['cp'])):
        print "{} {}".format(1,1) #format(beam.Number,cp['cp'])
        start = time.time()
        return_array.append(gdpm(
            fluence_map_info=mc_dir + "fluence{}_cp{}.ang".format(1,1),
            fluence_map=mc_dir + "fluence_map{}_cp{}.txt".format(1,1),
            jaw_position_file=mc_dir + "fluence_jaw{}_cp{}.txt".format(1,1),
            smooth_result=True,
            voxels_bin=mc_dir + "plan_",
            output_prefix=mc_dir + "mc{}_cp{}".format(1,1),
            geo_source2axis = 100.0,
            gpu_id=0,
            accuracy_percent_threshold=0.05, #5%,
            accuracy_dose_threshold=0.5, #above 50%,
            # xargs_file_list = PROJ_FOLDER+"plan_dose_shift.xargs,"+PROJ_FOLDER+"fluence_isocenter{}_cp{}.xargs".format(1,1)
        ))
        print "Calc. Time:{} s".format(time.time()-start)
    #upsample dose grid
    # if not os.path.isfile(PROJ_FOLDER + "mc{}_cp{}Dose_ctres.dat".format(beam.Number,cp['cp'])):
    #     rsmcd = gresample()
    #     rsmcd.input_file = PROJ_FOLDER + "mc{}_cp{}Dose.dat".format(beam.Number,cp['cp']) #dose
    #     rsmcd.output_prefix = PROJ_FOLDER + "mc{}_cp{}Dose_ctres.dat".format(beam.Number,cp['cp']) # the file will be overwritten
    #     rsmcd.convert_HU = False #just do the dose grid
    #     rsmcd.xargs_file_list = PROJ_FOLDER + "plan_dose.xargs,"+PROJ_FOLDER+"plan_ct.xargs" #need at least one comma
    #     rsmcd.execute(nosave=True)


    print "total time = {} s".format(time.time()-mc_start)

    if temp_dir:
        print "deleting temporary mc folder: {}".format(mc_dir)
        shutil.rmtree(mc_dir)

    return return_array

def gdpm(
    # $ gDPM --help
    # --SourceModelFile=<file>
    # Sourse model file.
    # --EGS4GeometryFile=<file>
    # Use EGS4 Phantom input.
    # --BinaryVoxelFilePrefix=<file>
    # Used binary Voxel input.
    # * [VoxPrefix]dens.[fxyz|fyxz] containing floats
    # * [VoxPrefix]mat.[ixyz|iyxz] containing ints
    # * [VoxPrefix]struc.[ixyz|iyxz] containing ints
    # --XYZ                 Use X-major binary format.
    # * [VoxPrefix][dens|mat|struc].[f|i]xyz
    # --YXZ                 Use Y-major binary format.
    # * [VoxPrefix][dens|mat|struc].[f|i]yxz
    # --CTdimension, --Dimension=<x>
    # Number of image data elements (voxels).
    # * repeat argument for <y> and <z> in order.
    # --CTresolution, --Resolution=<x>
    # Physical size of image voxels (cm).
    # * repeat argument for <y> and <z> in order.
    # --CToffset, --Offset=<x>
    # Image offset from isocenter (cm).
    # * repeat argument for <y> and <z> in order.
    # --Isocenter=<x>       World coordinate isocenter (cm).
    # * repeat argument for <y> and <z> in order.
    # --SAD=<n>             Source to Axis Distance (cm).
    # --FluenceMapInfoFile=<file>
    # Setup and angle file.
    # --FluenceMapFile=<file>
    # In text format - transposed from matlab.
    # --OutputPrefix=<file> filename prefix for output.
    # -T, --TransposeOutput     Transposes output to match DICOM format
    # --DebugMode           Generates binary voxel output after reading.



    # from cgem-rt/TARGET
    # --NumberOfHistories=250000000
    # --GPU_ID=1
    # --Photons --SourceEnergy=6.0e6
    # --ElectronAbsorptionEnergy=200.0e3
    # --PhotonAbsorptionEnergy=50.0e3
    # --CompatibleFilePrefix=../../../dat/gDPM_input/pre4elec
    # --DoseToWaterConversionFile=../../../dat/gDPM_input/sfactor.dtw
    # --SourceModelFile=../../../dat/gDPM_input/source_new.src
    # --FluenceMapInfoFile=IMRT_fluence.ang
    # --FluenceMapFile=IMRT_fluence_map.txt
    # --BinaryVoxelFilePrefix=test_gResample_ct_
    # --XYZ
    # --OutputPrefix=test_gDPM_
    # --TransposeOutput

    # --SAD=100

    # --Dimension=181
    # --Dimension=115
    # --Dimension=186

    # --Resolution=2.500000
    # --Resolution=2.500000
    # --Resolution=2.500000

    # --Offset=-179.454418
    # --Offset=-162.313497
    # --Offset=-208.500000

    # --Isocenter=-5.371220
    # --Isocenter=3.417850
    # --Isocenter=-22.210880

    # from carrie (additional parameters):
    # --TargetAccuracy=0.1
    # --RandonInitialSeed=0
    # --CalibrationFactor=1
    #
    # --JawPositionFile=patient1/IMRT_fluence_jaw.txt
    # --NumberOfFractions=5

    ##OLD:##--PlanInfoFile=patient1/IMRT_fluence_plan_info.txt

    #
    # --FudgeFactorFile=calibrationfile/FudgeFactor.txt
    # --HornEffectCoefficientFile=calibrationfile/BmletInten400.bin
    # --BBASmoothFunction

    # --BinaryVoxelFilePrefix=
    voxels_bin,  # generated on demand
        # help_text="[VoxPrefix]dens.[fxyz|fyxz] containing floats \n"
        #           + "[VoxPrefix]mat.[ixyz|iyxz] containing ints \n"
        #           + "[VoxPrefix]struc.[ixyz|iyxz] containing ints",

    # --FluenceMapInfoFile=
    fluence_map_info,  # generated on demand
        # help_text="path to the file containing information about the fluence map: dimensions and angles",

    # --FluenceMapFile=
    fluence_map,  # generated on demand
        # help_text="path to the binary or text file containing the fluence map",

    # --PlanInfoFile no longer used?
    # plan_info_file,
        # contains number of fractions and rx dose in cGy (I think only number of fractions is required...

    # --JawPositionFile
    jaw_position_file,
        # help_text="name of file containing dose and plan information",

    # --OutputPrefix=
    output_prefix,
        # help_text="prefix (name) of output files",

    ## GEOMETRY
    # --SAD=
    geo_source2axis,
        # help_text="the distance from the x-ray source to the isocenter",

    # --GPU_ID
    gpu_id = 0,
        # help_text="ID number of GPU used to run simulation on (typically 0-3)",

    exe = "gDPM",

    ## PHYSICS

    # --SourceEnergy=
    source_energy =6.0e6,
        # help_text="energy of source in in eV",

    # # --Photons
    # photons = True,
    #     # help_text="simulate photons",
    #
    # # --Electrons
    # electrons = False,
    #     # help_text="simulate electrons",

    # --ElectronAbsorptionEnergy=
    electron_threshold = 200.0e3,
        # help_text="energy cutoff below which electrons are absorbed",

    # --PhotonAbsorptionEnergy=
    photon_threshold = 50.0e3,
        # help_text="energy cutoff below which electrons are absorbed",

    # --CompatibleFilePrefix=
    phys_config_prefix = GDPM_CONFIG_PATH + "pre4elec",
        # help_text="compatible file name prefix (physics data files)",
    # --DoseToWaterConversionFile=
    phys_dose_to_water = GDPM_CONFIG_PATH + "sfactor.dtw",
        # help_text="dose to water conversion factor file name",

    # --SourceModelFile=
    phys_source_model = GDPM_CONFIG_PATH + "source_10bin.src",  # new file!
        # help_text="dose to water conversion factor file name",

    # --HornEffectCoefficientFile=
    horn_effect_file = GDPM_CONFIG_PATH + "HornEffectCoeff.bin",
        # help_text="Comissioned weights for horn effect",

    # --FudgeFactorFile=
    output_factor_file = GDPM_CONFIG_PATH + "FudgeFactor.txt",
        # help_text="Output factor correction file",

    ## USER VARS

    # --BBASmoothFunction
    smooth_result = False,
        # help_text="Smooths noise in dose result",

    # ('Accuracy', 'Stops when average error is below a threshold'),
    # ('Particle Count', 'Stops after running specified number of primary particle histories'),

    # mode is inferred by which command line arguments are passed
    mc_stopping_mode = 'Accuracy',
        # help_text="Select weather to stop based on desired accuracy or number of particle histories",

    # --TargetAccuracy=
    accuracy_percent_threshold = 0.01,  # 1% error
        # help_text="A percentage (as decimal) of the average error within a region that, when reached, stopps the computation.",

    # --TargetAccuracy=
    accuracy_dose_threshold = 0.10,  # 10% of max dose
        # help_text="A percentage (as decimal) of the maximum dose that, when a region is below, it is not considered for accuracy threshold.",

    # --NumberOfHistories=
    num_hist = 250000000,
        # help_text="number of particle histories to simulate",

    # DATA_FORMAT_CHOICES = (
    #     ('--XYZ', 'x first, then y'),
    #     ('--YXZ', 'y first, then x'),

    # --XYZ or --YXZ
    voxel_format = '--XYZ',
        # help_text="format of data array in binary voxel files: 'XYZ' or 'YXZ' only",

    # --TransposeOutput
    output_transpose = True,
        # help_text="transposes output array for image",

    number_of_fractions =1,
        # help_text="The number of fractions in the plan, multiplies Fx dose",
    ):
        cmd_line = [str(exe), '--GPU_ID=%i' % gpu_id,
                    '--SourceEnergy=%e' % source_energy,
                    '--ElectronAbsorptionEnergy=%e' % electron_threshold,
                    '--PhotonAbsorptionEnergy=%e' % photon_threshold,
                    '--SAD=%f' % geo_source2axis,
                    '--CompatibleFilePrefix=' + phys_config_prefix,
                    '--DoseToWaterConversionFile=' + phys_dose_to_water,
                    '--SourceModelFile=' + phys_source_model,
                    '--FluenceMapInfoFile=' + fluence_map_info,
                    '--FluenceMapFile=' + fluence_map,
                    '--BinaryVoxelFilePrefix=' + voxels_bin,
                    '--OutputPrefix=' + output_prefix,
                    '--JawPositionFile=' + jaw_position_file,
                    '--NumberOfFractions=' + str(number_of_fractions),
                    '--HornEffectCoefficientFile=' + horn_effect_file,
                    '--FudgeFactorFile=' + output_factor_file,
                    str(voxel_format)]

        if output_transpose:
            cmd_line.append('--TransposeOutput')

        # no longer used
        # if photons:
        # 	cmd_line.append('--Photons')
        # if electrons:
        # 	cmd_line.append('--Electrons')

        if smooth_result:
            cmd_line.append('--BBASmoothFunction')

        if mc_stopping_mode == 'Accuracy':
            cmd_line.append('--TargetAccuracy=%f' % accuracy_percent_threshold)  # MUST COME FIRST
            cmd_line.append('--TargetAccuracy=%f' % accuracy_dose_threshold)  # MUST COME SECOND
        else:
            # we are using a fixed number of histories
            cmd_line.append('--NumberOfHistories=%i' % num_hist)

        # "Random Seed" (for denoising)
        # TODO: make truely random
        cmd_line.append('--RandonInitialSeed=0')
        cmd_line.append('--CalibrationFactor=1.0')

        #simply display command line
        # cmd = request.GET.get('cmd',None)
        # if cmd is not None:
        # 	return HttpResponse(" ".join(cmd_line))


        # procERROR = None #process error flag

        #import signal
        #signal disabled for dev mode
        class Alarm(Exception):
            pass

        def alarm_handler(signum, frame):
            raise Alarm

        #signal.signal(signal.SIGALRM, alarm_handler)
        #signal.alarm(120)  # 2 minute MAX!
        try:
            ps = subprocess.Popen(cmd_line, stdout=subprocess.PIPE, env={'PATH':BIN_PATH,'LD_LIBRARY_PATH':'/usr/local/cuda-7.0/lib64'})
        except Alarm:
            ps.terminate()
            ps.kill() #works best?
            # procERROR = True

        #grab stdio
        stdio = ps.communicate()

        return {'cmd':" ".join(cmd_line), 'stdout': stdio[0],'stderr':stdio[1]}

