import h5py
import numpy as np
from orthanc import Portal
from dicomrt import DicomStudy, RTData
from dicomrt.rt_plan import read_dicom_beams, read_mlc_boundaries, read_number_of_fractions
from glob import glob
from time import time
from StringIO import StringIO

#test saving through moba
#test 2

try:
    import pydicom as dicom
except:
    import dicom

class RT5Study:
	'''
	HDF5 layout
	.attrs*
		notes*
		study uuid*
	ct/
		.attrs
			voxel_size
		hu
		voxel_coords ([0,0,0] is image offset)
	structures/
		.attrs
			voxel_size
		[roi label]/
			mask
			shell
			voxel_coords (a link)
			contours/
				[ct slice index]
				[...]
		[...]/
	dose/
		.attrs
			voxel_size
		gy
		voxel_coords (a link)
		dvh/
			[roi label]/
				gy
				cm3
			[...]/
	plan/
		.attrs*
			fraction_count
			modality*
			units (mm and degrees)*
		leaf_locations
		beams/
			[beam number]/
				.attrs
					control_point_count
				collimator
				couch
				description
				energy
				gantry
				isocenter
				jaw_x1
				jaw_x2
				jaw_y1
				jaw_y2
				mlcx_a
				mlcx_b
				mu

	*todo
	'''

	def __init__(self,filename):
		self.h5 = h5py.File(filename)
		self.dcmrt = None

		#build initial structure if it's not there yet
		self.create_groups(['ct','structures','dose'])
	
	def __del__(self):
		'''safely close the h5 file'''
		self.h5.close()
		print "rth5 closed"

	def __getitem__(self, item):
		'''allow dictionary lookup of datasets wrapped in numpy array, or simply h5 groups (passes list of members)'''
		val = self.h5[item]
		if isinstance(val,h5py.Dataset):
			return np.asarray(val)
		else:
			return val

	def __setitem__(self,key,item):
		'''proxy for HDF5 file'''
		self.h5.__setitem__(key,item)

	def __delitem__(self,key):
		'''proxy for HDF5 file'''
		self.h5.__delitem__(key)

	def save(self):
		self.h5.flush()

	def create_groups(self,groups):
		'''creates groups, fails silently if a group already exists'''
		for g in groups:
			try:
				return self.h5.create_group(g)
			except ValueError:
				pass

	def create_dataset(self,name,data,overwrite=True,compression='gzip'):
		''' creates a compressed (gzip) dataset of given data with name in h5file. Overwrite is default. '''
		if overwrite:
			try:
				del self.h5[name]
			except KeyError:
				pass	
		return self.h5.create_dataset(name,data=data,compression=compression)

	def close(self):
		del self

	def load_dicom_orthanc(self,study_id,server,username=None,password=None):
		'''load in a study (CT, Structures, Plan, and Dose) using orthanc study ID'''
		orthanc = Portal(server,username,password)
		# print orthanc.loadPatientList()
		# print orthanc.Patients[0].Studies[0].MainDicomTags.StudyDescription
		self.dcmrt = DicomStudy(orthanc.getDicomStudy(study_id))
		print "[Note] Please modify RTStudy.dcmrt.structure_list, if needed, then run RTStudy.process_dicom()"

	def load_dicom_folder(self,folder_path):
		'''load in a study (CT, Structures, Plan, and Dose) from folder of raw dicom files'''
		file_paths = glob('{}/*'.format(folder_path))
		self.dcmrt = DicomStudy(file_paths)
		print "[Note] Please modify RTStudy.dcmrt.structure_list, if needed, then run RTStudy.process_dicom()"

	def import_dicom(self,ct_structures_dose=True,dvh=True,plan =True):
		if ct_structures_dose:
			import_ct_structures_dose(self)
		if plan:
			import_plan(self)
		if dvh:
			import_dvh(self)

	def tree(self, max_depth=2, max_children = 10):

		def h5_tree(x, padding='', depth=1):
				for i,j in x.items():
					if isinstance(j,h5py.Group):
						#print '{}{}/'.format(' '*len(padding),i)
						if depth <= max_depth:
							if len(j) <= max_children:
								h5_tree( j, '{}{}/'.format(padding,i),depth+1)
							else:
								print('{}{}/({} items)'.format(padding,i,len(j)))
						else:
							print('{}{}/({} items)'.format(padding,i,len(j)))
					elif isinstance(j,h5py.Dataset):
						print '{}{}'.format(padding,i)
		h5_tree(self.h5)


def import_ct_structures_dose(rt5):
	''' create roi masks, extract and resample dose distribution to CT geometry, extract and stack CT slices '''
	
	if rt5.dcmrt:
		print "Parsing dicom ct, structures (w/ masks), and dose..."

		#fail if multiple dose files
		if isinstance(rt5.dcmrt.files.dose,list):
			raise Exception("Only one dose file is supported at this time (a list is not allowed)")

		rt5.rt_data = RTData().import_data(
					image_files=rt5.dcmrt.files.ct,
					structure_file = rt5.dcmrt.files.structures,
					roi_ref_nums = rt5.dcmrt.get_structure_index_list(),
					roi_key_names = rt5.dcmrt.get_structure_label_list(),
					dose_file=rt5.dcmrt.files.dose,
					keep_only_roi_images = False #otherwise CT is truncated
				)
		tic = time()
		rt5.rt_data.create_roi_masks() #default is to create masks for all
		toc = time()-tic
		print "\t{} seconds to create masks".format(toc) #168

		### save rt_data and plan information into h5 format ###

		## ct  (slices) ##
		rt5.create_dataset('ct/hu',rt5.rt_data.slices.astype('int16')-1000) #converting to CT number to HU
		rt5.h5['ct'].attrs['voxel_size'] = rt5.rt_data.dv
		rt5.create_dataset('ct/voxel_coords', rt5.rt_data.voxel_coords)


		#record voxel_size (dv)

		## tps_gray (dose_grid) ##
		if rt5.rt_data.dose_grid is not None:
			rt5.create_dataset('dose/gy',rt5.rt_data.dose_grid)
			#record voxel_size (dv)
			rt5.h5['dose'].attrs['voxel_size'] = rt5.h5['ct'].attrs['voxel_size'] # a link
			# rt5.h5['dose/voxel_coords'] = rt5.h5['ct/voxel_coords'] # a link

		## structures: mask, shell, and points (volume_mask, surface_mask, contour_arrays) ##
		if rt5.rt_data.contour_arrays is not None:
			rt5.create_groups(['structures/{}'.format(label) for label in rt5.rt_data.roi_names])
			rt5.create_groups(['structures/{}/contours'.format(label) for label in rt5.rt_data.roi_names])
			#record voxel_size (dv)
			# rt5.h5['structures'].attrs['voxel_size'] = rt5.h5['ct'].attrs['voxel_size'] # a link
			for struct_name in rt5.rt_data.roi_names:
				rt5.create_dataset('structures/{}/mask'.format(struct_name), rt5.rt_data.volume_mask[struct_name])
				rt5.create_dataset('structures/{}/shell'.format(struct_name), rt5.rt_data.surface_mask[struct_name])
				# rt5.h5['structures/{}/voxel_coords'.format(struct_name)] = rt5.h5['ct/voxel_coords'] # a link

				for slice_idx, contour_array in zip(rt5.rt_data.contour_image_inds[struct_name],rt5.rt_data.contour_arrays[struct_name]):
					#contour arrays are of non-uniform length, therefore we must save each contour array individually
					#also too small to bother compressing
					rt5.create_dataset('structures/{}/contours/{}'.format(struct_name,slice_idx),contour_array, compression=None)
			#some meta data
			
	
	else:
		raise Exception("dicom files have not been loaded into this instance or have been set to None.")


def import_plan(rt5):
	''' reads in plan data (beams with control points) '''

	if rt5.dcmrt:
		print "Parsing dicom plan, (beams w/ control points)..."

		beam_dict = read_dicom_beams(rt5.dcmrt.files.plan)
		for beam in beam_dict.itervalues():
			# rt5.create_groups(['plan/beams/{}'.format(beam.Number)])
			rt5.create_dataset('plan/beams/{}/{}'.format(beam.Number,'description'),beam.Name,compression=None)
			rt5.create_dataset('plan/beams/{}/{}'.format(beam.Number,'energy'),beam.BeamEnergy,compression=None)
			rt5.create_dataset('plan/beams/{}/{}'.format(beam.Number,'mu'),beam.MU,compression=None)
			rt5.create_dataset('plan/beams/{}/{}'.format(beam.Number,'mlcx_a'),beam.BankA,compression=None)
			rt5.create_dataset('plan/beams/{}/{}'.format(beam.Number,'mlcx_b'),beam.BankB,compression=None)
			rt5.create_dataset('plan/beams/{}/{}'.format(beam.Number,'jaw_x1'),beam.X1,compression=None)
			rt5.create_dataset('plan/beams/{}/{}'.format(beam.Number,'jaw_x2'),beam.X2,compression=None)
			rt5.create_dataset('plan/beams/{}/{}'.format(beam.Number,'jaw_y1'),beam.Y1,compression=None)
			rt5.create_dataset('plan/beams/{}/{}'.format(beam.Number,'jaw_y2'),beam.Y2,compression=None)
			rt5.create_dataset('plan/beams/{}/{}'.format(beam.Number,'isocenter'),np.array(zip(beam.isoX,beam.isoY,beam.isoZ)),compression=None)
			rt5.create_dataset('plan/beams/{}/{}'.format(beam.Number,'gantry'),beam.GantryAng,compression=None)
			rt5.create_dataset('plan/beams/{}/{}'.format(beam.Number,'collimator'),beam.CollimatorAng,compression=None)
			rt5.create_dataset('plan/beams/{}/{}'.format(beam.Number,'couch'),beam.CouchAng,compression=None)
			rt5['plan/beams/{}'.format(beam.Number)].attrs['control_point_count'] = beam.ControlPointCount

		#other needed data
		print "Loading MLC leaf boundaries..."
		#find first treatment beam
		rt5.create_dataset('plan/leaf_locations',np.array(read_mlc_boundaries(rt5.dcmrt.files.plan)))

		fx_count = read_number_of_fractions(rt5.dcmrt.files.plan)
		rt5['plan'].attrs['fraction_count'] = fx_count
		print "Fraction count saved: {}".format(fx_count)

	else:
		raise Exception("dicom files have not been loaded into this instance or have been set to None.")

def import_dvh(rt5):
    if rt5.dcmrt:
        print "Parsing dicom dvh, (from TPS)..."
        dose_file = dicom.read_file(rt5.dcmrt.files.dose)

        #we always want to rewind in case of StringIO
        if isinstance(rt5.dcmrt.files.dose,StringIO):
            rt5.dcmrt.files.dose.seek(0)
        
        #sometimes DVH arn't there!
        if hasattr(dose_file,'DVHSequence'):
            for dvh in dose_file.DVHSequence:
                #get label for a given index (if the structure is listed in our dcmrt study)
                structure = rt5.dcmrt.find_structure(dvh.DVHReferencedROISequence[0].ReferencedROINumber) #assuming always 1 element in sequence
                if structure:
                    print "Adding DVH (Label: {}, Idx: {})".format(structure[0], structure[1])
                    rt5.create_dataset('dose/dvh/{}/{}'.format(structure[0],dvh.DoseUnits.lower()),np.cumsum(dvh.DVHData[0::2])) #odd indices are volume data
                    vol_data = np.array(dvh.DVHData[1::2])
                    rt5.create_dataset('dose/dvh/{}/{}'.format(structure[0],dvh.DVHVolumeUnits.lower()),vol_data) #odd indices are volume data
                    rt5.create_dataset('dose/dvh/{}/{}'.format(structure[0],'percent'),vol_data/vol_data[0]) #odd indices are volume data
                else:
                    print "Skipping index {}...".format(dvh.DVHReferencedROISequence[0].ReferencedROINumber)
        else:
        	print "[ALERT] No DVH found in dose file!"
    else:
        raise Exception("dcmrt files have not been loaded into this instance or have been set to None.")