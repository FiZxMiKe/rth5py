from dicomrt.rt_data import resample_array
from rt5study import RT5Study
import numpy as np
## in general the modules should implement the rt5Study object


def make_lowres(rt5,inplane_factor=2):
    """Inplane factor is the factor by which you want to grow the pixes (or reduce the number of elements in a slice)"""
    if isinstance(rt5,RT5Study):

        try:
            rt5.create_dataset('dose_lowres/gy',resample_array([inplane_factor,inplane_factor,1],rt5['dose/gy']))
            print "Lowres dose data created @ 'dose_lowres/gy'"
        except KeyError:
            print "[ALERT] No CT data found!"

        try:
            rt5.create_dataset('ct_lowres/hu',resample_array([inplane_factor,inplane_factor,1],rt5['ct/hu']))
            print "Lowres dose CT created @ 'ct_lowres/hu'"
        except KeyError:
            print "[ALERT] No Dose data found!"

        for roi_label in rt5['structures/']:
            try:
                rt5.create_dataset('structures_lowres/{}/mask'.format(roi_label),
                    resample_array([inplane_factor,inplane_factor,1],rt5['structures/{}/mask'.format(roi_label)]).astype('float32')) #float64 is returned by default
                print "Lowres mask for structure created @ 'structures/{}/mask'".format(roi_label)
            except KeyError:
                print "[ALERT] Mask data for structure '{}' not found!".format(roi_label)

    else:
        raise Exception('Plese provide an RT5Study instance')


def set_ct_calibration(rt5, hu=[-811.6,-508.1,-63.9,-27.6,4.2,52.65,53.9,250.8,936.35],dens=[0.2,0.5,0.96,0.99,1,1.07,1.06,1.16,1.61],ct_group='ct/'):
    ''' density in g/cm3 default is from UCSD'''
    if isinstance(rt5,RT5Study):
        rt5.create_dataset(ct_group+'/calibration/hu',hu, compression=None)
        rt5.create_dataset(ct_group+'/calibration/density',dens, compression=None)
        print "CT calibration curve (hu,density) saved in '{}calibration'".format(ct_group)
    else:
        raise Exception('Plese provide an RT5Study instance')


def make_materials(rt5,ct_group='ct/', tissue_max_hu=200, air_max_hu=-800,air_number=4,tissue_number=3,bone_number=2):
    '''
        for compatability with gDPM 2.53 binary read in which material number re-assignment is hard coded
        and relative material numbers are soft-coded in pre4elec.matter with the following ordering:
        0 = water, 1 = lung, 2 = cortical bone, 3 = tissue, 4 = dry air
    '''
    if isinstance(rt5,RT5Study):
        ct_hu = rt5[ct_group+'/hu']
        mat_nums = ct_hu.copy()

        air_idxs = np.where(ct_hu<=air_max_hu)
        tissue_idxs = np.where((air_max_hu<ct_hu) & (ct_hu<=tissue_max_hu))
        bone_idxs = np.where(tissue_max_hu<ct_hu)

        mat_nums[air_idxs] = air_number
        mat_nums[tissue_idxs] = tissue_number
        mat_nums[bone_idxs] = bone_number

        rt5.create_dataset(ct_group+'/materials',mat_nums.astype('int32'))
        print "Materials array saved to {}/materials".format(ct_group)
    else:
        raise Exception('Plese provide an RT5Study instance')
    

def make_ct_density(rt5,ct_group='ct/'):

    if isinstance(rt5,RT5Study):
        if rt5[ct_group+'/hu'] is not None:
            ct = rt5[ct_group+'/hu']

            # create piecewise quadratic fit
            try:
                x = rt5[ct_group+'/calibration/hu']
                y = rt5[ct_group+'/calibration/density']
            except KeyError:
                raise Exception("CT calibration data not found in {} 'calibration/'".format(ct_group))

            # we split the curve at zero
            gte0 = np.where(x>=0.)
            lt0 = np.where(x<0.)

            # fit quadratic functions
            dens_gte0_fxn = calc_ployfit(x[gte0],y[gte0],order=2)  # quadratic fit
            dens_lt0_fxn = calc_ployfit(x[lt0],y[lt0],order=2)  # quadratic fit

            # we split ct into positive and negative indices
            ct_gte0 = np.where(ct >= 0)
            ct_lt0 = np.where(ct < 0)

            # a place to store the data
            dens = ct.copy().astype('float32')

            # converting
            dens[ct_gte0] = dens_gte0_fxn(dens[ct_gte0])
            dens[ct_lt0] = dens_lt0_fxn(dens[ct_lt0])

            # saving
            rt5.create_dataset(ct_group+'density', dens)
            print "CT density image created in '{}density'".format(ct_group)

        else:
            print "[ALERT] '{}hu' data not found".format(ct_group)

    else:
        raise Exception('Plese provide an RT5Study instance')


def calc_ployfit(x,y,order=1):
    # calculate polynomial interpolating object
    z = np.polyfit(x, y, order)
    return np.poly1d(z)


def make_features(rt5):
    """ must have rt_data for now """
    from features.feature_data import GeometryFeatures
    for roi_label in study['structures/']:
        print "Processing ROI: {}".format(roi_label)
        geo_feats = GeometryFeatures(rt5.rt_data, roi=roi_label) 
        geo_feats.calc_volume()
        geo_feats.calc_roundness()
        geo_feats.calc_size_native()
        geo_feats.calc_size_transform()
        geo_feats.calc_center_of_mass()
        if roi_label is not 'PTV':
            geo_feats.calc_overlap(roi2='PTV')
            geo_feats.calc_out_of_field_frac(roi2='PTV')
            geo_feats.calc_DTH(target_roi='PTV', num_dist_bins=1800, dist_range=[-60, 750], num_vol_bins=200, normalize=True)
            
        for key, value in geo_feats.feats.iteritems():
            if value is not None:
                rt5.h5['/features/{}/geometry/{}'.format(roi_label,key)] = value