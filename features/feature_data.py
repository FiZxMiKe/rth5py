"""
This module contains functions and classes for extracting features from DICOM RT data. Most
classes require DICOM RT data to have been previously imported into a DicomRTImage object.
"""

__author__ = 'dstaub'

import os
import json
import re

import numpy as np

import scipy
import scipy.stats as stats

try:
    import dicom
except:
    import pydicom as dicom

import mahotas.features.texture as mhtstexture

from dicomrt.rt_data import calc_min_displacement



class Features:
    """ Holds and reads/writes feature data.

    Contains methods that read and write a dictionary of features to and from a text file. The feature dictionary is an
    attribute of the class.

        Parameters:

        Attributes:
            - feats: A dictionary where keys are feature names and values are lists of feature values.
    """
    def __init__(self):
        self.feats = {}

    def read(self, file_path):
        """ Reads features from text file into Feature object.

            Parameters:
                - file_path: A string denoting the location of the file containing the feature data to be read in.

            Returns:
                - self: The Feature object.
        """
        # Read in dictionary
        with open(file_path, "r") as fp:
            self.feats = json.load(fp)

        # Turn lists into numpy arrays
        for key, value in self.feats.iteritems():
            if value is not None:
                if len(value) == 0:
                    self.feats[key] = None
                else:
                    self.feats[key] = np.array(value)

        return self

    def write(self, file_path):
        """ Saves Feature object to text file.

        If the directory to which it should be saved doesn't exist it will be created.

            Parameters:
                - file_path: A string denoting the location of the file to which data should be written.

            Returns:
                - self: The Feature object.

            Raises:
        """
        # Create temporary feature object with lists instead of numpy arrays
        temp = Features()
        for key, value in self.feats.iteritems():
            if value is None:
                temp.feats[key] = None
            else:
                temp.feats[key] = value.tolist()

        # Write feature dictionary to text file
        f = json.dumps(temp.feats)
        f = f.replace('],', '],\n')  # Add line breaks for easier reading
        f = f.replace('null,', 'null,\n')  # Add line breaks for easier reading
        file_dir = os.path.dirname(file_path)
        if not os.path.exists(file_dir):
            os.makedirs(file_dir)
        with open(file_path, "w") as fp:
            fp.write(f)

    def blobify_feats(self, feat_regex='.'):
        """ Turn specified features into binary strings (BLOBs).

        Convert numpy arrays to binary strings for all features matching the input regex.

            Parameters:
                - feat_regex: Pattern against which feature names are matched. Matching
                    features are converted.

            Returns:
                - self: The Feature object.

            Raises:
        """
        for key, value in self.feats.iteritems():
            if re.search(feat_regex, key) is not None:
                self.feats[key] = blobify(self.feats[key])

        return self

    def deblobify_feats(self, feat_regex='.'):
        """ Turn specified features into numpy arrays from binary strings (BLOBs).

        Convert binary strings back to numpy arrays for all features matching the input regex.

            Parameters:
                - feat_regex: Pattern against which feature names are matched. Matching
                    features are converted.

            Returns:
                - self: The Feature object.

            Raises:
        """
        for key, value in self.feats.iteritems():
            if re.search(feat_regex, key) is not None:
                self.feats[key] = deblobify(self.feats[key])

        return self


class PlanFeatures(Features):
    """ Holds, reads/writes, and calculates plan feature data.

    Contains methods that read, write, and calculate plan features which are stored in a dictionary. The feature
    dictionary is an attribute of the class.

        Parameters:

        Attributes:
            - feats: A dictionary where keys are plan feature names and values are lists of plan feature values.
    """

    def __init__(self):
        self.feats = {'prescription_dose': None, 'beam_angles': None, 'beam_MUs': None, 'beam_angle_hist_vals': None,
                      'beam_angle_hist_bins': None}

    def calculate(self, plan_file, num_bins=120):
        """ Imports raw plan data from DICOM file and calculates features from imported data.

        Imports prescription dose and beam angle/MU data. Creates a histogram based on beam angle with MUs as
        histogram weights.

            Parameters:
                - plan_file: String containing the file path where the DICOM plan file is located.
                - num_bins: Number of bins used in the createion of the beam angle histogram. The histogram range is
                    fixed from 0-360.

            Returns:
                - self: The PlanFeatures object.

            Raises:
                - An error if the supplied file path is not valid.
        """
        # Check that file path points to valid file.
        if os.path.isfile(plan_file) is False:
            raise Exception('File \'{0}\' does not exist.'.format(plan_file))

        # Read DICOM plan file
        p = dicom.read_file(plan_file)

        # Get beam MUs and reference numbers, sort results according to beam reference number
        mu_vals, mu_beam_num = [], []
        for beam in p.FractionGroupSequence[0].ReferencedBeamSequence:
            try:
                mu_vals.append(float(beam.BeamMeterset))
                mu_beam_num.append(int(float(beam.ReferencedBeamNumber)))
            except AttributeError:
                print 'WARNING: No BeamMeterset attribute found for beam {0}, skipping.'.format(float(beam.ReferencedBeamNumber))
        mu_vals = [val for (num, val) in sorted(zip(mu_beam_num, mu_vals))]

        # Get beam gantry angles and reference numbers, sort results according to beam reference number
        angle_vals, angle_beam_num = [], []
        for beam in p.BeamSequence:
            if int(float(beam.BeamNumber)) in mu_beam_num:
                angle_vals.append(float(beam.ControlPointSequence[0].GantryAngle))
                angle_beam_num.append(int(float(beam.BeamNumber)))
        angle_vals = [val for (num, val) in sorted(zip(angle_beam_num, angle_vals))]

        # Create beam angle histogram if necessary data is available
        hist_vals, bin_centers = np.array([]), np.array([])
        if angle_vals:
            hist_vals, edges = np.histogram(angle_vals, bins=num_bins, range=(0, 360), weights=mu_vals)
            hist_vals /= float(sum(hist_vals))
            bin_centers = (edges[:-1] + edges[1:]) / 2

        # Assign attribute values
        self.feats['prescription_dose'] = np.array([float(p.DoseReferenceSequence[0].DeliveryMaximumDose)])
        self.feats['beam_angles'] = np.array(angle_vals)
        self.feats['beam_MUs'] = np.array(mu_vals)
        self.feats['beam_angle_hist_vals'] = hist_vals
        self.feats['beam_angle_hist_bins'] = bin_centers

        return self


class DoseFeatures(Features):
    """ Holds, reads/writes, and calculates dose feature data.

    Contains methods that read, write, and calculate dose features which are stored in a dictionary. The feature
    dictionary is an attribute of the class.

        Parameters:

        Attributes:
            - feats: A dictionary where keys are dose feature names and values are lists of dose feature
                values.
    """

    def __init__(self, image_data=None, roi=None):
        # Check input
        if image_data is None:
            pass
        elif roi is None:
            roi = image_data.roi_names[0]
        elif roi not in image_data.roi_names:
            raise KeyError("Incorrect ROI name supplied, valid options are {0}".format(image_data.roi_names))

        # Assign attribute values
        self.image_data = image_data
        self.roi = roi
        self.feats = {'DVH_vol_vals': None, 'DVH_dose_bins': None, 'DVH_dose_vals': None, 'DVH_vol_bins': None,
                      'eDVH_vol_vals': None, 'eDVH_dose_bins': None, 'eDVH_dose_vals': None, 'eDVH_vol_bins': None}

    def import_DVH(self, dose_file, roi_ref_num=None, num_dose_bins=None, dose_range=None, num_vol_bins=None, vol_range=None, normalize=False):
        """ Imports raw dose data from DICOM file and calculates features from imported data.

        Imports DVH data and resamples histogram if desired.

            Parameters:
                - dose_file: String containing the file path where the DICOM dose file is located.
                - roi_ref_num: The ROIReferenceNumber of the ROI for which dose features should be calculated. If
                    none is supplied the first ROI in the sequence is chosen by default.
                - num_dose_bins: Number of bins used to resample the raw DVH. If None then resampling is not performed.
                - dose_range: A list of two numbers [low high] denoting the lower and upper bounds used to perform
                    histogram resampling. Values outside of these bounds are assigned the closest in-bounds bin. If no
                    values are supplied the minimum and maximum dose values will be used by default.
                - num_vol_bins: An integer specifying the number of volume bins to use for resampling cumulative DVHs along
                    the volume axis. If no value is given then volume resampled DVHs are not computed.
                - vol_range: A list of two numbers [low high] denoting the lower and upper bounds used to perform
                    histogram resampling along the volume axis. Values outside of these bounds are assigned the closest
                    in-bounds bin. If no parameter is supplied the minimum and maximum volume values will be used by default.
                - normalize: If True volume axis is normalized to 1.

            Returns:
                - self: The DoseFeatures object.

        """
        # Read DICOM dose file
        d = dicom.read_file(dose_file)

        # Import DVH values and bins
        if roi_ref_num is None:
            dvh_data = d.DVHSequence[0].DVHData
            scale_factor = d.DVHSequence[0].DVHDoseScaling
        else:
            for dvh_seq in d.DVHSequence:
                if dvh_seq.DVHReferencedROISequence[0].ReferencedROINumber == roi_ref_num:
                    dvh_data = dvh_seq.DVHData
                    scale_factor = dvh_seq.DVHDoseScaling

        # Get separated bin and value data, save bin centers in bin array
        vol_vals_raw = np.array(dvh_data[1::2])
        vol_vals_raw *= 1000  # Convert from cm^3 to mm^3
        if normalize:
            vol_vals_raw /= max(dvh_data[1::2])
        dose_bin_widths = np.array(dvh_data[0::2]) * scale_factor
        dose_bins_raw = np.cumsum(dose_bin_widths) - dose_bin_widths/2

        # Resample histogram along dose axis if desired
        dose_bins = dose_bins_raw
        vol_vals = vol_vals_raw
        if num_dose_bins is not None:
            dose_bins, vol_vals = histogram_resamp(dose_bins, vol_vals, num_dose_bins, range_resamp=dose_range)

        # Resample histogram along volume axis if desired
        vol_bins = np.array([])
        dose_vals = np.array([])
        if num_vol_bins is not None:
            vol_bins, dose_vals = histogram_resamp(dose_bins_raw, vol_vals_raw, num_vol_bins, range_resamp=vol_range, flip_axes=True)

        # Assign attribute values
        self.feats['DVH_dose_bins'] = dose_bins
        self.feats['DVH_vol_vals'] = vol_vals
        self.feats['DVH_vol_bins'] = vol_bins
        self.feats['DVH_dose_vals'] = dose_vals

        return self

    def calc_DVH(self, num_dose_bins, dose_range=None, num_vol_bins=None, vol_range=None, normalize=False):
        """ Calculates DVH for primary ROI.

        Calculates DVH data for primary ROI and resamples histogram along volume axis if desired.

            Parameters:
                - num_dose_bins: Number of bins used to create the DVH.
                - dose_range: A list of two numbers [low high] denoting the lower and upper bounds used to perform
                    histogram binning. Values outside of these bounds are assigned the closest in-bounds bin. If no
                    values are supplied the minimum and maximum dose values will be used by default.
                - num_vol_bins: An integer specifying the number of volume bins to use for resampling cumulative DVHs along
                    the volume axis. Bins will be distributed evenly between within the specified range. If no value is
                    given then resampled DVHs are not computed.
                - vol_range: A list of two numbers [low high] denoting the lower and upper bounds used to perform
                    histogram resampling along the volume axis. Values outside of these bounds are assigned the closest
                    in-bounds bin. If no values are supplied the minimum and maximum volume values will be used by default.
                - normalize: If True volume axis is normalized to 1.

            Returns:
                - self: The DoseFeatures object.
        """
        # Compute DVH for primary ROI
        dose_vals = self.image_data.dose_grid[self.image_data.volume_mask[self.roi]]
        d_vol_vals, edges = np.histogram(dose_vals, bins=num_dose_bins, range=dose_range)
        d_vol_vals = d_vol_vals.astype('float') * np.prod(self.image_data.dv)  # Convert to volume units (mm^3)
        if normalize:
            d_vol_vals /= sum(d_vol_vals)
        c_vol_vals = np.cumsum(d_vol_vals[::-1])[::-1]  # Get cumulative histogram from differential histogram
        dose_bins = (edges[:-1] + edges[1:]) / 2

        # Resample histogram along volume axis if desired
        vol_bins = np.array([])
        c_dose_vals = np.array([])
        if num_vol_bins is not None:
            vol_bins, c_dose_vals = histogram_resamp(dose_bins, c_vol_vals, num_vol_bins, range_resamp=vol_range, flip_axes=True)

        # Assign attribute values
        self.feats['DVH_dose_bins'] = dose_bins
        self.feats['DVH_vol_vals'] = c_vol_vals
        self.feats['DVH_vol_bins'] = vol_bins
        self.feats['DVH_dose_vals'] = c_dose_vals

        return self

    def calc_eDVH(self, num_dose_bins, norm_roi, dose_grid_roi_norm=None, weights=None, dose_range=None, num_vol_bins=None, vol_range=None,
                  normalize=False):
        """ Calculates estimated DVH for primary ROI from a ROI normalized dose grid.

        Calculates estimated DVH data for primary ROI and resamples histogram along volume axis if desired.

            Parameters:
                - num_dose_bins: Number of bins used to create the DVH.
                - norm_roi: Name of ROI to which `dose_grid_roi_norm` is normalized to.
                - dose_grid_roi_norm: ROI normalized dose grid. If None then the grid is calculated.
                - weights: Weight array or flag for calculating the ROI normalized dose grid. Only used if
                    `dose_grid_roi_norm` is None.
                - dose_range: A list of two numbers [low high] denoting the lower and upper bounds used to perform
                    histogram binning. Values outside of these bounds are assigned the closest in-bounds bin. If no
                    values are supplied the minimum and maximum dose values will be used by default.
                - num_vol_bins: An integer specifying the number of volume bins to use for resampling cumulative DVHs along
                    the volume axis. Bins will be distributed evenly between within the specified range. If no value is
                    given then resampled DVHs are not computed.
                - vol_range: A list of two numbers [low high] denoting the lower and upper bounds used to perform
                    histogram resampling along the volume axis. Values outside of these bounds are assigned the closest
                    in-bounds bin. If no values are supplied the minimum and maximum volume values will be used by default.
                - normalize: If True, volume axis is normalized to 1.

            Returns:
                - self: The DoseFeatures object.
        """
        # Check and set values
        image_data = self.image_data
        if dose_grid_roi_norm is None:
            dose_grid_roi_norm = image_data.calc_roi_norm_grid(norm_roi, 'dose_grid', weights=weights)

        # Get minimum displacements of ROI voxels to norm ROI volume
        disp_roi = calc_min_displacement(image_data, self.roi, norm_roi, target_type='volume', disp_type='ijk')

        # Shift origin to center of dose grid to get dose grid subscripts
        subs = disp_roi + np.array(dose_grid_roi_norm.shape) / 2

        # Remove out of bounds subscripts
        grid_size = dose_grid_roi_norm.shape
        mask = ((subs[:, 0] >= 0) * (subs[:, 0] < grid_size[0]) *
                (subs[:, 1] >= 0) * (subs[:, 1] < grid_size[1]) *
                (subs[:, 2] >= 0) * (subs[:, 2] < grid_size[2]))
        subs = subs[mask, :]

        # Sample dose values from normalized dose grid
        dose_vals = dose_grid_roi_norm[subs[:, 0], subs[:, 1], subs[:, 2]]

        # Histogram dose values
        d_vol_vals, edges = np.histogram(dose_vals, bins=num_dose_bins, range=dose_range)
        d_vol_vals = d_vol_vals.astype('float') * np.prod(self.image_data.dv)  # Convert to volume units (mm^3)
        if normalize:
            d_vol_vals /= sum(d_vol_vals)
        c_vol_vals = np.cumsum(d_vol_vals[::-1])[::-1]  # Get cumulative histogram from differential histogram
        dose_bins = (edges[:-1] + edges[1:]) / 2

        # Resample histogram along volume axis if desired
        vol_bins = np.array([])
        c_dose_vals = np.array([])
        if num_vol_bins is not None:
            vol_bins, c_dose_vals = histogram_resamp(dose_bins, c_vol_vals, num_vol_bins, range_resamp=vol_range, flip_axes=True)

        # Assign attribute values
        self.feats['eDVH_dose_bins'] = dose_bins
        self.feats['eDVH_vol_vals'] = c_vol_vals
        self.feats['eDVH_vol_bins'] = vol_bins
        self.feats['eDVH_dose_vals'] = c_dose_vals

        return self


class GeometryFeatures(Features):
    """ Holds, reads/writes, and calculates geometry feature data.

    Contains methods that read, write, and calculate geometry features which are stored in a dictionary. The feature
    dictionary is an attribute of the class.

        Parameters:

        Attributes:
            - image_data: A DicomRTImage object containing image slices, volume and surface contour masks,
                    and volume and surface mask point arrays for both source and target ROIs.
            - roi: A string denoting the primary ROI to be used for feature calculation. Used as a key
                into the `image_data` dictionaries containing ROI information.
            - feats: A dictionary where keys are geometry feature names and values are lists of geometry feature values.
    """
    def __init__(self, image_data=None, roi=None):
        # Check input
        if image_data is None:
            pass
        elif roi is None:
            roi = image_data.roi_names[0]
        elif roi not in image_data.roi_names:
            raise KeyError("Incorrect ROI name supplied, valid options are {0}".format(image_data.roi_names))

        # Assign attribute values
        self.image_data = image_data
        self.roi = roi
        self.feats = {'volume': None, 'size_native': None, 'size_transform': None, 'roundness': None, 'cum_DTH_vol_vals': None,
                      'dif_DTH_vol_vals': None, 'DTH_dist_bins': None, 'cum_DTH_dist_vals': None,
                      'DTH_vol_bins': None, 'overlap_volume_frac': None, 'out_of_field_volume_frac': None,
                      'cent_of_mass': None, 'cum_RVH_vol_vals': None, 'dif_RVH_vol_vals': None,
                      'RVH_risk_bins': None, 'cum_xyRVH_vol_vals': None, 'dif_xyRVH_vol_vals': None, 'cum_zRVH_vol_vals': None,
                      'dif_zRVH_vol_vals': None, 'compRVH_risk_bins': None}

    def calc_volume(self):
        """ Calculates ROI volume in mm^3 units.

            Parameters:

            Returns:
                - self: The GeometryFeatures object.
        """
        volume = np.sum(self.image_data.volume_mask[self.roi]) * self.image_data.dv.prod()
        self.feats['volume'] = np.array([volume])
        return self

    def calc_roundness(self):
        """ Calculates ROI roundness.

            Parameters:

            Returns:
                - self: The GeometryFeatures object.
        """
        # Check, import, and set values
        volume_mask = self.image_data.volume_mask[self.roi]
        dv = self.image_data.dv
        nv = volume_mask.shape
        volume = np.sum(volume_mask) * dv.prod()

        # Calculate surface area of all exposed voxel faces in volume mask
        zeros_i = np.zeros([1, nv[1], nv[2]])
        zeros_j = np.zeros([nv[0], 1, nv[2]])
        zeros_k = np.zeros([nv[0], nv[1], 1])

        i_faces = (1 + volume_mask - np.concatenate([zeros_i, volume_mask[0:nv[0] - 1, :, :]], axis=0) -
                   np.concatenate([volume_mask[1:nv[0], :, :], zeros_i], axis=0))
        j_faces = (1 + volume_mask - np.concatenate([zeros_j, volume_mask[:, 0:nv[1] - 1, :]], axis=1) -
                   np.concatenate([volume_mask[:, 1:nv[1], :], zeros_j], axis=1))
        k_faces = (1 + volume_mask - np.concatenate([zeros_k, volume_mask[:, :, 0:nv[2] - 1]], axis=2) -
                   np.concatenate([volume_mask[:, :, 1:nv[2]], zeros_k], axis=2))
        surface_area_voxel = sum(i_faces[volume_mask]*dv[1]*dv[2] + j_faces[volume_mask]*dv[0]*dv[2] + k_faces[volume_mask]*dv[0]*dv[1])

        # Calculate structure roundness
        r = (volume * (3.0 / 4.0) * (1.0 / np.pi)) ** (1.0 / 3.0)
        surface_area_sphere = 4 * np.pi * r ** 2
        roundness = surface_area_sphere / surface_area_voxel
        self.feats['roundness'] = np.array([roundness])
        return self

    def calc_size_native(self):
        """ Calculates approximate ROI size along the three CT coordinate system axes.

            Returns:
                - self: The GeometryFeatures object.
        """
        surface_mask = self.image_data.surface_mask[self.roi]
        surface_points = self.image_data.voxel_coords[surface_mask]
        size_native = surface_points.std(axis=0) * 4
        self.feats['size_native'] = size_native
        return self

    def calc_size_transform(self):
        """ Calculates approximate ROI size along its three principal axes.

            Returns:
                - self: The GeometryFeatures object.
        """
        surface_mask = self.image_data.surface_mask[self.roi]
        surface_points = self.image_data.voxel_coords[surface_mask]
        surface_points -= surface_points.mean(axis=0)
        u_trans, _, _ = scipy.linalg.svd(surface_points.T, full_matrices=False)
        contour_points_transform = np.dot(surface_points, u_trans)
        size_transform = contour_points_transform.std(axis=0) * 4
        self.feats['size_transform'] = size_transform
        return self

    def calc_center_of_mass(self):
        """ Calculates ROI center of mass.

            Returns:
                - self: The GeometryFeatures object.
        """
        volume_mask = self.image_data.volume_mask[self.roi]
        volume_points = self.image_data.voxel_coords[volume_mask]
        volume_vals = self.image_data.slices[volume_mask]
        cent_of_mass = np.dot(volume_vals, volume_points) / sum(volume_vals)
        self.feats['cent_of_mass'] = cent_of_mass
        return self

    def calc_overlap(self, roi2):
        """ Calculates the fraction of primary ROI volume overlapping the `roi2` volume.

            Parameters:
                - roi2: A string denoting the ROI to compare to the primary ROI. Used as a key into the `image_data`
                    dictionaries containing ROI information..

            Returns:
                - self: The GeometryFeatures object.
        """
        # Check, import, and set values
        image_data = self.image_data
        roi = self.roi

        # Calculate ROI/target overlap volume fraction
        overlap_vol = np.sum(image_data.volume_mask[roi] * image_data.volume_mask[roi2])
        overlap_vol_frac = float(overlap_vol) / np.sum(image_data.volume_mask[roi])
        self.feats['overlap_volume_frac'] = np.array([overlap_vol_frac])
        return self

    def calc_out_of_field_frac(self, roi2):
        """ Calculates the fraction of primary ROI volume extending beyond the z bounds of `roi2`.

            Parameters:
                - roi2: A string denoting the ROI to compare to the primary ROI. Used as a key into the `image_data`
                    dictionaries containing ROI information..

            Returns:
                - self: The GeometryFeatures object.
        """
        # Check, import, and set values
        volume_mask = self.image_data.volume_mask[self.roi]
        dv = self.image_data.dv
        volume = np.sum(volume_mask) * dv.prod()
        volume_mask = self.image_data.volume_mask[self.roi]
        volume_mask2 = self.image_data.volume_mask[roi2]
        volume_points = self.image_data.voxel_coords[volume_mask]
        volume_points2 = self.image_data.voxel_coords[volume_mask2]

        # Calculate feature
        max_z = max(volume_points2[:, 2])
        min_z = min(volume_points2[:, 2])
        out_of_field_vol_frac = sum((volume_points[:, 2] > max_z) | (volume_points[:, 2] < min_z)) * dv.prod() / volume
        self.feats['out_of_field_volume_frac'] = np.array([out_of_field_vol_frac])
        return self

    def calc_DTH(self, target_roi, num_dist_bins, dist_range=None, num_vol_bins=None, vol_range=None, normalize=False):
        """ Calculates a distance to target histogram for a given ROI.

        Calculates a distance-to-target histogram for the ROI contained in the `image_data` ContourImage object. The distance-to-target histogram is
        calculated relative to the ROI contained in the `target_image_data` ContourImage object. For each voxel in the `image_data` ROI the minimum
        distance to the target ROI contained in `target_image_data` is determined, and these values are binned along the distance axis (and the
        volume axis if `num_vol_bins` is supplied) to create the final histogram.

            Parameters:
                - image_data: A DicomRTImage object containing image slices, volume and surface contour masks,
                    and volume and surface mask point arrays for both source and target ROIs.
                - source_roi: A string denoting the ROI that should serve as the source volume for the DTH calculation.
                    Used as a key into the `image_data` dictionaries containing ROI information.
                - target_roi: A string denoting the ROI that should serve as the target volume for the DTH calculation.
                    Used as a key into the `image_data` dictionaries containing ROI information.
                - num_dist_bins: Number of bins used to create distance-to-target histogram. `dist_range` must also be
                    provided for histogram creation to occur.
                - dist_range: A list of two numbers [low high] denoting the lower and upper bounds used to perform
                    histogram binning. Values outside of these bounds are assigned the closest in-bounds bin. If no parameter is
                    supplied the minimum and maximum volume values will be used by default.
                - num_vol_bins: Number of bins used to create version of distance-to-target histogram resampled along the volume axis. If no
                    value is provided the resampled DTH will not be calculated.
                - vol_range: A list of two numbers [low high] denoting the lower and upper bounds used to perform
                    histogram resampling along the volume axis. Values outside of these bounds are assigned the closest
                    in-bounds bin. If no parameter is supplied the minimum and maximum volume values will be used by default.
                - normalize: If True volume axis is normalized to 1.

            Returns:
                - self: The GeometryFeatures object.
        """
        # Check, import, and set values
        image_data = self.image_data
        source_roi = self.roi

        # Find points within target
        inside_points = calc_min_displacement(image_data, source_roi, target_roi, target_type='volume', disp_type='xyz')
        inside_points = np.linalg.norm(inside_points, axis=1) == 0

        # Get minimum displacement from each voxel in ROI to surface of target ROI, make displacement negative for points within target
        min_dist = calc_min_displacement(image_data, source_roi, target_roi, target_type='surface', disp_type='xyz')
        min_dist = np.linalg.norm(min_dist, axis=1)
        min_dist[inside_points] *= -1

        # Calculate histogram sampled along distance axis
        d_vol_vals, edges = np.histogram(min_dist, bins=num_dist_bins, range=dist_range)
        d_vol_vals = d_vol_vals.astype('float') * np.prod(self.image_data.dv)  # Convert to volume units (mm^3)
        if normalize:
            d_vol_vals /= sum(d_vol_vals)
        c_vol_vals = np.cumsum(d_vol_vals)  # Get cumulative histogram from differential histogram
        dist_bins = (edges[:-1] + edges[1:]) / 2

        # Resample cumulative histogram along volume axis
        c_dist_vals = vol_bins = None
        if num_vol_bins is not None:
            vol_bins, c_dist_vals = histogram_resamp(dist_bins, c_vol_vals, num_vol_bins, range_resamp=vol_range, flip_axes=True)

        self.feats['cum_DTH_vol_vals'] = c_vol_vals
        self.feats['dif_DTH_vol_vals'] = d_vol_vals
        self.feats['DTH_dist_bins'] = dist_bins
        self.feats['cum_DTH_dist_vals'] = c_dist_vals
        self.feats['DTH_vol_bins'] = vol_bins

    def calc_RVH(self, target_roi, num_risk_bins, normalize=False, w_ap=(0.7269, 0.07641), w_lr=(0.6158, 0.05669), w_si=(0.9887, 0.1465)):
        """ Calculates a risk volume histogram for a given ROI.

        Calculates a risk volume histogram for the ROI contained in the `image_data` ContourImage object. The risk histogram is calculated relative
        to the ROI contained in the `target_image_data` ContourImage object. A risk value is generated for a single voxel in the `image_data` ROI by
        calculating the distance (in mm) in the AP, LR, and SI directions to the nearest voxel in the `target_image_data` ROI, d_ap, d_lr,
        and d_si. Each distance is plugged into a formula for the appropriate dimension to yield a risk value, r_ap = f_ap(d_ap),
        r_lr = f_lr(d_lr), and r_si = f_si(d_si). Each risk value ranges between 1 (maximum risk) and 0 (no risk). A combined risk value is then
        calculated as r = r_si * (r_ap + r_lr)/2. Any `image_data` ROI voxel contained within the `target_image_data` ROI is automatically assigned a
        risk of 1. The risk histogram is created by binning risk values for all voxels in the `image_data` ROI into `num_risk_bins` bins equally
        spaced from 0 - 1.

            Parameters:
                - image_data: A ContourImage object containing image slices, volume and surface contour masks,
                    and volume and surface mask point arrays for the primary ROI.
                - target_image_data: A ContourImage object containing image slices, volume and surface contour masks,
                    and volume and surface mask point arrays for a secondary "target" ROI.
                - num_risk_bins: Number of bins used to create the risk histogram.
                - normalize: If True volume axis is normalized to 1.

            Returns:
                - self: The GeometryFeatures object.

            Raises:
        """
        # Check, import, and set values
        image_data = self.image_data
        source_roi = self.roi
        surface_mask_target = self.image_data.surface_mask[target_roi]
        volume_mask_source = self.image_data.volume_mask[source_roi]
        surface_points_target = self.image_data.voxel_coords[surface_mask_target]
        volume_points_source = self.image_data.voxel_coords[volume_mask_source]

        # Get minimum displacement vector from each voxel in ROI to target ROI, modify z component
        min_dist = calc_min_displacement(image_data, source_roi, target_roi, target_type='volume')
        z_target_max = max(surface_points_target[:, 2])
        z_target_min = min(surface_points_target[:, 2])
        min_dist[:, 2] = ((volume_points_source[:, 2] >= z_target_max) * (volume_points_source[:, 2] - z_target_max) +
                          (volume_points_source[:, 2] <= z_target_min) * (z_target_min - volume_points_source[:, 2]))
        min_dist = abs(min_dist)

        # Calculate combined risk values
        risk_lr = w_lr[0] * np.exp(-w_lr[1] * min_dist[:, 0]) + 1 - w_lr[0]
        risk_ap = w_ap[0] * np.exp(-w_ap[1] * min_dist[:, 1]) + 1 - w_ap[0]
        risk_si = w_si[0] * np.exp(-w_si[1] * min_dist[:, 2]) + 1 - w_si[0]
        risk_vals = risk_si * ((risk_ap + risk_lr) / 2)

        # Calculate histogram sampled along risk axis
        d_vol_vals, edges = np.histogram(risk_vals, bins=num_risk_bins, range=(0.0, 1.0))
        d_vol_vals = d_vol_vals.astype('float') * np.prod(image_data.dv)  # Convert to volume units (mm^3)
        if normalize:
            d_vol_vals /= sum(d_vol_vals)
        c_vol_vals = np.cumsum(d_vol_vals[::-1])[::-1]  # Get cumulative histogram from differential histogram
        risk_bins = (edges[:-1] + edges[1:]) / 2

        # Assign attribute values
        self.feats['cum_RVH_vol_vals'] = c_vol_vals
        self.feats['dif_RVH_vol_vals'] = d_vol_vals
        self.feats['RVH_risk_bins'] = risk_bins

        return self

    def calc_RVH2(self, target_roi, num_risk_bins, normalize=False, w_ap=(0.7269, 0.07641), w_lr=(0.6158, 0.05669), w_si=(0.9887, 0.1465)):
        """ Calculates a risk volume histogram for a given ROI using 2nd version of risk formula.

        Calculates a risk volume histogram for the ROI contained in the `image_data` ContourImage object. The risk histogram is calculated relative
        to the ROI contained in the `target_image_data` ContourImage object. A risk value is generated for a single voxel in the `image_data` ROI by
        calculating the distance (in mm) in the AP, LR, and SI directions to the nearest voxel in the `target_image_data` ROI, d_ap, d_lr,
        and d_si. Each distance is plugged into a formula for the appropriate dimension to yield a risk value, r_ap = f_ap(d_ap),
        r_lr = f_lr(d_lr), and r_si = f_si(d_si). Each risk value ranges between 1 (maximum risk) and 0 (no risk). A combined risk value is then
        calculated as r = r_si * (r_ap + r_lr)/2. Any `image_data` ROI voxel contained within the `target_image_data` ROI is automatically assigned a
        risk of 1. The risk histogram is created by binning risk values for all voxels in the `image_data` ROI into `num_risk_bins` bins equally
        spaced from 0 - 1.

            Parameters:
                - image_data: A ContourImage object containing image slices, volume and surface contour masks,
                    and volume and surface mask point arrays for the primary ROI.
                - target_image_data: A ContourImage object containing image slices, volume and surface contour masks,
                    and volume and surface mask point arrays for a secondary "target" ROI.
                - num_risk_bins: Number of bins used to create the risk histogram.
                - normalize: If True volume axis is normalized to 1.

            Returns:
                - self: The GeometryFeatures object.

            Raises:
        """
        # Check, import, and set values
        image_data = self.image_data
        source_roi = self.roi
        surface_mask_target = self.image_data.surface_mask[target_roi]
        volume_mask_source = self.image_data.volume_mask[source_roi]
        surface_points_target = self.image_data.voxel_coords[surface_mask_target]
        volume_points_source = self.image_data.voxel_coords[volume_mask_source]

        # Get minimum displacement vector from each voxel in ROI to target ROI, modify z component
        min_dist = calc_min_displacement(image_data, source_roi, target_roi, target_type='volume', disp_type='xyz')
        norm_dist = np.linalg.norm(min_dist, axis=1)
        z_target_max = max(surface_points_target[:, 2])
        z_target_min = min(surface_points_target[:, 2])
        min_dist[:, 2] = ((volume_points_source[:, 2] >= z_target_max) * (volume_points_source[:, 2] - z_target_max) +
                          (volume_points_source[:, 2] <= z_target_min) * (z_target_min - volume_points_source[:, 2]))
        min_dist = abs(min_dist)

        # Calculate risk due to the distance from PTV effect
        risk_ap = w_ap[0] * np.exp(-w_ap[1] * norm_dist) + 1 - w_ap[0]
        risk_lr = w_lr[0] * np.exp(-w_lr[1] * norm_dist) + 1 - w_lr[0]
        aplr_zero_mask = (min_dist[:, 0] == 0) * (min_dist[:, 1] == 0)
        risk_aplr = aplr_zero_mask.astype('float')  # make risk 1 where ap and lr displacements are both 0, initialize other risk values to 0
        risk_aplr[~aplr_zero_mask] = ((min_dist[~aplr_zero_mask, 1] * risk_lr[~aplr_zero_mask] +
                                       min_dist[~aplr_zero_mask, 0] * risk_ap[~aplr_zero_mask]) /
                                      (min_dist[~aplr_zero_mask, 0] + min_dist[~aplr_zero_mask, 1]))

        # Calculate risk due to the out of field effect
        risk_si = w_si[0] * np.exp(-w_si[1] * min_dist[:, 2]) + 1 - w_si[0]

        # Combine risk values
        risk_vals = risk_si * risk_aplr

        # Calculate histogram sampled along risk axis
        d_vol_vals, edges = np.histogram(risk_vals, bins=num_risk_bins, range=(0.0, 1.0))
        d_vol_vals = d_vol_vals.astype('float') * np.prod(image_data.dv)  # Convert to volume units (mm^3)
        if normalize:
            d_vol_vals /= sum(d_vol_vals)
        c_vol_vals = np.cumsum(d_vol_vals[::-1])[::-1]  # Get cumulative histogram from differential histogram
        risk_bins = (edges[:-1] + edges[1:]) / 2

        # Assign attribute values
        self.feats['cum_RVH_vol_vals'] = c_vol_vals
        self.feats['dif_RVH_vol_vals'] = d_vol_vals
        self.feats['RVH_risk_bins'] = risk_bins

        return self

    def calc_compRVH(self, target_roi, num_risk_bins, normalize=False, w_ap=(0.7269, 0.07641), w_lr=(0.6158, 0.05669), w_si=(0.9887, 0.1465)):
        """ Calculates component risk volume histograms for a given ROI.

        Calculates a risk volume histogram for the ROI contained in the `image_data` ContourImage object. The risk histogram is calculated
        relative to the ROI contained in the `target_image_data` ContourImage object. A risk value is generated for a single voxel in the
        `image_data` ROI by calculating the distance (in mm) in the AP, LR, and SI directions to the nearest voxel in the `target_image_data`
        ROI, d_ap, d_lr, and d_si. Each distance is plugged into a formula for the appropriate dimension to yield a risk value, r_ap = f_ap(d_ap),
        r_lr = f_lr(d_lr), and r_si = f_si(d_si). Each risk value ranges between 1 (maximum risk) and 0 (no risk). Separate histograms are
        created for the SI (or z) component and the combined AP and LR (or X and Y) components. Combined risk values for the AP and LR directions
        are calculated as r_ap.lr = (r_ap + r_lr)/2.. Any `image_data` ROI voxel contained within the `target_image_data` ROI is automatically
        assigned a risk of 1. The risk histogram is created by binning risk values for all voxels in the `image_data` ROI into `num_risk_bins` bins
        equally spaced from 0 - 1.

            Parameters:
                - image_data: A ContourImage object containing image slices, volume and surface contour masks,
                    and volume and surface mask point arrays for the primary ROI.
                - target_image_data: A ContourImage object containing image slices, volume and surface contour masks,
                    and volume and surface mask point arrays for a secondary "target" ROI.
                - num_risk_bins: Number of bins used to create the risk histogram.
                - normalize: If True volume axis is normalized to 1.

            Returns:
                - self: The GeometryFeatures object.

            Raises:
        """
        # Check, import, and set values
        image_data = self.image_data
        source_roi = self.roi
        surface_mask_target = self.image_data.surface_mask[target_roi]
        volume_mask_source = self.image_data.volume_mask[source_roi]
        surface_points_target = self.image_data.voxel_coords[surface_mask_target]
        volume_points_source = self.image_data.voxel_coords[volume_mask_source]

        # Get minimum displacement vector from each voxel in ROI to target ROI, modify z component
        min_dist = calc_min_displacement(image_data, source_roi, target_roi, target_type='volume')
        z_target_max = max(surface_points_target[:, 2])
        z_target_min = min(surface_points_target[:, 2])
        min_dist[:, 2] = ((volume_points_source[:, 2] >= z_target_max) * (volume_points_source[:, 2] - z_target_max) +
                          (volume_points_source[:, 2] <= z_target_min) * (z_target_min - volume_points_source[:, 2]))
        min_dist = abs(min_dist)

        # Calculate combined risk values
        risk_lr = w_lr[0] * np.exp(-w_lr[1] * min_dist[:, 0]) + 1 - w_lr[0]
        risk_ap = w_ap[0] * np.exp(-w_ap[1] * min_dist[:, 1]) + 1 - w_ap[0]
        risk_aplr = (risk_ap + risk_lr) / 2
        risk_si = w_si[0] * np.exp(-w_si[1] * min_dist[:, 2]) + 1 - w_si[0]

        # Calculate histogram sampled along risk axis
        d_xyvol_vals, edges = np.histogram(risk_aplr, bins=num_risk_bins, range=(0.0, 1.0))
        d_xyvol_vals = d_xyvol_vals.astype('float') * np.prod(image_data.dv)  # Convert to volume units (mm^3)
        d_zvol_vals, edges = np.histogram(risk_si, bins=num_risk_bins, range=(0.0, 1.0))
        d_zvol_vals = d_zvol_vals.astype('float') * np.prod(image_data.dv)  # Convert to volume units (mm^3)
        if normalize:
            d_xyvol_vals /= sum(d_xyvol_vals)
            d_zvol_vals /= sum(d_zvol_vals)
        c_xyvol_vals = np.cumsum(d_xyvol_vals[::-1])[::-1]  # Get cumulative histogram from differential histogram
        c_zvol_vals = np.cumsum(d_zvol_vals[::-1])[::-1]  # Get cumulative histogram from differential histogram
        risk_bins = (edges[:-1] + edges[1:]) / 2

        # Assign attribute values
        self.feats['cum_xyRVH_vol_vals'] = c_xyvol_vals
        self.feats['dif_xyRVH_vol_vals'] = d_xyvol_vals
        self.feats['cum_zRVH_vol_vals'] = c_zvol_vals
        self.feats['dif_zRVH_vol_vals'] = d_zvol_vals
        self.feats['compRVH_risk_bins'] = risk_bins

        return self


class IntensityFeatures(Features):
    """ Calculates intensity feature data.

    Contains methods that calculate intensity features, which are stored in a dictionary. The feature
    dictionary is an attribute of the class.

        Parameters:
            - image_data: A DicomRTImage object containing image slices, volume and surface contour masks,
                    and volume and surface mask point arrays for both source and target ROIs.
            - roi: A string denoting the primary ROI to be used for feature calculation. Used as a key
                into the `image_data` dictionaries containing ROI information.

        Attributes:
            - image_data: A DicomRTImage object containing image slices, volume and surface contour masks,
                and volume and surface mask point arrays for both source and target ROIs.
            - roi: A string denoting the primary ROI to be used for feature calculation. Used as a key
                into the `image_data` dictionaries containing ROI information.
            - feats: A dictionary where keys are intensity feature names and values are lists of intensity feature
                values.
    """
    def __init__(self, image_data=None, roi=None):
        # Check input
        if image_data is None:
            pass
        elif roi is None:
            roi = image_data.roi_names[0]
        elif roi not in image_data.roi_names:
            raise KeyError("Incorrect ROI name supplied, valid options are {0}".format(image_data.roi_names))

        # Assign attribute values
        self.image_data = image_data
        self.roi = roi
        self.feats = {'intensity_min': None, 'intensity_max': None, 'intensity_mean': None, 'intensity_median': None,
                      'intensity_sum': None, 'intensity_variance': None, 'intensity_stdev': None, 'intensity_skewness': None,
                      'intensity_kurtosis': None}

    def calculate(self):
        """ Calculates intensity features from contour and image data.

        Uses the DicomRTImage object containing the contour and image data for the primary ROI and calculates basic intensity
        features of the ROI.

            Parameters:

            Returns:
                - self: The IntensityFeatures object.

            Raises:
        """
        slices = self.image_data.slices
        volume_mask = self.image_data.volume_mask[self.roi]
        intensity_vals = slices[volume_mask]
        self.feats['intensity_min'] = np.array([min(intensity_vals)])
        self.feats['intensity_max'] = np.array([max(intensity_vals)])
        self.feats['intensity_mean'] = np.array([np.mean(intensity_vals)])
        self.feats['intensity_median'] = np.array([np.median(intensity_vals)])
        self.feats['intensity_sum'] = np.array([sum(intensity_vals)])
        self.feats['intensity_variance'] = np.array([np.var(intensity_vals)])
        self.feats['intensity_stdev'] = np.array([np.std(intensity_vals)])
        self.feats['intensity_skewness'] = np.array([stats.skew(intensity_vals)])
        self.feats['intensity_kurtosis'] = np.array([stats.kurtosis(intensity_vals)])

        return self


class TextureFeatures(Features):
    """ Calculates texture feature data.

    Uses the DicomRTImage object containing the contour and image data for the primary ROI and calculates texture
    features of the ROI.

        Parameters:
            - image_data: A DicomRTImage object containing image slices, volume and surface contour masks,
                    and volume and surface mask point arrays for both source and target ROIs.
            - roi: A string denoting the primary ROI to be used for feature calculation. Used as a key
                into the `image_data` dictionaries containing ROI information.

        Attributes:
            - image_data: A DicomRTImage object containing image slices, volume and surface contour masks,
                    and volume and surface mask point arrays for both source and target ROIs.
            - roi: A string denoting the primary ROI to be used for feature calculation. Used as a key
                into the `image_data` dictionaries containing ROI information.
            - feats: A dictionary where keys are texture feature names and values are lists of texture feature
                values.
    """

    def __init__(self, image_data=None, roi=None):
        # Check input
        if image_data is None:
            pass
        elif roi is None:
            roi = image_data.roi_names[0]
        elif roi not in image_data.roi_names:
            raise KeyError("Incorrect ROI name supplied, valid options are {0}".format(image_data.roi_names))

        # Assign attribute values
        self.image_data = image_data
        self.roi = roi
        self.feats = {'angular_second_moment': None, 'contrast': None, 'correlation': None, 'sum_of_squares_variance': None,
                      'homogeneity': None, 'sum_average': None, 'sum_variance': None, 'sum_entropy': None, 'entropy': None,
                      'difference_variance': None, 'difference_entropy': None, 'info_measure_of_correlation1': None,
                      'info_measure_of_correlation2': None,

                      'cluster_prominence': None, 'cluster_shade': None, 'dissimilarity': None, 'maximum_probability': None,
                      'inverse_difference': None, 'inverse_difference_normalized': None,
                      'inverse_difference_moment_normalized': None, 'autocorrelation': None}

    def calculate(self, num_glcm_levels=255):
        """ Calculates texture features from contour and image data.

        Takes a DicomRTImage object containing the contour and image data for an ROI and calculates texture features
        of the ROI.

            Parameters:
                - image_data: A DicomRTImage object containing image slices, volume and surface contour masks,
                    and volume and surface mask point arrays for the ROI.
                - num_glcm_levels: The number of gray levels to which the image slices are resampled for the
                    calculation of GLCM matrices. If this value is less than 1 or greater than 255 it is defaulted to
                    255.

            Returns:
                - self: The TextureFeatures object.

            Raises:
        """
        # Check input
        if num_glcm_levels < 1 or num_glcm_levels > 255:
            num_glcm_levels = 255

        # Create scaled integer image, values outside of contour should = 0, all other values should range from 1 to num_glcm_levels
        slices = self.image_data.slices
        volume_mask = self.image_data.volume_mask[self.roi]
        image_min = slices[volume_mask].min()
        image_max = slices[volume_mask].max()
        scale_image = (slices - image_min) * (num_glcm_levels - 1.0)/(image_max - image_min) + 1
        scale_image = (scale_image * volume_mask).astype('int32')

        # Calculate texture features
        feats = mhtstexture.haralick(scale_image, ignore_zeros=True)
        self.feats['angular_second_moment'] = np.array([np.mean(feats[:, 0])])  # also known as energy
        self.feats['contrast'] = np.array([np.mean(feats[:, 1])])
        self.feats['correlation'] = np.array([np.mean(feats[:, 2])])
        self.feats['sum_of_squares_variance'] = np.array([np.mean(feats[:, 3])])  # old variance
        self.feats['homogeneity'] = np.array([np.mean(feats[:, 4])])  # also known as inverse difference moment, essentially same as inverse_difference
        self.feats['sum_average'] = np.array([np.mean(feats[:, 5])])
        self.feats['sum_variance'] = np.array([np.mean(feats[:, 6])])
        self.feats['sum_entropy'] = np.array([np.mean(feats[:, 7])])
        self.feats['entropy'] = np.array([np.mean(feats[:, 8])])
        self.feats['difference_variance'] = np.array([np.mean(feats[:, 9])])
        self.feats['difference_entropy'] = np.array([np.mean(feats[:, 10])])
        self.feats['info_measure_of_correlation1'] = np.array([np.mean(feats[:, 11])])
        self.feats['info_measure_of_correlation2'] = np.array([np.mean(feats[:, 12])])
        self.feats['cluster_shade'] = np.array([np.mean(feats[:, 14])])
        self.feats['cluster_prominence'] = np.array([np.mean(feats[:, 15])])
        self.feats['autocorrelation'] = np.array([np.mean(feats[:, 16])])
        self.feats['inverse_difference'] = np.array([np.mean(feats[:, 17])])
        self.feats['maximum_probability'] = np.array([np.mean(feats[:, 18])])
        self.feats['dissimilarity'] = np.array([np.mean(feats[:, 19])])
        self.feats['inverse_difference_normalized'] = np.array([np.mean(feats[:, 20])])
        self.feats['inverse_difference_moment_normalized'] = np.array([np.mean(feats[:, 21])])

        return self


def histogram_resamp(bins, vals, num_bins_resamp, range_resamp=None, flip_axes=False):
    """ Takes an input histogram and resamples it to the desired number of bins and range.

        Parameters:
            - bins: A list of equally spaced values that denote the center locations
                of the original histogram bins.
            - vals: A list containing the original histogram bin values.
            - num_bins_resamp: The number of bins the resampled histogram should contain.
            - range_resamp: A two element list indicated the lower and upper bounds,
                respectively between which value for the new histogram should be binned.
            - flip_axes: If True the histogram will be resampled along the value axis.
                The returned values will correspond to the original bin axis and the
                returned bins will correspond to the original value axis. To get meaningful
                output the input histogram must be either monotonically increasing
                or decreasing. Any plateau areas are reduced to a single unique point.

        Returns:
            - bins_resmp: A list of equally spaced histogram bin center sampled along
                the original bin axis.
            - vals_resamp: A list containing new histogram values.

        Raises:
            - An error if `bins` and `vals` are different lengths.
    """
    # Check for errors
    if len(bins) != len(vals):
        raise Exception('Number of bins not equal to number of values.')

    # Flip axes if desired
    if flip_axes:
        eps_i = abs(float(vals[0]) * 10 ** -6)
        eps_f = abs(float(vals[-1]) * 10 ** -6)
        mask_i = abs(vals - vals[0]) <= eps_i
        mask_f = abs(vals - vals[-1]) <= eps_f
        mask_i[np.max(np.where(mask_i))] = False
        mask_f[np.min(np.where(mask_f))] = False
        mask = mask_i + mask_f
        vals = np.array(vals)[~mask]
        bins = np.array(bins)[~mask]
        vals_unique, inds = np.unique(vals, return_index=True)
        bins_unique = bins[inds]
        bins = vals_unique
        vals = bins_unique

    # Perform resampling
    if range_resamp is None:
        range_resamp = [min(bins), max(bins)]
    bins_resamp = np.linspace(range_resamp[0], range_resamp[1], num_bins_resamp)
    vals_resamp = np.interp(bins_resamp, bins, vals)

    return bins_resamp, vals_resamp


def blobify(arr):
    """ Turn numpy array into binary string.

    Turns a numpy array into a binary string. This makes the array suitable for storage as a Binary Large
    OBject, or BLOB.

        Parameters:
            - arr: A numpy array.

        Returns:
            - blob: The binary string version of the array.

        References:
            - http://stackoverflow.com/questions/18621513/python-insert-numpy-array-into-sqlite3-database
    """
    out = io.BytesIO()
    np.save(out, arr)
    out.seek(0)
    blob = out.read()
    return blob


def deblobify(blob):
    """ Turn a numpy array stored as a binary string back into a numpy array.

    Converts a binary string back into a numpy array. Assumes that the binary string
    was originally a numpy array.

        Parameters:
            - blob: The binary string version of the array.

        Returns:
            - arr: A numpy array.

        References:
            - http://stackoverflow.com/questions/18621513/python-insert-numpy-array-into-sqlite3-database
    """
    out = io.BytesIO(blob)
    out.seek(0)
    arr = np.load(out)
    return arr
